<?php

  require_once(dirname(__FILE__) . "/MySqlDatabase.class.php");

  class EventsTable
  {

    private $clsDatabase = null;
    private $tableName = "events";
    
    var $typeOfEvents = array("views", "plays", "clicks");
    
    public function __construct($clsDatabase) {
    
      $this->clsDatabase = $clsDatabase;
      
    }

    public function isEventExists($dateOfEvent, $countryCode, $typeOfEvent) {
    
      $query = "SELECT id FROM " . $this->tableName . " USE INDEX (events_index)" .
               " WHERE date_of_event=" . $this->clsDatabase->quote($dateOfEvent) .
               "   AND country_code=" . $this->clsDatabase->quote($countryCode) .
               "   AND type_of_event=" . $this->clsDatabase->quote($typeOfEvent);
                
      $result = $this->clsDatabase->executeSingleSelectQuery($query);
      
      if ($result == null) {
        return false;
      }
      
      return true;
    }

    public function registerEvent($dateOfEvent, $countryCode, $typeOfEvent, $increaseCountsBy = 1) {

      if (in_array($typeOfEvent, $this->typeOfEvents) == false) {
        throw new Exception("RegisterEvent Error: Unsupported event type '" . $typeOfEvent . "'");
      };
      
      if ($this->isEventExists($dateOfEvent, $countryCode, $typeOfEvent) == true) {
      
        $query = "UPDATE " . $this->tableName . " USE INDEX (events_index)" .
                 "   SET counts=(counts + " . $increaseCountsBy . ") " . 
                 " WHERE date_of_event=" . $this->clsDatabase->quote($dateOfEvent) .
                 "   AND country_code=" . $this->clsDatabase->quote($countryCode) .
                 "   AND type_of_event=" . $this->clsDatabase->quote($typeOfEvent);
        $rowsAffected = $this->clsDatabase->executeUpdateQuery($query);
        
      } else {
      
        $query = "INSERT INTO " . $this->tableName .
                 "  (date_of_event, country_code, type_of_event, counts) VALUES (" . 
                 $this->clsDatabase->quote($dateOfEvent) . ", " .
                 $this->clsDatabase->quote($countryCode) . ", " .
                 $this->clsDatabase->quote($typeOfEvent) . ", " .
                 "" . $increaseCountsBy . ")"; 
                  
        $rowsAffected = $this->clsDatabase->executeInsertQuery($query);
                  
      };

      if ($rowsAffected == 1) {
        return true;
      }
        
      return false;
    }
    
    public function getTopFiveCountriesOfAllTimes() {
    
      $query = "SELECT country_code, SUM(counts) AS all_counts FROM " . $this->tableName . " USE INDEX (events_index) " .
               " GROUP BY country_code " .
               " ORDER BY all_counts DESC " .
               " LIMIT 0,5";
                
      return $this->clsDatabase->executeSelectQuery($query);
    }

    public function getLastSevenDaysByCountires($countryCodes) {
    
      if (count($countryCodes) == 0) {
        return array();
      }
    
      $query = "SELECT *, SUM(counts)AS total_7 FROM " . $this->tableName . " USE INDEX (events_index) " .
               " WHERE country_code IN (" . "'" . implode("','", $countryCodes) . "') " .
               "   AND date_of_event <= NOW() " .
               "   AND date_of_event >= DATE_SUB(NOW(), INTERVAL 7 DAY) " .
               " GROUP BY counts";
                
      return $this->clsDatabase->executeSelectQuery($query);
    }
}


?>