<?php

class MySqlDatabase {

  private $host = "localhost";
  private $port = "";
  private $username = "root";
  private $password = "";
  private $databaseName = "";

  private $databaseResourceId = null;
  
  public function __construct($host,
                              $port,
                              $username,
                              $password,
                              $database) {
    $this->setHost($host);
    $this->setPort($port);
    $this->setUsername($username);
    $this->setPassword($password);
    $this->setDatabaseName($database);
  }

  public function setHost($value) {
    $this->host = $value;
  }
  public function getHost() {
    return $this->host;
  }

  public function setPort($value) {
    $this->port = $value;
  }
  public function getPort() {
    return $this->port;
  }

  public function setUsername($value) {
    $this->username = $value;
  }
  public function getUsername() {
    return $this->username;
  }

  public function setPassword($value) {
    $this->password = $value;
  }
  public function getPassword() {
    return $this->password;
  }

  public function setDatabaseName($value) {
    $this->databaseName = $value;
  }
  public function getDatabaseName() {
    return $this->databaseName;
  }

  public function createRowFromArray($associativeArray,
                                     $rowInstance = null) {
    if (get_magic_quotes_gpc())
    {
      foreach($associativeArray as $tag => $value)
      {
        $associativeArray[$tag] = stripslashes($value);
      }    
    }    
    if (is_null($rowInstance))
    {
       return array();
    }
    else if (is_array($rowInstance))
    {
       return $associativeArray;
    }
    else
    {
      $result = $rowInstance->instance();
      foreach($associativeArray as $tag => $value)
      {
        $result->setValue($tag,$value);
      }
      return $result;
    }      
  }

  public function connect() {
    $this->databaseResourceId = mysqli_connect($this->getHost(), // . ($this->getPort() != "" ? ":" . $this->getPort() : ""), 
                                               $this->getUsername(), 
                                               $this->getPassword(),
                                               $this->getDatabaseName());
    if (!$this->databaseResourceId) {
      throw new Exception("MySql Error: Unable to connect to MySQL, error number is: " . mysqli_connect_errno());
    };
    return true;
  }

  public function disconnect() {
    mysqli_close($this->databaseResourceId);
    $this->databaseResourceId = null;
    return true;
  }

  public function quote($value) {
    if (is_null($value))
    {
      return "NULL";
    }
    if (strcmp(strtolower($value),"now()") == 0 ||
        strcmp(strtolower($value),"getdate()") == 0)
    {
      return "NOW()";
    }    
    if (get_magic_quotes_gpc())
    {
      return "'" . mysqli_real_escape_string($this->databaseResourceId, stripslashes($value)) . "'";
    }
    else
    {
      return "'" . mysqli_real_escape_string($this->databaseResourceId, stripslashes($value)) . "'";
    }
  }

  public function quoteField($name) {
    return "`" . $name . "`";
  }

  public function executeSingleSelectQuery($query) {
    $result = null;
    $resultSet = mysqli_query($this->databaseResourceId,$query);
    if ($resultSet === FALSE) {
      throw new Exception("MySql executeSingleSelectQuery Error: ." . mysqli_error($this->databaseResourceId));
    };
    if ($row = mysqli_fetch_array($resultSet, MYSQLI_ASSOC))
    {
      $result = $this->createRowFromArray($row, array());
    }
    mysqli_free_result($resultSet);
    return $result;
  }
  
  public function executeSelectQuery($query) {
    $result = array();
    $resultSet = mysqli_query($this->databaseResourceId, $query);
    if ($resultSet === FALSE) {
      throw new Exception("MySql executeSelectQuery Error: ." . mysqli_error($this->databaseResourceId));
    };
    while ($row = mysqli_fetch_array($resultSet, MYSQLI_ASSOC))
    {
      $result[] = $this->createRowFromArray($row, array());
    }
    mysqli_free_result($resultSet);
    return $result;
  } 

  public function executeInsertQuery($query) {
    $resultSet = mysqli_query($this->databaseResourceId, $query);
    if ($resultSet === FALSE) {
      throw new Exception("MySql executeInsertQuery Error: ." . mysqli_error($this->databaseResourceId));
    };
    return mysqli_affected_rows($this->databaseResourceId);
  }

  public function executeUpdateQuery($query) {
    $resultSet = mysqli_query($this->databaseResourceId, $query);
    if ($resultSet === FALSE) {
      throw new Exception("MySql executeUpdateQuery Error: ." . mysqli_error($this->databaseResourceId));
    };
    return mysqli_affected_rows($this->databaseResourceId);
  }

}

  
?>