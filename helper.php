<?php

  function JSON_Response($success, $message) {
    global $startTime;

    // write error to log file "_errors.log.txt"
    if ($success == false) {
      Log_Error($message);
    };

    // get script(s) execution end time (float seconds)
    list($microseconds, $seconds) = explode(" ",microtime());
    $stopTime = ($microseconds + $seconds);
    $duration = $stopTime - $startTime;

    // create output JSON array
    $jsonArray = array("success" => $success, 
                       "message" => $message,
                       "duration" => $duration . " seconds",
                       "flush_time" => date("F d Y H:i:s")
                      );

    // fails on dev hosting
    // @ob_clean();  

    header("Content-Type: application/json");

    // create output JSON text and send to browser as response
    echo json_encode($jsonArray, JSON_PRETTY_PRINT);
    exit();
  };

  function Flush_JSON_Response($jsonArray, $cacheFileName = "") {
    global $startTime;
  
    // get script(s) execution end time (float seconds)
    list($microseconds, $seconds) = explode(" ",microtime());
    $stopTime = ($microseconds + $seconds);
    $duration = $stopTime - $startTime;
  
    // create output JSON array
    $jsonArray = array("duration" => $duration . " seconds",
                       "flush_time" => date("F d Y H:i:s"),
                       "data" => $jsonArray);

    // create output JSON text and send to browser as response
    $jsonAsString = json_encode($jsonArray, JSON_PRETTY_PRINT);

    if ($cacheFileName != "") {
      $filePath = dirname(__FILE__) . CACHED_DIRECTORY_PATH . $cacheFileName;
      if (file_exists($filePath)) {
        unlink($filePath);
      }
      file_put_contents($filePath, $jsonAsString);
    }

    header("Content-Type: application/json");
    echo $jsonAsString;
    
    exit();

  };

  function To_XML(SimpleXMLElement $object, array $xmlArray) {
    foreach ($xmlArray as $tag => $value) {
      if (is_array($value)) {
        if (is_integer($tag)) {
          $new_object = $object->addChild("row_" . $tag);
        } else {
          $new_object = $object->addChild($tag);
        }
        To_XML($new_object, $value);
      } else {
        $object->addChild($tag, $value);
      }   
    }   
  }     
  
  function Flush_XML_Response($xmlArray, $cacheFileName = "") {
    global $startTime;
  
    // get script(s) execution end time (float seconds)
    list($microseconds, $seconds) = explode(" ",microtime());
    $stopTime = ($microseconds + $seconds);
    $duration = $stopTime - $startTime;
  
    // create output XML array
    $xmlArray = array("duration" => $duration . " seconds",
                      "flush_time" => date("F d Y H:i:s"),
                      "data" => $xmlArray);

    $xmlElement = new SimpleXMLElement("<root/>");
    To_XML($xmlElement, $xmlArray);
    $ownerDocument = dom_import_simplexml($xmlElement)->ownerDocument;
    $ownerDocument->formatOutput = TRUE;
    $xmlAsString = (string) $ownerDocument->saveXML();  

    if ($cacheFileName != "") {
      $filePath = dirname(__FILE__) . CACHED_DIRECTORY_PATH . $cacheFileName;
      if (file_exists($filePath)) {
        unlink($filePath);
      }
      file_put_contents($filePath, $xmlAsString);
    }

    header("Content-type: application/xml");
    echo $xmlAsString;
    
    exit();

  };

  function Flush_CSV_Response($csvArray, $cacheFileName = "") {
    global $startTime;
  
    // get script(s) execution end time (float seconds)
    list($microseconds, $seconds) = explode(" ",microtime());
    $stopTime = ($microseconds + $seconds);
    $duration = $stopTime - $startTime;

    $csvAsString = implode(array("country_code","all_counts","counts_7_last_days","plays","views","clicks"), ",") . "\r\n";
    foreach($csvArray as $csvLine) {
      $csvAsString .= implode($csvLine, ",") . "\r\n";
    }
    
    if ($cacheFileName != "") {
      $filePath = dirname(__FILE__) . CACHED_DIRECTORY_PATH . $cacheFileName;
      if (file_exists($filePath)) {
        unlink($filePath);
      }
      // $fp = fopen($filePath, 'w');
      
      file_put_contents($filePath, $csvAsString);
    }

    header("Content-type: application/excel");
    header("Content-disposition: attachment; filename=\"" . CACHED_CSV_FILE_NAME ."\""); 
    echo $csvAsString;
    
    exit();

  };

?>
