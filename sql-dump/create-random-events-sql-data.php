<?php

  require_once(dirname(__FILE__) . "/../configuration.php");
  require_once(dirname(__FILE__) . "/../classes/EventsTable.class.php");

  if (isset($_SERVER["REQUEST_METHOD"])) {
    header("HTTP/1.1 400 BAD REQUEST");
    JSON_Response(false, "[\sql-dump\create-random-events-sql-data.php] This script can run only from command line!");
  };

  // create database layer, do not connect yet to database to save access time
  $clsDatabase = new MySqlDatabase(DB_HOST, // host 
                                   "", // port 
                                   DB_USER, // username
                                   DB_PASSWORD, // password
                                   DB_NAME // database
                                  );


  $start_date = date("Y-01-01");
  $end_date = date("Y-m-d");
  $daysDates = array();
  // get all dates from year begin (2018-01-01) to today date/day  
  while (strtotime($start_date) <= strtotime($end_date)) {
      $daysDates[] = $start_date;
      $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));
  };
  
  try {
  
    // connect to database
    $clsDatabase->connect();
  
    // create EventsTable mapped to MySql table 'events'
    $tblEvents = new EventsTable($clsDatabase);
    
    $someOfCountries = array("AU", "AT", "CA", "BE", "BA", "BR", "HR", "CY", "EG", "RS", "FR", "DE");

    // list all dates from begi of year
    foreach($daysDates as $dateOfEvent) {
    
      // list 12 random countries
      foreach($someOfCountries as $countryCode) {
      
        // list event types: "views", "plays", "clicks"
        foreach($typeOfEvents as $typeOfEvent) {
        
          // create one event registration for each of three loops
          $eventRegistered = $tblEvents->registerEvent($dateOfEvent, // $dateOfEvent
                                                       $countryCode, // $countryCode
                                                       $typeOfEvent, // $typeOfEvent
                                                       rand(1, 50000) // $increaseCountsBy
                                                      );
          // star is needed to see how many events has been created
          echo "*";
        };
      };
      echo "\r\n";
    };
    
    // disconnect from database
    $clsDatabase->disconnect();

    // return to web caller, Event has been registered with no errors
    JSON_Response(true, "Database filled with random events!");
    
  } catch (Exception $e) {

      // return to web caller failed Event registrations
      JSON_Response(false, $e->getMessage()) ;
  }

?>
