-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.19-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5269
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table __quantoxphptest.events
DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_of_event` date NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `type_of_event` enum('views','plays','clicks') NOT NULL,
  `counts` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `day_of_event_country_code_type_of_event` (`date_of_event`,`country_code`,`type_of_event`),
  KEY `events_index` (`date_of_event`,`country_code`,`type_of_event`,`counts`)
) ENGINE=InnoDB AUTO_INCREMENT=10776 DEFAULT CHARSET=utf8;

-- Dumping data for table __quantoxphptest.events: ~5,328 rows (approximately)
DELETE FROM `events`;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5451, '2018-01-01', 'AT', 'views', 53772);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5452, '2018-01-01', 'AT', 'plays', 11080);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5453, '2018-01-01', 'AT', 'clicks', 24986);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5448, '2018-01-01', 'AU', 'views', 67618);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5449, '2018-01-01', 'AU', 'plays', 23882);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5450, '2018-01-01', 'AU', 'clicks', 39137);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5460, '2018-01-01', 'BA', 'views', 58995);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5461, '2018-01-01', 'BA', 'plays', 67291);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5462, '2018-01-01', 'BA', 'clicks', 54045);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5457, '2018-01-01', 'BE', 'views', 84147);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5458, '2018-01-01', 'BE', 'plays', 89534);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5459, '2018-01-01', 'BE', 'clicks', 34832);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5463, '2018-01-01', 'BR', 'views', 83960);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5464, '2018-01-01', 'BR', 'plays', 42247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5465, '2018-01-01', 'BR', 'clicks', 52494);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5454, '2018-01-01', 'CA', 'views', 36730);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5455, '2018-01-01', 'CA', 'plays', 75609);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5456, '2018-01-01', 'CA', 'clicks', 58853);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5469, '2018-01-01', 'CY', 'views', 78187);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5470, '2018-01-01', 'CY', 'plays', 34873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5471, '2018-01-01', 'CY', 'clicks', 38110);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5481, '2018-01-01', 'DE', 'views', 70818);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5482, '2018-01-01', 'DE', 'plays', 31451);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5483, '2018-01-01', 'DE', 'clicks', 79645);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5472, '2018-01-01', 'EG', 'views', 80654);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5473, '2018-01-01', 'EG', 'plays', 45417);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5474, '2018-01-01', 'EG', 'clicks', 74438);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5478, '2018-01-01', 'FR', 'views', 73278);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5479, '2018-01-01', 'FR', 'plays', 20224);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5480, '2018-01-01', 'FR', 'clicks', 50879);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5466, '2018-01-01', 'HR', 'views', 34340);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5467, '2018-01-01', 'HR', 'plays', 94736);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5468, '2018-01-01', 'HR', 'clicks', 61882);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5475, '2018-01-01', 'RS', 'views', 43104);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5476, '2018-01-01', 'RS', 'plays', 57161);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5477, '2018-01-01', 'RS', 'clicks', 40899);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5487, '2018-01-02', 'AT', 'views', 58923);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5488, '2018-01-02', 'AT', 'plays', 23926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5489, '2018-01-02', 'AT', 'clicks', 15557);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5484, '2018-01-02', 'AU', 'views', 40209);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5485, '2018-01-02', 'AU', 'plays', 45158);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5486, '2018-01-02', 'AU', 'clicks', 67030);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5496, '2018-01-02', 'BA', 'views', 54949);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5497, '2018-01-02', 'BA', 'plays', 67157);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5498, '2018-01-02', 'BA', 'clicks', 68767);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5493, '2018-01-02', 'BE', 'views', 83111);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5494, '2018-01-02', 'BE', 'plays', 11992);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5495, '2018-01-02', 'BE', 'clicks', 82790);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5499, '2018-01-02', 'BR', 'views', 36850);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5500, '2018-01-02', 'BR', 'plays', 14989);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5501, '2018-01-02', 'BR', 'clicks', 58493);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5490, '2018-01-02', 'CA', 'views', 58755);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5491, '2018-01-02', 'CA', 'plays', 21536);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5492, '2018-01-02', 'CA', 'clicks', 60165);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5505, '2018-01-02', 'CY', 'views', 68281);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5506, '2018-01-02', 'CY', 'plays', 89062);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5507, '2018-01-02', 'CY', 'clicks', 57076);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5517, '2018-01-02', 'DE', 'views', 54830);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5518, '2018-01-02', 'DE', 'plays', 54334);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5519, '2018-01-02', 'DE', 'clicks', 33132);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5508, '2018-01-02', 'EG', 'views', 41763);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5509, '2018-01-02', 'EG', 'plays', 23152);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5510, '2018-01-02', 'EG', 'clicks', 20382);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5514, '2018-01-02', 'FR', 'views', 28616);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5515, '2018-01-02', 'FR', 'plays', 48863);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5516, '2018-01-02', 'FR', 'clicks', 49642);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5502, '2018-01-02', 'HR', 'views', 52154);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5503, '2018-01-02', 'HR', 'plays', 57249);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5504, '2018-01-02', 'HR', 'clicks', 19471);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5511, '2018-01-02', 'RS', 'views', 19203);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5512, '2018-01-02', 'RS', 'plays', 67744);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5513, '2018-01-02', 'RS', 'clicks', 60767);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5523, '2018-01-03', 'AT', 'views', 57966);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5524, '2018-01-03', 'AT', 'plays', 21680);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5525, '2018-01-03', 'AT', 'clicks', 44785);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5520, '2018-01-03', 'AU', 'views', 93946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5521, '2018-01-03', 'AU', 'plays', 78735);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5522, '2018-01-03', 'AU', 'clicks', 22994);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5532, '2018-01-03', 'BA', 'views', 25782);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5533, '2018-01-03', 'BA', 'plays', 27717);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5534, '2018-01-03', 'BA', 'clicks', 31957);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5529, '2018-01-03', 'BE', 'views', 47403);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5530, '2018-01-03', 'BE', 'plays', 20887);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5531, '2018-01-03', 'BE', 'clicks', 34970);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5535, '2018-01-03', 'BR', 'views', 17897);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5536, '2018-01-03', 'BR', 'plays', 15185);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5537, '2018-01-03', 'BR', 'clicks', 23248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5526, '2018-01-03', 'CA', 'views', 58503);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5527, '2018-01-03', 'CA', 'plays', 58657);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5528, '2018-01-03', 'CA', 'clicks', 22530);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5541, '2018-01-03', 'CY', 'views', 59212);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5542, '2018-01-03', 'CY', 'plays', 46287);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5543, '2018-01-03', 'CY', 'clicks', 59719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5553, '2018-01-03', 'DE', 'views', 57877);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5554, '2018-01-03', 'DE', 'plays', 74234);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5555, '2018-01-03', 'DE', 'clicks', 84168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5544, '2018-01-03', 'EG', 'views', 49200);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5545, '2018-01-03', 'EG', 'plays', 17219);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5546, '2018-01-03', 'EG', 'clicks', 60070);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5550, '2018-01-03', 'FR', 'views', 39271);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5551, '2018-01-03', 'FR', 'plays', 15695);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5552, '2018-01-03', 'FR', 'clicks', 48036);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5538, '2018-01-03', 'HR', 'views', 46788);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5539, '2018-01-03', 'HR', 'plays', 23708);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5540, '2018-01-03', 'HR', 'clicks', 53980);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5547, '2018-01-03', 'RS', 'views', 68039);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5548, '2018-01-03', 'RS', 'plays', 48657);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5549, '2018-01-03', 'RS', 'clicks', 44829);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5559, '2018-01-04', 'AT', 'views', 60561);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5560, '2018-01-04', 'AT', 'plays', 66694);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5561, '2018-01-04', 'AT', 'clicks', 32479);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5556, '2018-01-04', 'AU', 'views', 41572);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5557, '2018-01-04', 'AU', 'plays', 70847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5558, '2018-01-04', 'AU', 'clicks', 43780);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5568, '2018-01-04', 'BA', 'views', 19973);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5569, '2018-01-04', 'BA', 'plays', 41402);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5570, '2018-01-04', 'BA', 'clicks', 79246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5565, '2018-01-04', 'BE', 'views', 5443);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5566, '2018-01-04', 'BE', 'plays', 63595);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5567, '2018-01-04', 'BE', 'clicks', 42138);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5571, '2018-01-04', 'BR', 'views', 51577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5572, '2018-01-04', 'BR', 'plays', 44494);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5573, '2018-01-04', 'BR', 'clicks', 50600);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5562, '2018-01-04', 'CA', 'views', 19679);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5563, '2018-01-04', 'CA', 'plays', 61772);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5564, '2018-01-04', 'CA', 'clicks', 25734);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5577, '2018-01-04', 'CY', 'views', 36559);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5578, '2018-01-04', 'CY', 'plays', 69058);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5579, '2018-01-04', 'CY', 'clicks', 58399);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5589, '2018-01-04', 'DE', 'views', 57910);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5590, '2018-01-04', 'DE', 'plays', 54072);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5591, '2018-01-04', 'DE', 'clicks', 73303);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5580, '2018-01-04', 'EG', 'views', 42130);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5581, '2018-01-04', 'EG', 'plays', 43935);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5582, '2018-01-04', 'EG', 'clicks', 33600);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5586, '2018-01-04', 'FR', 'views', 23098);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5587, '2018-01-04', 'FR', 'plays', 59346);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5588, '2018-01-04', 'FR', 'clicks', 44703);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5574, '2018-01-04', 'HR', 'views', 83680);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5575, '2018-01-04', 'HR', 'plays', 37327);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5576, '2018-01-04', 'HR', 'clicks', 39696);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5583, '2018-01-04', 'RS', 'views', 49707);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5584, '2018-01-04', 'RS', 'plays', 51242);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5585, '2018-01-04', 'RS', 'clicks', 45605);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5595, '2018-01-05', 'AT', 'views', 83720);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5596, '2018-01-05', 'AT', 'plays', 69094);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5597, '2018-01-05', 'AT', 'clicks', 39438);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5592, '2018-01-05', 'AU', 'views', 75673);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5593, '2018-01-05', 'AU', 'plays', 88418);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5594, '2018-01-05', 'AU', 'clicks', 13639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5604, '2018-01-05', 'BA', 'views', 70001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5605, '2018-01-05', 'BA', 'plays', 55101);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5606, '2018-01-05', 'BA', 'clicks', 57417);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5601, '2018-01-05', 'BE', 'views', 74710);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5602, '2018-01-05', 'BE', 'plays', 9789);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5603, '2018-01-05', 'BE', 'clicks', 57370);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5607, '2018-01-05', 'BR', 'views', 54939);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5608, '2018-01-05', 'BR', 'plays', 21111);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5609, '2018-01-05', 'BR', 'clicks', 17652);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5598, '2018-01-05', 'CA', 'views', 57497);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5599, '2018-01-05', 'CA', 'plays', 28648);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5600, '2018-01-05', 'CA', 'clicks', 44477);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5613, '2018-01-05', 'CY', 'views', 13005);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5614, '2018-01-05', 'CY', 'plays', 70636);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5615, '2018-01-05', 'CY', 'clicks', 60523);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5625, '2018-01-05', 'DE', 'views', 71706);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5626, '2018-01-05', 'DE', 'plays', 67613);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5627, '2018-01-05', 'DE', 'clicks', 70961);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5616, '2018-01-05', 'EG', 'views', 27405);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5617, '2018-01-05', 'EG', 'plays', 84203);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5618, '2018-01-05', 'EG', 'clicks', 49844);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5622, '2018-01-05', 'FR', 'views', 49413);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5623, '2018-01-05', 'FR', 'plays', 52462);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5624, '2018-01-05', 'FR', 'clicks', 52399);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5610, '2018-01-05', 'HR', 'views', 61284);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5611, '2018-01-05', 'HR', 'plays', 44970);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5612, '2018-01-05', 'HR', 'clicks', 52627);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5619, '2018-01-05', 'RS', 'views', 57703);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5620, '2018-01-05', 'RS', 'plays', 8805);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5621, '2018-01-05', 'RS', 'clicks', 53038);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5631, '2018-01-06', 'AT', 'views', 63427);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5632, '2018-01-06', 'AT', 'plays', 45650);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5633, '2018-01-06', 'AT', 'clicks', 49178);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5628, '2018-01-06', 'AU', 'views', 12808);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5629, '2018-01-06', 'AU', 'plays', 52292);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5630, '2018-01-06', 'AU', 'clicks', 37200);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5640, '2018-01-06', 'BA', 'views', 44305);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5641, '2018-01-06', 'BA', 'plays', 58397);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5642, '2018-01-06', 'BA', 'clicks', 56841);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5637, '2018-01-06', 'BE', 'views', 33353);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5638, '2018-01-06', 'BE', 'plays', 32104);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5639, '2018-01-06', 'BE', 'clicks', 74472);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5643, '2018-01-06', 'BR', 'views', 54115);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5644, '2018-01-06', 'BR', 'plays', 46991);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5645, '2018-01-06', 'BR', 'clicks', 72566);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5634, '2018-01-06', 'CA', 'views', 43475);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5635, '2018-01-06', 'CA', 'plays', 14558);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5636, '2018-01-06', 'CA', 'clicks', 44415);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5649, '2018-01-06', 'CY', 'views', 34363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5650, '2018-01-06', 'CY', 'plays', 70546);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5651, '2018-01-06', 'CY', 'clicks', 72108);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5661, '2018-01-06', 'DE', 'views', 38220);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5662, '2018-01-06', 'DE', 'plays', 31505);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5663, '2018-01-06', 'DE', 'clicks', 52181);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5652, '2018-01-06', 'EG', 'views', 81427);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5653, '2018-01-06', 'EG', 'plays', 27518);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5654, '2018-01-06', 'EG', 'clicks', 25321);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5658, '2018-01-06', 'FR', 'views', 47013);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5659, '2018-01-06', 'FR', 'plays', 30431);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5660, '2018-01-06', 'FR', 'clicks', 25706);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5646, '2018-01-06', 'HR', 'views', 56363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5647, '2018-01-06', 'HR', 'plays', 65809);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5648, '2018-01-06', 'HR', 'clicks', 36488);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5655, '2018-01-06', 'RS', 'views', 55268);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5656, '2018-01-06', 'RS', 'plays', 53659);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5657, '2018-01-06', 'RS', 'clicks', 53361);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5667, '2018-01-07', 'AT', 'views', 25527);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5668, '2018-01-07', 'AT', 'plays', 62993);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5669, '2018-01-07', 'AT', 'clicks', 59958);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5664, '2018-01-07', 'AU', 'views', 31721);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5665, '2018-01-07', 'AU', 'plays', 30417);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5666, '2018-01-07', 'AU', 'clicks', 22051);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5676, '2018-01-07', 'BA', 'views', 24778);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5677, '2018-01-07', 'BA', 'plays', 44531);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5678, '2018-01-07', 'BA', 'clicks', 79393);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5673, '2018-01-07', 'BE', 'views', 17986);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5674, '2018-01-07', 'BE', 'plays', 52691);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5675, '2018-01-07', 'BE', 'clicks', 50874);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5679, '2018-01-07', 'BR', 'views', 60060);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5680, '2018-01-07', 'BR', 'plays', 62021);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5681, '2018-01-07', 'BR', 'clicks', 86467);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5670, '2018-01-07', 'CA', 'views', 51482);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5671, '2018-01-07', 'CA', 'plays', 30049);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5672, '2018-01-07', 'CA', 'clicks', 69084);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5685, '2018-01-07', 'CY', 'views', 65254);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5686, '2018-01-07', 'CY', 'plays', 64817);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5687, '2018-01-07', 'CY', 'clicks', 34670);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5697, '2018-01-07', 'DE', 'views', 61895);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5698, '2018-01-07', 'DE', 'plays', 57038);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5699, '2018-01-07', 'DE', 'clicks', 48506);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5688, '2018-01-07', 'EG', 'views', 38481);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5689, '2018-01-07', 'EG', 'plays', 16299);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5690, '2018-01-07', 'EG', 'clicks', 36504);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5694, '2018-01-07', 'FR', 'views', 51404);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5695, '2018-01-07', 'FR', 'plays', 46400);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5696, '2018-01-07', 'FR', 'clicks', 30176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5682, '2018-01-07', 'HR', 'views', 49341);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5683, '2018-01-07', 'HR', 'plays', 23644);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5684, '2018-01-07', 'HR', 'clicks', 84530);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5691, '2018-01-07', 'RS', 'views', 94040);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5692, '2018-01-07', 'RS', 'plays', 94506);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5693, '2018-01-07', 'RS', 'clicks', 73718);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5703, '2018-01-08', 'AT', 'views', 45807);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5704, '2018-01-08', 'AT', 'plays', 64446);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5705, '2018-01-08', 'AT', 'clicks', 50661);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5700, '2018-01-08', 'AU', 'views', 45401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5701, '2018-01-08', 'AU', 'plays', 13164);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5702, '2018-01-08', 'AU', 'clicks', 61143);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5712, '2018-01-08', 'BA', 'views', 36659);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5713, '2018-01-08', 'BA', 'plays', 58162);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5714, '2018-01-08', 'BA', 'clicks', 50799);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5709, '2018-01-08', 'BE', 'views', 58059);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5710, '2018-01-08', 'BE', 'plays', 20363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5711, '2018-01-08', 'BE', 'clicks', 66704);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5715, '2018-01-08', 'BR', 'views', 73767);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5716, '2018-01-08', 'BR', 'plays', 28234);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5717, '2018-01-08', 'BR', 'clicks', 49283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5706, '2018-01-08', 'CA', 'views', 88659);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5707, '2018-01-08', 'CA', 'plays', 34917);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5708, '2018-01-08', 'CA', 'clicks', 40426);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5721, '2018-01-08', 'CY', 'views', 56928);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5722, '2018-01-08', 'CY', 'plays', 43025);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5723, '2018-01-08', 'CY', 'clicks', 22319);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5733, '2018-01-08', 'DE', 'views', 11558);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5734, '2018-01-08', 'DE', 'plays', 21538);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5735, '2018-01-08', 'DE', 'clicks', 72544);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5724, '2018-01-08', 'EG', 'views', 52757);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5725, '2018-01-08', 'EG', 'plays', 74262);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5726, '2018-01-08', 'EG', 'clicks', 36653);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5730, '2018-01-08', 'FR', 'views', 52247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5731, '2018-01-08', 'FR', 'plays', 50684);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5732, '2018-01-08', 'FR', 'clicks', 71143);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5718, '2018-01-08', 'HR', 'views', 38725);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5719, '2018-01-08', 'HR', 'plays', 59646);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5720, '2018-01-08', 'HR', 'clicks', 59362);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5727, '2018-01-08', 'RS', 'views', 56087);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5728, '2018-01-08', 'RS', 'plays', 36967);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5729, '2018-01-08', 'RS', 'clicks', 49845);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5739, '2018-01-09', 'AT', 'views', 63818);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5740, '2018-01-09', 'AT', 'plays', 57961);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5741, '2018-01-09', 'AT', 'clicks', 87708);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5736, '2018-01-09', 'AU', 'views', 75898);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5737, '2018-01-09', 'AU', 'plays', 55237);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5738, '2018-01-09', 'AU', 'clicks', 20069);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5748, '2018-01-09', 'BA', 'views', 30338);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5749, '2018-01-09', 'BA', 'plays', 23006);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5750, '2018-01-09', 'BA', 'clicks', 57983);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5745, '2018-01-09', 'BE', 'views', 45950);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5746, '2018-01-09', 'BE', 'plays', 49190);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5747, '2018-01-09', 'BE', 'clicks', 42277);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5751, '2018-01-09', 'BR', 'views', 33182);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5752, '2018-01-09', 'BR', 'plays', 48518);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5753, '2018-01-09', 'BR', 'clicks', 44368);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5742, '2018-01-09', 'CA', 'views', 71158);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5743, '2018-01-09', 'CA', 'plays', 71749);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5744, '2018-01-09', 'CA', 'clicks', 51505);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5757, '2018-01-09', 'CY', 'views', 26558);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5758, '2018-01-09', 'CY', 'plays', 32760);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5759, '2018-01-09', 'CY', 'clicks', 21548);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5769, '2018-01-09', 'DE', 'views', 56083);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5770, '2018-01-09', 'DE', 'plays', 45559);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5771, '2018-01-09', 'DE', 'clicks', 38918);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5760, '2018-01-09', 'EG', 'views', 67952);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5761, '2018-01-09', 'EG', 'plays', 53124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5762, '2018-01-09', 'EG', 'clicks', 68391);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5766, '2018-01-09', 'FR', 'views', 38849);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5767, '2018-01-09', 'FR', 'plays', 64588);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5768, '2018-01-09', 'FR', 'clicks', 80609);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5754, '2018-01-09', 'HR', 'views', 35215);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5755, '2018-01-09', 'HR', 'plays', 54134);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5756, '2018-01-09', 'HR', 'clicks', 45016);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5763, '2018-01-09', 'RS', 'views', 46974);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5764, '2018-01-09', 'RS', 'plays', 41130);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5765, '2018-01-09', 'RS', 'clicks', 58286);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5775, '2018-01-10', 'AT', 'views', 52792);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5776, '2018-01-10', 'AT', 'plays', 41363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5777, '2018-01-10', 'AT', 'clicks', 24417);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5772, '2018-01-10', 'AU', 'views', 13245);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5773, '2018-01-10', 'AU', 'plays', 79704);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5774, '2018-01-10', 'AU', 'clicks', 58639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5784, '2018-01-10', 'BA', 'views', 53288);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5785, '2018-01-10', 'BA', 'plays', 54178);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5786, '2018-01-10', 'BA', 'clicks', 38174);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5781, '2018-01-10', 'BE', 'views', 14931);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5782, '2018-01-10', 'BE', 'plays', 68293);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5783, '2018-01-10', 'BE', 'clicks', 47432);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5787, '2018-01-10', 'BR', 'views', 9950);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5788, '2018-01-10', 'BR', 'plays', 54245);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5789, '2018-01-10', 'BR', 'clicks', 43893);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5778, '2018-01-10', 'CA', 'views', 83066);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5779, '2018-01-10', 'CA', 'plays', 59309);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5780, '2018-01-10', 'CA', 'clicks', 42215);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5793, '2018-01-10', 'CY', 'views', 64077);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5794, '2018-01-10', 'CY', 'plays', 30004);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5795, '2018-01-10', 'CY', 'clicks', 88271);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5805, '2018-01-10', 'DE', 'views', 16797);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5806, '2018-01-10', 'DE', 'plays', 20085);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5807, '2018-01-10', 'DE', 'clicks', 43802);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5796, '2018-01-10', 'EG', 'views', 61205);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5797, '2018-01-10', 'EG', 'plays', 67786);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5798, '2018-01-10', 'EG', 'clicks', 55747);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5802, '2018-01-10', 'FR', 'views', 31926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5803, '2018-01-10', 'FR', 'plays', 71342);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5804, '2018-01-10', 'FR', 'clicks', 14524);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5790, '2018-01-10', 'HR', 'views', 65745);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5791, '2018-01-10', 'HR', 'plays', 26234);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5792, '2018-01-10', 'HR', 'clicks', 60093);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5799, '2018-01-10', 'RS', 'views', 47535);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5800, '2018-01-10', 'RS', 'plays', 54908);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5801, '2018-01-10', 'RS', 'clicks', 50432);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5811, '2018-01-11', 'AT', 'views', 62716);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5812, '2018-01-11', 'AT', 'plays', 30294);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5813, '2018-01-11', 'AT', 'clicks', 42384);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5808, '2018-01-11', 'AU', 'views', 70537);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5809, '2018-01-11', 'AU', 'plays', 66587);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5810, '2018-01-11', 'AU', 'clicks', 30736);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5820, '2018-01-11', 'BA', 'views', 60884);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5821, '2018-01-11', 'BA', 'plays', 47301);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5822, '2018-01-11', 'BA', 'clicks', 15458);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5817, '2018-01-11', 'BE', 'views', 26913);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5818, '2018-01-11', 'BE', 'plays', 50509);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5819, '2018-01-11', 'BE', 'clicks', 42233);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5823, '2018-01-11', 'BR', 'views', 68310);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5824, '2018-01-11', 'BR', 'plays', 30423);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5825, '2018-01-11', 'BR', 'clicks', 38360);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5814, '2018-01-11', 'CA', 'views', 34915);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5815, '2018-01-11', 'CA', 'plays', 19034);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5816, '2018-01-11', 'CA', 'clicks', 77176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5829, '2018-01-11', 'CY', 'views', 48093);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5830, '2018-01-11', 'CY', 'plays', 47603);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5831, '2018-01-11', 'CY', 'clicks', 41577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5841, '2018-01-11', 'DE', 'views', 46231);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5842, '2018-01-11', 'DE', 'plays', 56133);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5843, '2018-01-11', 'DE', 'clicks', 89555);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5832, '2018-01-11', 'EG', 'views', 62252);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5833, '2018-01-11', 'EG', 'plays', 76747);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5834, '2018-01-11', 'EG', 'clicks', 46979);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5838, '2018-01-11', 'FR', 'views', 53949);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5839, '2018-01-11', 'FR', 'plays', 27412);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5840, '2018-01-11', 'FR', 'clicks', 60480);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5826, '2018-01-11', 'HR', 'views', 70624);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5827, '2018-01-11', 'HR', 'plays', 28576);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5828, '2018-01-11', 'HR', 'clicks', 62737);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5835, '2018-01-11', 'RS', 'views', 70398);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5836, '2018-01-11', 'RS', 'plays', 58497);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5837, '2018-01-11', 'RS', 'clicks', 31175);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5847, '2018-01-12', 'AT', 'views', 24702);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5848, '2018-01-12', 'AT', 'plays', 18666);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5849, '2018-01-12', 'AT', 'clicks', 65979);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5844, '2018-01-12', 'AU', 'views', 77154);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5845, '2018-01-12', 'AU', 'plays', 77859);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5846, '2018-01-12', 'AU', 'clicks', 83912);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5856, '2018-01-12', 'BA', 'views', 8243);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5857, '2018-01-12', 'BA', 'plays', 66294);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5858, '2018-01-12', 'BA', 'clicks', 34806);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5853, '2018-01-12', 'BE', 'views', 51144);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5854, '2018-01-12', 'BE', 'plays', 68309);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5855, '2018-01-12', 'BE', 'clicks', 27725);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5859, '2018-01-12', 'BR', 'views', 45393);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5860, '2018-01-12', 'BR', 'plays', 45628);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5861, '2018-01-12', 'BR', 'clicks', 64046);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5850, '2018-01-12', 'CA', 'views', 57094);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5851, '2018-01-12', 'CA', 'plays', 82177);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5852, '2018-01-12', 'CA', 'clicks', 34992);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5865, '2018-01-12', 'CY', 'views', 16592);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5866, '2018-01-12', 'CY', 'plays', 39401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5867, '2018-01-12', 'CY', 'clicks', 61316);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5877, '2018-01-12', 'DE', 'views', 89826);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5878, '2018-01-12', 'DE', 'plays', 42741);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5879, '2018-01-12', 'DE', 'clicks', 92059);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5868, '2018-01-12', 'EG', 'views', 14920);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5869, '2018-01-12', 'EG', 'plays', 74082);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5870, '2018-01-12', 'EG', 'clicks', 72991);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5874, '2018-01-12', 'FR', 'views', 77898);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5875, '2018-01-12', 'FR', 'plays', 33941);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5876, '2018-01-12', 'FR', 'clicks', 57810);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5862, '2018-01-12', 'HR', 'views', 30005);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5863, '2018-01-12', 'HR', 'plays', 21791);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5864, '2018-01-12', 'HR', 'clicks', 7325);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5871, '2018-01-12', 'RS', 'views', 61040);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5872, '2018-01-12', 'RS', 'plays', 57839);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5873, '2018-01-12', 'RS', 'clicks', 66670);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5883, '2018-01-13', 'AT', 'views', 62846);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5884, '2018-01-13', 'AT', 'plays', 14424);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5885, '2018-01-13', 'AT', 'clicks', 39521);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5880, '2018-01-13', 'AU', 'views', 82341);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5881, '2018-01-13', 'AU', 'plays', 48766);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5882, '2018-01-13', 'AU', 'clicks', 17829);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5892, '2018-01-13', 'BA', 'views', 39883);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5893, '2018-01-13', 'BA', 'plays', 65535);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5894, '2018-01-13', 'BA', 'clicks', 64757);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5889, '2018-01-13', 'BE', 'views', 64490);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5890, '2018-01-13', 'BE', 'plays', 18014);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5891, '2018-01-13', 'BE', 'clicks', 64803);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5895, '2018-01-13', 'BR', 'views', 18382);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5896, '2018-01-13', 'BR', 'plays', 36623);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5897, '2018-01-13', 'BR', 'clicks', 38360);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5886, '2018-01-13', 'CA', 'views', 79010);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5887, '2018-01-13', 'CA', 'plays', 58689);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5888, '2018-01-13', 'CA', 'clicks', 72171);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5901, '2018-01-13', 'CY', 'views', 69879);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5902, '2018-01-13', 'CY', 'plays', 93476);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5903, '2018-01-13', 'CY', 'clicks', 68850);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5913, '2018-01-13', 'DE', 'views', 68293);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5914, '2018-01-13', 'DE', 'plays', 40846);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5915, '2018-01-13', 'DE', 'clicks', 58658);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5904, '2018-01-13', 'EG', 'views', 33720);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5905, '2018-01-13', 'EG', 'plays', 92955);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5906, '2018-01-13', 'EG', 'clicks', 45916);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5910, '2018-01-13', 'FR', 'views', 27384);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5911, '2018-01-13', 'FR', 'plays', 43522);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5912, '2018-01-13', 'FR', 'clicks', 30795);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5898, '2018-01-13', 'HR', 'views', 53323);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5899, '2018-01-13', 'HR', 'plays', 36122);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5900, '2018-01-13', 'HR', 'clicks', 20885);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5907, '2018-01-13', 'RS', 'views', 54504);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5908, '2018-01-13', 'RS', 'plays', 52355);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5909, '2018-01-13', 'RS', 'clicks', 29870);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5919, '2018-01-14', 'AT', 'views', 44292);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5920, '2018-01-14', 'AT', 'plays', 29930);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5921, '2018-01-14', 'AT', 'clicks', 68528);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5916, '2018-01-14', 'AU', 'views', 39940);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5917, '2018-01-14', 'AU', 'plays', 58467);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5918, '2018-01-14', 'AU', 'clicks', 55610);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5928, '2018-01-14', 'BA', 'views', 11833);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5929, '2018-01-14', 'BA', 'plays', 53862);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5930, '2018-01-14', 'BA', 'clicks', 51188);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5925, '2018-01-14', 'BE', 'views', 15577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5926, '2018-01-14', 'BE', 'plays', 35418);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5927, '2018-01-14', 'BE', 'clicks', 40794);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5931, '2018-01-14', 'BR', 'views', 43784);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5932, '2018-01-14', 'BR', 'plays', 82193);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5933, '2018-01-14', 'BR', 'clicks', 12907);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5922, '2018-01-14', 'CA', 'views', 47698);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5923, '2018-01-14', 'CA', 'plays', 26628);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5924, '2018-01-14', 'CA', 'clicks', 31947);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5937, '2018-01-14', 'CY', 'views', 61144);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5938, '2018-01-14', 'CY', 'plays', 17287);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5939, '2018-01-14', 'CY', 'clicks', 40515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5949, '2018-01-14', 'DE', 'views', 41247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5950, '2018-01-14', 'DE', 'plays', 49313);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5951, '2018-01-14', 'DE', 'clicks', 38483);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5940, '2018-01-14', 'EG', 'views', 67695);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5941, '2018-01-14', 'EG', 'plays', 68445);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5942, '2018-01-14', 'EG', 'clicks', 37004);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5946, '2018-01-14', 'FR', 'views', 44742);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5947, '2018-01-14', 'FR', 'plays', 29529);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5948, '2018-01-14', 'FR', 'clicks', 58026);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5934, '2018-01-14', 'HR', 'views', 41393);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5935, '2018-01-14', 'HR', 'plays', 50257);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5936, '2018-01-14', 'HR', 'clicks', 66395);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5943, '2018-01-14', 'RS', 'views', 60013);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5944, '2018-01-14', 'RS', 'plays', 94159);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5945, '2018-01-14', 'RS', 'clicks', 34035);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5955, '2018-01-15', 'AT', 'views', 95982);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5956, '2018-01-15', 'AT', 'plays', 19312);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5957, '2018-01-15', 'AT', 'clicks', 25364);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5952, '2018-01-15', 'AU', 'views', 55155);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5953, '2018-01-15', 'AU', 'plays', 22256);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5954, '2018-01-15', 'AU', 'clicks', 41176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5964, '2018-01-15', 'BA', 'views', 47862);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5965, '2018-01-15', 'BA', 'plays', 40698);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5966, '2018-01-15', 'BA', 'clicks', 50416);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5961, '2018-01-15', 'BE', 'views', 37012);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5962, '2018-01-15', 'BE', 'plays', 46302);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5963, '2018-01-15', 'BE', 'clicks', 43099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5967, '2018-01-15', 'BR', 'views', 15548);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5968, '2018-01-15', 'BR', 'plays', 64289);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5969, '2018-01-15', 'BR', 'clicks', 72972);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5958, '2018-01-15', 'CA', 'views', 54695);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5959, '2018-01-15', 'CA', 'plays', 25548);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5960, '2018-01-15', 'CA', 'clicks', 21000);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5973, '2018-01-15', 'CY', 'views', 66199);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5974, '2018-01-15', 'CY', 'plays', 78175);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5975, '2018-01-15', 'CY', 'clicks', 74374);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5985, '2018-01-15', 'DE', 'views', 54678);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5986, '2018-01-15', 'DE', 'plays', 51206);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5987, '2018-01-15', 'DE', 'clicks', 75604);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5976, '2018-01-15', 'EG', 'views', 39821);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5977, '2018-01-15', 'EG', 'plays', 57620);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5978, '2018-01-15', 'EG', 'clicks', 48811);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5982, '2018-01-15', 'FR', 'views', 18615);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5983, '2018-01-15', 'FR', 'plays', 64558);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5984, '2018-01-15', 'FR', 'clicks', 40212);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5970, '2018-01-15', 'HR', 'views', 4057);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5971, '2018-01-15', 'HR', 'plays', 49884);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5972, '2018-01-15', 'HR', 'clicks', 57587);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5979, '2018-01-15', 'RS', 'views', 54112);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5980, '2018-01-15', 'RS', 'plays', 29420);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5981, '2018-01-15', 'RS', 'clicks', 79550);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5991, '2018-01-16', 'AT', 'views', 47840);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5992, '2018-01-16', 'AT', 'plays', 40392);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5993, '2018-01-16', 'AT', 'clicks', 62262);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5988, '2018-01-16', 'AU', 'views', 81891);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5989, '2018-01-16', 'AU', 'plays', 37314);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5990, '2018-01-16', 'AU', 'clicks', 49902);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6000, '2018-01-16', 'BA', 'views', 50998);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6001, '2018-01-16', 'BA', 'plays', 84544);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6002, '2018-01-16', 'BA', 'clicks', 71465);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5997, '2018-01-16', 'BE', 'views', 29746);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5998, '2018-01-16', 'BE', 'plays', 19062);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5999, '2018-01-16', 'BE', 'clicks', 60636);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6003, '2018-01-16', 'BR', 'views', 46564);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6004, '2018-01-16', 'BR', 'plays', 52250);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6005, '2018-01-16', 'BR', 'clicks', 24556);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5994, '2018-01-16', 'CA', 'views', 49955);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5995, '2018-01-16', 'CA', 'plays', 15373);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(5996, '2018-01-16', 'CA', 'clicks', 10259);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6009, '2018-01-16', 'CY', 'views', 52833);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6010, '2018-01-16', 'CY', 'plays', 71560);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6011, '2018-01-16', 'CY', 'clicks', 47487);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6021, '2018-01-16', 'DE', 'views', 58962);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6022, '2018-01-16', 'DE', 'plays', 48603);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6023, '2018-01-16', 'DE', 'clicks', 56469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6012, '2018-01-16', 'EG', 'views', 64605);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6013, '2018-01-16', 'EG', 'plays', 49160);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6014, '2018-01-16', 'EG', 'clicks', 36036);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6018, '2018-01-16', 'FR', 'views', 82567);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6019, '2018-01-16', 'FR', 'plays', 55765);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6020, '2018-01-16', 'FR', 'clicks', 87331);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6006, '2018-01-16', 'HR', 'views', 37578);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6007, '2018-01-16', 'HR', 'plays', 42702);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6008, '2018-01-16', 'HR', 'clicks', 57451);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6015, '2018-01-16', 'RS', 'views', 30088);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6016, '2018-01-16', 'RS', 'plays', 18178);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6017, '2018-01-16', 'RS', 'clicks', 35966);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6027, '2018-01-17', 'AT', 'views', 90813);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6028, '2018-01-17', 'AT', 'plays', 12826);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6029, '2018-01-17', 'AT', 'clicks', 60167);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6024, '2018-01-17', 'AU', 'views', 38669);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6025, '2018-01-17', 'AU', 'plays', 52180);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6026, '2018-01-17', 'AU', 'clicks', 50983);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6036, '2018-01-17', 'BA', 'views', 74324);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6037, '2018-01-17', 'BA', 'plays', 34458);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6038, '2018-01-17', 'BA', 'clicks', 61140);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6033, '2018-01-17', 'BE', 'views', 36942);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6034, '2018-01-17', 'BE', 'plays', 82667);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6035, '2018-01-17', 'BE', 'clicks', 50367);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6039, '2018-01-17', 'BR', 'views', 31493);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6040, '2018-01-17', 'BR', 'plays', 25538);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6041, '2018-01-17', 'BR', 'clicks', 68692);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6030, '2018-01-17', 'CA', 'views', 56555);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6031, '2018-01-17', 'CA', 'plays', 59371);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6032, '2018-01-17', 'CA', 'clicks', 37443);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6045, '2018-01-17', 'CY', 'views', 46143);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6046, '2018-01-17', 'CY', 'plays', 82561);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6047, '2018-01-17', 'CY', 'clicks', 42925);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6057, '2018-01-17', 'DE', 'views', 67117);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6058, '2018-01-17', 'DE', 'plays', 39712);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6059, '2018-01-17', 'DE', 'clicks', 71633);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6048, '2018-01-17', 'EG', 'views', 67750);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6049, '2018-01-17', 'EG', 'plays', 99068);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6050, '2018-01-17', 'EG', 'clicks', 52889);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6054, '2018-01-17', 'FR', 'views', 25674);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6055, '2018-01-17', 'FR', 'plays', 42156);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6056, '2018-01-17', 'FR', 'clicks', 39756);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6042, '2018-01-17', 'HR', 'views', 39426);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6043, '2018-01-17', 'HR', 'plays', 30116);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6044, '2018-01-17', 'HR', 'clicks', 47502);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6051, '2018-01-17', 'RS', 'views', 47203);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6052, '2018-01-17', 'RS', 'plays', 62008);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6053, '2018-01-17', 'RS', 'clicks', 37346);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6063, '2018-01-18', 'AT', 'views', 77302);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6064, '2018-01-18', 'AT', 'plays', 41396);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6065, '2018-01-18', 'AT', 'clicks', 59490);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6060, '2018-01-18', 'AU', 'views', 38610);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6061, '2018-01-18', 'AU', 'plays', 36276);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6062, '2018-01-18', 'AU', 'clicks', 48249);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6072, '2018-01-18', 'BA', 'views', 48165);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6073, '2018-01-18', 'BA', 'plays', 35906);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6074, '2018-01-18', 'BA', 'clicks', 54719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6069, '2018-01-18', 'BE', 'views', 47887);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6070, '2018-01-18', 'BE', 'plays', 47457);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6071, '2018-01-18', 'BE', 'clicks', 59859);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6075, '2018-01-18', 'BR', 'views', 63305);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6076, '2018-01-18', 'BR', 'plays', 36097);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6077, '2018-01-18', 'BR', 'clicks', 71062);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6066, '2018-01-18', 'CA', 'views', 21498);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6067, '2018-01-18', 'CA', 'plays', 46059);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6068, '2018-01-18', 'CA', 'clicks', 72140);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6081, '2018-01-18', 'CY', 'views', 94478);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6082, '2018-01-18', 'CY', 'plays', 21010);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6083, '2018-01-18', 'CY', 'clicks', 31315);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6093, '2018-01-18', 'DE', 'views', 33041);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6094, '2018-01-18', 'DE', 'plays', 43365);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6095, '2018-01-18', 'DE', 'clicks', 33898);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6084, '2018-01-18', 'EG', 'views', 64883);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6085, '2018-01-18', 'EG', 'plays', 85751);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6086, '2018-01-18', 'EG', 'clicks', 37559);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6090, '2018-01-18', 'FR', 'views', 4584);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6091, '2018-01-18', 'FR', 'plays', 44027);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6092, '2018-01-18', 'FR', 'clicks', 66360);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6078, '2018-01-18', 'HR', 'views', 53542);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6079, '2018-01-18', 'HR', 'plays', 51631);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6080, '2018-01-18', 'HR', 'clicks', 88538);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6087, '2018-01-18', 'RS', 'views', 24562);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6088, '2018-01-18', 'RS', 'plays', 41713);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6089, '2018-01-18', 'RS', 'clicks', 67308);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6099, '2018-01-19', 'AT', 'views', 66294);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6100, '2018-01-19', 'AT', 'plays', 35215);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6101, '2018-01-19', 'AT', 'clicks', 62968);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6096, '2018-01-19', 'AU', 'views', 42599);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6097, '2018-01-19', 'AU', 'plays', 50013);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6098, '2018-01-19', 'AU', 'clicks', 77839);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6108, '2018-01-19', 'BA', 'views', 58774);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6109, '2018-01-19', 'BA', 'plays', 21046);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6110, '2018-01-19', 'BA', 'clicks', 36089);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6105, '2018-01-19', 'BE', 'views', 35140);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6106, '2018-01-19', 'BE', 'plays', 75518);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6107, '2018-01-19', 'BE', 'clicks', 50600);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6111, '2018-01-19', 'BR', 'views', 11116);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6112, '2018-01-19', 'BR', 'plays', 47805);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6113, '2018-01-19', 'BR', 'clicks', 25803);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6102, '2018-01-19', 'CA', 'views', 38772);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6103, '2018-01-19', 'CA', 'plays', 74021);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6104, '2018-01-19', 'CA', 'clicks', 59841);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6117, '2018-01-19', 'CY', 'views', 73004);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6118, '2018-01-19', 'CY', 'plays', 25815);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6119, '2018-01-19', 'CY', 'clicks', 49806);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6129, '2018-01-19', 'DE', 'views', 73339);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6130, '2018-01-19', 'DE', 'plays', 58553);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6131, '2018-01-19', 'DE', 'clicks', 59868);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6120, '2018-01-19', 'EG', 'views', 33384);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6121, '2018-01-19', 'EG', 'plays', 52586);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6122, '2018-01-19', 'EG', 'clicks', 24046);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6126, '2018-01-19', 'FR', 'views', 18726);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6127, '2018-01-19', 'FR', 'plays', 90124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6128, '2018-01-19', 'FR', 'clicks', 38104);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6114, '2018-01-19', 'HR', 'views', 57467);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6115, '2018-01-19', 'HR', 'plays', 59216);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6116, '2018-01-19', 'HR', 'clicks', 95566);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6123, '2018-01-19', 'RS', 'views', 53428);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6124, '2018-01-19', 'RS', 'plays', 47473);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6125, '2018-01-19', 'RS', 'clicks', 47585);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6135, '2018-01-20', 'AT', 'views', 9382);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6136, '2018-01-20', 'AT', 'plays', 52375);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6137, '2018-01-20', 'AT', 'clicks', 44734);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6132, '2018-01-20', 'AU', 'views', 62860);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6133, '2018-01-20', 'AU', 'plays', 36721);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6134, '2018-01-20', 'AU', 'clicks', 19584);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6144, '2018-01-20', 'BA', 'views', 67320);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6145, '2018-01-20', 'BA', 'plays', 43017);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6146, '2018-01-20', 'BA', 'clicks', 18747);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6141, '2018-01-20', 'BE', 'views', 54678);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6142, '2018-01-20', 'BE', 'plays', 60986);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6143, '2018-01-20', 'BE', 'clicks', 45297);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6147, '2018-01-20', 'BR', 'views', 41471);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6148, '2018-01-20', 'BR', 'plays', 59574);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6149, '2018-01-20', 'BR', 'clicks', 74124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6138, '2018-01-20', 'CA', 'views', 33008);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6139, '2018-01-20', 'CA', 'plays', 39350);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6140, '2018-01-20', 'CA', 'clicks', 61632);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6153, '2018-01-20', 'CY', 'views', 32889);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6154, '2018-01-20', 'CY', 'plays', 46557);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6155, '2018-01-20', 'CY', 'clicks', 29273);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6165, '2018-01-20', 'DE', 'views', 22146);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6166, '2018-01-20', 'DE', 'plays', 75371);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6167, '2018-01-20', 'DE', 'clicks', 52922);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6156, '2018-01-20', 'EG', 'views', 48325);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6157, '2018-01-20', 'EG', 'plays', 39085);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6158, '2018-01-20', 'EG', 'clicks', 60393);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6162, '2018-01-20', 'FR', 'views', 70529);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6163, '2018-01-20', 'FR', 'plays', 63763);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6164, '2018-01-20', 'FR', 'clicks', 41239);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6150, '2018-01-20', 'HR', 'views', 86813);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6151, '2018-01-20', 'HR', 'plays', 73757);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6152, '2018-01-20', 'HR', 'clicks', 41849);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6159, '2018-01-20', 'RS', 'views', 28382);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6160, '2018-01-20', 'RS', 'plays', 34313);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6161, '2018-01-20', 'RS', 'clicks', 91585);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6171, '2018-01-21', 'AT', 'views', 67185);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6172, '2018-01-21', 'AT', 'plays', 66167);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6173, '2018-01-21', 'AT', 'clicks', 68597);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6168, '2018-01-21', 'AU', 'views', 12734);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6169, '2018-01-21', 'AU', 'plays', 39128);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6170, '2018-01-21', 'AU', 'clicks', 68560);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6180, '2018-01-21', 'BA', 'views', 57560);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6181, '2018-01-21', 'BA', 'plays', 37990);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6182, '2018-01-21', 'BA', 'clicks', 33687);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6177, '2018-01-21', 'BE', 'views', 54505);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6178, '2018-01-21', 'BE', 'plays', 78363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6179, '2018-01-21', 'BE', 'clicks', 18133);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6183, '2018-01-21', 'BR', 'views', 50500);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6184, '2018-01-21', 'BR', 'plays', 44251);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6185, '2018-01-21', 'BR', 'clicks', 74221);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6174, '2018-01-21', 'CA', 'views', 59015);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6175, '2018-01-21', 'CA', 'plays', 54652);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6176, '2018-01-21', 'CA', 'clicks', 55031);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6189, '2018-01-21', 'CY', 'views', 21024);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6190, '2018-01-21', 'CY', 'plays', 50502);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6191, '2018-01-21', 'CY', 'clicks', 37896);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6201, '2018-01-21', 'DE', 'views', 45403);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6202, '2018-01-21', 'DE', 'plays', 56540);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6203, '2018-01-21', 'DE', 'clicks', 27452);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6192, '2018-01-21', 'EG', 'views', 58646);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6193, '2018-01-21', 'EG', 'plays', 38463);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6194, '2018-01-21', 'EG', 'clicks', 27785);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6198, '2018-01-21', 'FR', 'views', 65110);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6199, '2018-01-21', 'FR', 'plays', 81038);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6200, '2018-01-21', 'FR', 'clicks', 13146);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6186, '2018-01-21', 'HR', 'views', 36808);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6187, '2018-01-21', 'HR', 'plays', 86613);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6188, '2018-01-21', 'HR', 'clicks', 71705);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6195, '2018-01-21', 'RS', 'views', 55614);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6196, '2018-01-21', 'RS', 'plays', 78865);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6197, '2018-01-21', 'RS', 'clicks', 61137);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6207, '2018-01-22', 'AT', 'views', 22388);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6208, '2018-01-22', 'AT', 'plays', 54850);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6209, '2018-01-22', 'AT', 'clicks', 18349);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6204, '2018-01-22', 'AU', 'views', 38037);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6205, '2018-01-22', 'AU', 'plays', 46189);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6206, '2018-01-22', 'AU', 'clicks', 27780);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6216, '2018-01-22', 'BA', 'views', 67653);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6217, '2018-01-22', 'BA', 'plays', 77194);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6218, '2018-01-22', 'BA', 'clicks', 49909);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6213, '2018-01-22', 'BE', 'views', 61164);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6214, '2018-01-22', 'BE', 'plays', 77666);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6215, '2018-01-22', 'BE', 'clicks', 79472);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6219, '2018-01-22', 'BR', 'views', 29163);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6220, '2018-01-22', 'BR', 'plays', 29380);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6221, '2018-01-22', 'BR', 'clicks', 54754);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6210, '2018-01-22', 'CA', 'views', 73295);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6211, '2018-01-22', 'CA', 'plays', 30111);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6212, '2018-01-22', 'CA', 'clicks', 23520);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6225, '2018-01-22', 'CY', 'views', 58687);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6226, '2018-01-22', 'CY', 'plays', 12509);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6227, '2018-01-22', 'CY', 'clicks', 77604);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6237, '2018-01-22', 'DE', 'views', 46235);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6238, '2018-01-22', 'DE', 'plays', 47745);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6239, '2018-01-22', 'DE', 'clicks', 26617);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6228, '2018-01-22', 'EG', 'views', 54688);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6229, '2018-01-22', 'EG', 'plays', 41845);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6230, '2018-01-22', 'EG', 'clicks', 24160);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6234, '2018-01-22', 'FR', 'views', 77425);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6235, '2018-01-22', 'FR', 'plays', 83669);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6236, '2018-01-22', 'FR', 'clicks', 70154);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6222, '2018-01-22', 'HR', 'views', 17721);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6223, '2018-01-22', 'HR', 'plays', 37402);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6224, '2018-01-22', 'HR', 'clicks', 60393);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6231, '2018-01-22', 'RS', 'views', 59709);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6232, '2018-01-22', 'RS', 'plays', 34750);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6233, '2018-01-22', 'RS', 'clicks', 17291);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6243, '2018-01-23', 'AT', 'views', 32128);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6244, '2018-01-23', 'AT', 'plays', 47792);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6245, '2018-01-23', 'AT', 'clicks', 55776);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6240, '2018-01-23', 'AU', 'views', 51934);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6241, '2018-01-23', 'AU', 'plays', 13954);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6242, '2018-01-23', 'AU', 'clicks', 66138);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6252, '2018-01-23', 'BA', 'views', 34680);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6253, '2018-01-23', 'BA', 'plays', 44322);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6254, '2018-01-23', 'BA', 'clicks', 50516);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6249, '2018-01-23', 'BE', 'views', 76403);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6250, '2018-01-23', 'BE', 'plays', 62517);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6251, '2018-01-23', 'BE', 'clicks', 14121);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6255, '2018-01-23', 'BR', 'views', 89737);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6256, '2018-01-23', 'BR', 'plays', 29503);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6257, '2018-01-23', 'BR', 'clicks', 52208);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6246, '2018-01-23', 'CA', 'views', 21886);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6247, '2018-01-23', 'CA', 'plays', 2596);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6248, '2018-01-23', 'CA', 'clicks', 45907);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6261, '2018-01-23', 'CY', 'views', 26605);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6262, '2018-01-23', 'CY', 'plays', 42409);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6263, '2018-01-23', 'CY', 'clicks', 50416);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6273, '2018-01-23', 'DE', 'views', 23071);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6274, '2018-01-23', 'DE', 'plays', 36625);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6275, '2018-01-23', 'DE', 'clicks', 64866);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6264, '2018-01-23', 'EG', 'views', 73421);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6265, '2018-01-23', 'EG', 'plays', 42929);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6266, '2018-01-23', 'EG', 'clicks', 10462);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6270, '2018-01-23', 'FR', 'views', 12479);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6271, '2018-01-23', 'FR', 'plays', 70596);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6272, '2018-01-23', 'FR', 'clicks', 63315);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6258, '2018-01-23', 'HR', 'views', 70546);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6259, '2018-01-23', 'HR', 'plays', 47719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6260, '2018-01-23', 'HR', 'clicks', 60116);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6267, '2018-01-23', 'RS', 'views', 61371);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6268, '2018-01-23', 'RS', 'plays', 34593);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6269, '2018-01-23', 'RS', 'clicks', 30074);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6279, '2018-01-24', 'AT', 'views', 42975);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6280, '2018-01-24', 'AT', 'plays', 46541);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6281, '2018-01-24', 'AT', 'clicks', 53324);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6276, '2018-01-24', 'AU', 'views', 28816);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6277, '2018-01-24', 'AU', 'plays', 66473);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6278, '2018-01-24', 'AU', 'clicks', 63324);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6288, '2018-01-24', 'BA', 'views', 23240);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6289, '2018-01-24', 'BA', 'plays', 14854);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6290, '2018-01-24', 'BA', 'clicks', 53905);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6285, '2018-01-24', 'BE', 'views', 48641);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6286, '2018-01-24', 'BE', 'plays', 71721);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6287, '2018-01-24', 'BE', 'clicks', 72036);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6291, '2018-01-24', 'BR', 'views', 29153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6292, '2018-01-24', 'BR', 'plays', 44176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6293, '2018-01-24', 'BR', 'clicks', 78669);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6282, '2018-01-24', 'CA', 'views', 80451);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6283, '2018-01-24', 'CA', 'plays', 55126);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6284, '2018-01-24', 'CA', 'clicks', 51168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6297, '2018-01-24', 'CY', 'views', 24222);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6298, '2018-01-24', 'CY', 'plays', 27888);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6299, '2018-01-24', 'CY', 'clicks', 60670);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6309, '2018-01-24', 'DE', 'views', 64931);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6310, '2018-01-24', 'DE', 'plays', 96611);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6311, '2018-01-24', 'DE', 'clicks', 69879);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6300, '2018-01-24', 'EG', 'views', 22075);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6301, '2018-01-24', 'EG', 'plays', 73830);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6302, '2018-01-24', 'EG', 'clicks', 59284);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6306, '2018-01-24', 'FR', 'views', 55948);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6307, '2018-01-24', 'FR', 'plays', 45854);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6308, '2018-01-24', 'FR', 'clicks', 8738);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6294, '2018-01-24', 'HR', 'views', 46519);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6295, '2018-01-24', 'HR', 'plays', 39665);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6296, '2018-01-24', 'HR', 'clicks', 86573);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6303, '2018-01-24', 'RS', 'views', 81236);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6304, '2018-01-24', 'RS', 'plays', 67336);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6305, '2018-01-24', 'RS', 'clicks', 69782);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6315, '2018-01-25', 'AT', 'views', 65791);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6316, '2018-01-25', 'AT', 'plays', 28225);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6317, '2018-01-25', 'AT', 'clicks', 79401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6312, '2018-01-25', 'AU', 'views', 26263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6313, '2018-01-25', 'AU', 'plays', 84840);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6314, '2018-01-25', 'AU', 'clicks', 57422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6324, '2018-01-25', 'BA', 'views', 45203);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6325, '2018-01-25', 'BA', 'plays', 56087);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6326, '2018-01-25', 'BA', 'clicks', 78317);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6321, '2018-01-25', 'BE', 'views', 48988);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6322, '2018-01-25', 'BE', 'plays', 60198);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6323, '2018-01-25', 'BE', 'clicks', 46878);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6327, '2018-01-25', 'BR', 'views', 82929);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6328, '2018-01-25', 'BR', 'plays', 64431);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6329, '2018-01-25', 'BR', 'clicks', 30520);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6318, '2018-01-25', 'CA', 'views', 16391);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6319, '2018-01-25', 'CA', 'plays', 33281);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6320, '2018-01-25', 'CA', 'clicks', 51226);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6333, '2018-01-25', 'CY', 'views', 70133);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6334, '2018-01-25', 'CY', 'plays', 51254);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6335, '2018-01-25', 'CY', 'clicks', 40647);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6345, '2018-01-25', 'DE', 'views', 54733);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6346, '2018-01-25', 'DE', 'plays', 17304);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6347, '2018-01-25', 'DE', 'clicks', 52721);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6336, '2018-01-25', 'EG', 'views', 20683);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6337, '2018-01-25', 'EG', 'plays', 14705);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6338, '2018-01-25', 'EG', 'clicks', 23314);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6342, '2018-01-25', 'FR', 'views', 92405);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6343, '2018-01-25', 'FR', 'plays', 27175);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6344, '2018-01-25', 'FR', 'clicks', 67079);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6330, '2018-01-25', 'HR', 'views', 76133);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6331, '2018-01-25', 'HR', 'plays', 41061);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6332, '2018-01-25', 'HR', 'clicks', 41745);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6339, '2018-01-25', 'RS', 'views', 61000);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6340, '2018-01-25', 'RS', 'plays', 33087);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6341, '2018-01-25', 'RS', 'clicks', 48055);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6351, '2018-01-26', 'AT', 'views', 62489);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6352, '2018-01-26', 'AT', 'plays', 40045);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6353, '2018-01-26', 'AT', 'clicks', 88828);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6348, '2018-01-26', 'AU', 'views', 33839);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6349, '2018-01-26', 'AU', 'plays', 43446);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6350, '2018-01-26', 'AU', 'clicks', 72326);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6360, '2018-01-26', 'BA', 'views', 64530);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6361, '2018-01-26', 'BA', 'plays', 81154);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6362, '2018-01-26', 'BA', 'clicks', 59701);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6357, '2018-01-26', 'BE', 'views', 71629);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6358, '2018-01-26', 'BE', 'plays', 44851);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6359, '2018-01-26', 'BE', 'clicks', 61626);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6363, '2018-01-26', 'BR', 'views', 88394);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6364, '2018-01-26', 'BR', 'plays', 75002);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6365, '2018-01-26', 'BR', 'clicks', 51156);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6354, '2018-01-26', 'CA', 'views', 57324);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6355, '2018-01-26', 'CA', 'plays', 52979);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6356, '2018-01-26', 'CA', 'clicks', 18653);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6369, '2018-01-26', 'CY', 'views', 79934);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6370, '2018-01-26', 'CY', 'plays', 47194);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6371, '2018-01-26', 'CY', 'clicks', 35706);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6381, '2018-01-26', 'DE', 'views', 78189);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6382, '2018-01-26', 'DE', 'plays', 58705);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6383, '2018-01-26', 'DE', 'clicks', 77561);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6372, '2018-01-26', 'EG', 'views', 63505);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6373, '2018-01-26', 'EG', 'plays', 83067);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6374, '2018-01-26', 'EG', 'clicks', 49170);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6378, '2018-01-26', 'FR', 'views', 92363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6379, '2018-01-26', 'FR', 'plays', 86598);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6380, '2018-01-26', 'FR', 'clicks', 54140);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6366, '2018-01-26', 'HR', 'views', 72026);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6367, '2018-01-26', 'HR', 'plays', 63220);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6368, '2018-01-26', 'HR', 'clicks', 64332);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6375, '2018-01-26', 'RS', 'views', 91514);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6376, '2018-01-26', 'RS', 'plays', 40985);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6377, '2018-01-26', 'RS', 'clicks', 58235);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6387, '2018-01-27', 'AT', 'views', 62213);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6388, '2018-01-27', 'AT', 'plays', 37598);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6389, '2018-01-27', 'AT', 'clicks', 11453);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6384, '2018-01-27', 'AU', 'views', 55070);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6385, '2018-01-27', 'AU', 'plays', 38813);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6386, '2018-01-27', 'AU', 'clicks', 79084);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6396, '2018-01-27', 'BA', 'views', 25798);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6397, '2018-01-27', 'BA', 'plays', 50879);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6398, '2018-01-27', 'BA', 'clicks', 62365);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6393, '2018-01-27', 'BE', 'views', 31153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6394, '2018-01-27', 'BE', 'plays', 53916);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6395, '2018-01-27', 'BE', 'clicks', 73017);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6399, '2018-01-27', 'BR', 'views', 49955);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6400, '2018-01-27', 'BR', 'plays', 45222);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6401, '2018-01-27', 'BR', 'clicks', 34512);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6390, '2018-01-27', 'CA', 'views', 75192);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6391, '2018-01-27', 'CA', 'plays', 61507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6392, '2018-01-27', 'CA', 'clicks', 52162);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6405, '2018-01-27', 'CY', 'views', 59708);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6406, '2018-01-27', 'CY', 'plays', 61205);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6407, '2018-01-27', 'CY', 'clicks', 64057);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6417, '2018-01-27', 'DE', 'views', 57081);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6418, '2018-01-27', 'DE', 'plays', 9536);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6419, '2018-01-27', 'DE', 'clicks', 12398);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6408, '2018-01-27', 'EG', 'views', 52756);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6409, '2018-01-27', 'EG', 'plays', 41757);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6410, '2018-01-27', 'EG', 'clicks', 25195);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6414, '2018-01-27', 'FR', 'views', 54850);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6415, '2018-01-27', 'FR', 'plays', 41903);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6416, '2018-01-27', 'FR', 'clicks', 76539);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6402, '2018-01-27', 'HR', 'views', 63089);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6403, '2018-01-27', 'HR', 'plays', 64747);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6404, '2018-01-27', 'HR', 'clicks', 57722);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6411, '2018-01-27', 'RS', 'views', 82094);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6412, '2018-01-27', 'RS', 'plays', 65588);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6413, '2018-01-27', 'RS', 'clicks', 55606);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6423, '2018-01-28', 'AT', 'views', 88898);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6424, '2018-01-28', 'AT', 'plays', 69288);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6425, '2018-01-28', 'AT', 'clicks', 37708);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6420, '2018-01-28', 'AU', 'views', 74101);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6421, '2018-01-28', 'AU', 'plays', 54134);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6422, '2018-01-28', 'AU', 'clicks', 18474);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6432, '2018-01-28', 'BA', 'views', 71561);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6433, '2018-01-28', 'BA', 'plays', 19756);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6434, '2018-01-28', 'BA', 'clicks', 58161);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6429, '2018-01-28', 'BE', 'views', 2677);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6430, '2018-01-28', 'BE', 'plays', 54160);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6431, '2018-01-28', 'BE', 'clicks', 20572);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6435, '2018-01-28', 'BR', 'views', 73240);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6436, '2018-01-28', 'BR', 'plays', 37651);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6437, '2018-01-28', 'BR', 'clicks', 37030);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6426, '2018-01-28', 'CA', 'views', 18626);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6427, '2018-01-28', 'CA', 'plays', 33756);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6428, '2018-01-28', 'CA', 'clicks', 21716);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6441, '2018-01-28', 'CY', 'views', 49353);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6442, '2018-01-28', 'CY', 'plays', 59932);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6443, '2018-01-28', 'CY', 'clicks', 56956);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6453, '2018-01-28', 'DE', 'views', 40310);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6454, '2018-01-28', 'DE', 'plays', 67849);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6455, '2018-01-28', 'DE', 'clicks', 25777);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6444, '2018-01-28', 'EG', 'views', 59447);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6445, '2018-01-28', 'EG', 'plays', 35149);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6446, '2018-01-28', 'EG', 'clicks', 60824);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6450, '2018-01-28', 'FR', 'views', 13290);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6451, '2018-01-28', 'FR', 'plays', 71943);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6452, '2018-01-28', 'FR', 'clicks', 6767);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6438, '2018-01-28', 'HR', 'views', 71005);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6439, '2018-01-28', 'HR', 'plays', 73278);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6440, '2018-01-28', 'HR', 'clicks', 26920);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6447, '2018-01-28', 'RS', 'views', 64509);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6448, '2018-01-28', 'RS', 'plays', 46531);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6449, '2018-01-28', 'RS', 'clicks', 28654);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6459, '2018-01-29', 'AT', 'views', 58534);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6460, '2018-01-29', 'AT', 'plays', 51790);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6461, '2018-01-29', 'AT', 'clicks', 41246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6456, '2018-01-29', 'AU', 'views', 25058);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6457, '2018-01-29', 'AU', 'plays', 26469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6458, '2018-01-29', 'AU', 'clicks', 48830);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6468, '2018-01-29', 'BA', 'views', 45134);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6469, '2018-01-29', 'BA', 'plays', 64492);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6470, '2018-01-29', 'BA', 'clicks', 60625);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6465, '2018-01-29', 'BE', 'views', 21105);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6466, '2018-01-29', 'BE', 'plays', 40879);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6467, '2018-01-29', 'BE', 'clicks', 79735);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6471, '2018-01-29', 'BR', 'views', 17066);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6472, '2018-01-29', 'BR', 'plays', 67433);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6473, '2018-01-29', 'BR', 'clicks', 44198);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6462, '2018-01-29', 'CA', 'views', 44345);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6463, '2018-01-29', 'CA', 'plays', 68703);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6464, '2018-01-29', 'CA', 'clicks', 57817);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6477, '2018-01-29', 'CY', 'views', 53119);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6478, '2018-01-29', 'CY', 'plays', 24033);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6479, '2018-01-29', 'CY', 'clicks', 38053);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6489, '2018-01-29', 'DE', 'views', 46754);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6490, '2018-01-29', 'DE', 'plays', 63816);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6491, '2018-01-29', 'DE', 'clicks', 21578);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6480, '2018-01-29', 'EG', 'views', 66575);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6481, '2018-01-29', 'EG', 'plays', 58113);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6482, '2018-01-29', 'EG', 'clicks', 54972);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6486, '2018-01-29', 'FR', 'views', 25947);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6487, '2018-01-29', 'FR', 'plays', 78208);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6488, '2018-01-29', 'FR', 'clicks', 87593);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6474, '2018-01-29', 'HR', 'views', 10158);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6475, '2018-01-29', 'HR', 'plays', 47320);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6476, '2018-01-29', 'HR', 'clicks', 72704);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6483, '2018-01-29', 'RS', 'views', 70417);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6484, '2018-01-29', 'RS', 'plays', 52671);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6485, '2018-01-29', 'RS', 'clicks', 28143);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6495, '2018-01-30', 'AT', 'views', 53197);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6496, '2018-01-30', 'AT', 'plays', 56546);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6497, '2018-01-30', 'AT', 'clicks', 49070);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6492, '2018-01-30', 'AU', 'views', 25474);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6493, '2018-01-30', 'AU', 'plays', 58290);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6494, '2018-01-30', 'AU', 'clicks', 55082);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6504, '2018-01-30', 'BA', 'views', 77004);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6505, '2018-01-30', 'BA', 'plays', 15925);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6506, '2018-01-30', 'BA', 'clicks', 55093);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6501, '2018-01-30', 'BE', 'views', 37000);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6502, '2018-01-30', 'BE', 'plays', 72060);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6503, '2018-01-30', 'BE', 'clicks', 76499);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6507, '2018-01-30', 'BR', 'views', 70069);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6508, '2018-01-30', 'BR', 'plays', 45875);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6509, '2018-01-30', 'BR', 'clicks', 80456);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6498, '2018-01-30', 'CA', 'views', 29224);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6499, '2018-01-30', 'CA', 'plays', 82706);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6500, '2018-01-30', 'CA', 'clicks', 59420);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6513, '2018-01-30', 'CY', 'views', 53611);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6514, '2018-01-30', 'CY', 'plays', 42887);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6515, '2018-01-30', 'CY', 'clicks', 38040);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6525, '2018-01-30', 'DE', 'views', 50102);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6526, '2018-01-30', 'DE', 'plays', 71170);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6527, '2018-01-30', 'DE', 'clicks', 76358);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6516, '2018-01-30', 'EG', 'views', 49883);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6517, '2018-01-30', 'EG', 'plays', 34891);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6518, '2018-01-30', 'EG', 'clicks', 20093);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6522, '2018-01-30', 'FR', 'views', 46334);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6523, '2018-01-30', 'FR', 'plays', 41069);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6524, '2018-01-30', 'FR', 'clicks', 30768);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6510, '2018-01-30', 'HR', 'views', 53641);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6511, '2018-01-30', 'HR', 'plays', 64107);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6512, '2018-01-30', 'HR', 'clicks', 30363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6519, '2018-01-30', 'RS', 'views', 20935);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6520, '2018-01-30', 'RS', 'plays', 19401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6521, '2018-01-30', 'RS', 'clicks', 61298);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6531, '2018-01-31', 'AT', 'views', 32914);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6532, '2018-01-31', 'AT', 'plays', 41600);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6533, '2018-01-31', 'AT', 'clicks', 61014);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6528, '2018-01-31', 'AU', 'views', 33501);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6529, '2018-01-31', 'AU', 'plays', 83418);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6530, '2018-01-31', 'AU', 'clicks', 57595);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6540, '2018-01-31', 'BA', 'views', 36138);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6541, '2018-01-31', 'BA', 'plays', 53271);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6542, '2018-01-31', 'BA', 'clicks', 45929);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6537, '2018-01-31', 'BE', 'views', 59901);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6538, '2018-01-31', 'BE', 'plays', 57590);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6539, '2018-01-31', 'BE', 'clicks', 71419);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6543, '2018-01-31', 'BR', 'views', 80169);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6544, '2018-01-31', 'BR', 'plays', 31938);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6545, '2018-01-31', 'BR', 'clicks', 75647);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6534, '2018-01-31', 'CA', 'views', 39252);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6535, '2018-01-31', 'CA', 'plays', 16892);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6536, '2018-01-31', 'CA', 'clicks', 69325);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6549, '2018-01-31', 'CY', 'views', 55710);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6550, '2018-01-31', 'CY', 'plays', 73784);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6551, '2018-01-31', 'CY', 'clicks', 61692);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6561, '2018-01-31', 'DE', 'views', 28132);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6562, '2018-01-31', 'DE', 'plays', 40486);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6563, '2018-01-31', 'DE', 'clicks', 18176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6552, '2018-01-31', 'EG', 'views', 50257);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6553, '2018-01-31', 'EG', 'plays', 53623);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6554, '2018-01-31', 'EG', 'clicks', 74627);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6558, '2018-01-31', 'FR', 'views', 19716);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6559, '2018-01-31', 'FR', 'plays', 46439);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6560, '2018-01-31', 'FR', 'clicks', 83232);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6546, '2018-01-31', 'HR', 'views', 79269);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6547, '2018-01-31', 'HR', 'plays', 80110);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6548, '2018-01-31', 'HR', 'clicks', 57043);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6555, '2018-01-31', 'RS', 'views', 50072);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6556, '2018-01-31', 'RS', 'plays', 37590);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6557, '2018-01-31', 'RS', 'clicks', 18425);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6567, '2018-02-01', 'AT', 'views', 73291);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6568, '2018-02-01', 'AT', 'plays', 85320);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6569, '2018-02-01', 'AT', 'clicks', 33685);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6564, '2018-02-01', 'AU', 'views', 55176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6565, '2018-02-01', 'AU', 'plays', 46103);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6566, '2018-02-01', 'AU', 'clicks', 44317);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6576, '2018-02-01', 'BA', 'views', 57361);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6577, '2018-02-01', 'BA', 'plays', 42821);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6578, '2018-02-01', 'BA', 'clicks', 60784);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6573, '2018-02-01', 'BE', 'views', 84050);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6574, '2018-02-01', 'BE', 'plays', 56181);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6575, '2018-02-01', 'BE', 'clicks', 71444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6579, '2018-02-01', 'BR', 'views', 42315);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6580, '2018-02-01', 'BR', 'plays', 72424);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6581, '2018-02-01', 'BR', 'clicks', 17626);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6570, '2018-02-01', 'CA', 'views', 75683);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6571, '2018-02-01', 'CA', 'plays', 55290);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6572, '2018-02-01', 'CA', 'clicks', 26341);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6585, '2018-02-01', 'CY', 'views', 64729);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6586, '2018-02-01', 'CY', 'plays', 63386);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6587, '2018-02-01', 'CY', 'clicks', 73001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6597, '2018-02-01', 'DE', 'views', 17354);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6598, '2018-02-01', 'DE', 'plays', 28888);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6599, '2018-02-01', 'DE', 'clicks', 46491);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6588, '2018-02-01', 'EG', 'views', 36761);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6589, '2018-02-01', 'EG', 'plays', 31245);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6590, '2018-02-01', 'EG', 'clicks', 61571);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6594, '2018-02-01', 'FR', 'views', 59639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6595, '2018-02-01', 'FR', 'plays', 47764);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6596, '2018-02-01', 'FR', 'clicks', 61195);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6582, '2018-02-01', 'HR', 'views', 54894);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6583, '2018-02-01', 'HR', 'plays', 52790);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6584, '2018-02-01', 'HR', 'clicks', 78942);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6591, '2018-02-01', 'RS', 'views', 24469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6592, '2018-02-01', 'RS', 'plays', 42272);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6593, '2018-02-01', 'RS', 'clicks', 73036);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6603, '2018-02-02', 'AT', 'views', 50307);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6604, '2018-02-02', 'AT', 'plays', 75087);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6605, '2018-02-02', 'AT', 'clicks', 35746);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6600, '2018-02-02', 'AU', 'views', 12145);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6601, '2018-02-02', 'AU', 'plays', 21638);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6602, '2018-02-02', 'AU', 'clicks', 55598);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6612, '2018-02-02', 'BA', 'views', 45107);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6613, '2018-02-02', 'BA', 'plays', 63594);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6614, '2018-02-02', 'BA', 'clicks', 76946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6609, '2018-02-02', 'BE', 'views', 73745);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6610, '2018-02-02', 'BE', 'plays', 35459);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6611, '2018-02-02', 'BE', 'clicks', 67362);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6615, '2018-02-02', 'BR', 'views', 47846);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6616, '2018-02-02', 'BR', 'plays', 50196);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6617, '2018-02-02', 'BR', 'clicks', 40971);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6606, '2018-02-02', 'CA', 'views', 96889);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6607, '2018-02-02', 'CA', 'plays', 25517);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6608, '2018-02-02', 'CA', 'clicks', 7025);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6621, '2018-02-02', 'CY', 'views', 51471);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6622, '2018-02-02', 'CY', 'plays', 60808);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6623, '2018-02-02', 'CY', 'clicks', 74504);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6633, '2018-02-02', 'DE', 'views', 7630);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6634, '2018-02-02', 'DE', 'plays', 59314);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6635, '2018-02-02', 'DE', 'clicks', 55803);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6624, '2018-02-02', 'EG', 'views', 78760);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6625, '2018-02-02', 'EG', 'plays', 28777);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6626, '2018-02-02', 'EG', 'clicks', 48975);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6630, '2018-02-02', 'FR', 'views', 46447);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6631, '2018-02-02', 'FR', 'plays', 64742);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6632, '2018-02-02', 'FR', 'clicks', 24292);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6618, '2018-02-02', 'HR', 'views', 51585);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6619, '2018-02-02', 'HR', 'plays', 66088);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6620, '2018-02-02', 'HR', 'clicks', 88956);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6627, '2018-02-02', 'RS', 'views', 40069);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6628, '2018-02-02', 'RS', 'plays', 48226);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6629, '2018-02-02', 'RS', 'clicks', 27974);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6639, '2018-02-03', 'AT', 'views', 7779);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6640, '2018-02-03', 'AT', 'plays', 10888);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6641, '2018-02-03', 'AT', 'clicks', 38248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6636, '2018-02-03', 'AU', 'views', 79194);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6637, '2018-02-03', 'AU', 'plays', 75158);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6638, '2018-02-03', 'AU', 'clicks', 11617);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6648, '2018-02-03', 'BA', 'views', 47497);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6649, '2018-02-03', 'BA', 'plays', 63615);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6650, '2018-02-03', 'BA', 'clicks', 29678);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6645, '2018-02-03', 'BE', 'views', 47643);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6646, '2018-02-03', 'BE', 'plays', 40331);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6647, '2018-02-03', 'BE', 'clicks', 53012);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6651, '2018-02-03', 'BR', 'views', 45899);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6652, '2018-02-03', 'BR', 'plays', 55931);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6653, '2018-02-03', 'BR', 'clicks', 44922);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6642, '2018-02-03', 'CA', 'views', 68303);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6643, '2018-02-03', 'CA', 'plays', 47672);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6644, '2018-02-03', 'CA', 'clicks', 77629);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6657, '2018-02-03', 'CY', 'views', 46067);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6658, '2018-02-03', 'CY', 'plays', 66840);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6659, '2018-02-03', 'CY', 'clicks', 38679);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6669, '2018-02-03', 'DE', 'views', 67033);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6670, '2018-02-03', 'DE', 'plays', 29369);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6671, '2018-02-03', 'DE', 'clicks', 72804);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6660, '2018-02-03', 'EG', 'views', 38839);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6661, '2018-02-03', 'EG', 'plays', 61373);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6662, '2018-02-03', 'EG', 'clicks', 58660);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6666, '2018-02-03', 'FR', 'views', 44627);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6667, '2018-02-03', 'FR', 'plays', 59294);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6668, '2018-02-03', 'FR', 'clicks', 48665);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6654, '2018-02-03', 'HR', 'views', 28863);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6655, '2018-02-03', 'HR', 'plays', 59134);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6656, '2018-02-03', 'HR', 'clicks', 39318);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6663, '2018-02-03', 'RS', 'views', 48138);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6664, '2018-02-03', 'RS', 'plays', 70754);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6665, '2018-02-03', 'RS', 'clicks', 62300);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6675, '2018-02-04', 'AT', 'views', 31601);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6676, '2018-02-04', 'AT', 'plays', 56299);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6677, '2018-02-04', 'AT', 'clicks', 47976);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6672, '2018-02-04', 'AU', 'views', 83294);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6673, '2018-02-04', 'AU', 'plays', 63973);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6674, '2018-02-04', 'AU', 'clicks', 55192);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6684, '2018-02-04', 'BA', 'views', 32250);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6685, '2018-02-04', 'BA', 'plays', 68928);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6686, '2018-02-04', 'BA', 'clicks', 72356);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6681, '2018-02-04', 'BE', 'views', 59752);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6682, '2018-02-04', 'BE', 'plays', 47906);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6683, '2018-02-04', 'BE', 'clicks', 66701);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6687, '2018-02-04', 'BR', 'views', 64583);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6688, '2018-02-04', 'BR', 'plays', 52729);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6689, '2018-02-04', 'BR', 'clicks', 62667);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6678, '2018-02-04', 'CA', 'views', 91255);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6679, '2018-02-04', 'CA', 'plays', 54793);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6680, '2018-02-04', 'CA', 'clicks', 23542);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6693, '2018-02-04', 'CY', 'views', 39509);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6694, '2018-02-04', 'CY', 'plays', 42906);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6695, '2018-02-04', 'CY', 'clicks', 65163);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6705, '2018-02-04', 'DE', 'views', 59141);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6706, '2018-02-04', 'DE', 'plays', 39128);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6707, '2018-02-04', 'DE', 'clicks', 36647);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6696, '2018-02-04', 'EG', 'views', 47735);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6697, '2018-02-04', 'EG', 'plays', 81513);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6698, '2018-02-04', 'EG', 'clicks', 43176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6702, '2018-02-04', 'FR', 'views', 63450);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6703, '2018-02-04', 'FR', 'plays', 56567);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6704, '2018-02-04', 'FR', 'clicks', 9500);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6690, '2018-02-04', 'HR', 'views', 52279);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6691, '2018-02-04', 'HR', 'plays', 40169);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6692, '2018-02-04', 'HR', 'clicks', 69956);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6699, '2018-02-04', 'RS', 'views', 45639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6700, '2018-02-04', 'RS', 'plays', 73579);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6701, '2018-02-04', 'RS', 'clicks', 47280);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6711, '2018-02-05', 'AT', 'views', 14394);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6712, '2018-02-05', 'AT', 'plays', 28321);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6713, '2018-02-05', 'AT', 'clicks', 14270);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6708, '2018-02-05', 'AU', 'views', 65769);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6709, '2018-02-05', 'AU', 'plays', 36616);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6710, '2018-02-05', 'AU', 'clicks', 34451);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6720, '2018-02-05', 'BA', 'views', 25199);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6721, '2018-02-05', 'BA', 'plays', 49301);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6722, '2018-02-05', 'BA', 'clicks', 20515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6717, '2018-02-05', 'BE', 'views', 55003);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6718, '2018-02-05', 'BE', 'plays', 95373);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6719, '2018-02-05', 'BE', 'clicks', 78529);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6723, '2018-02-05', 'BR', 'views', 66921);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6724, '2018-02-05', 'BR', 'plays', 22558);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6725, '2018-02-05', 'BR', 'clicks', 26621);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6714, '2018-02-05', 'CA', 'views', 42263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6715, '2018-02-05', 'CA', 'plays', 44542);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6716, '2018-02-05', 'CA', 'clicks', 31758);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6729, '2018-02-05', 'CY', 'views', 88062);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6730, '2018-02-05', 'CY', 'plays', 29627);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6731, '2018-02-05', 'CY', 'clicks', 30566);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6741, '2018-02-05', 'DE', 'views', 10081);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6742, '2018-02-05', 'DE', 'plays', 14343);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6743, '2018-02-05', 'DE', 'clicks', 33280);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6732, '2018-02-05', 'EG', 'views', 30113);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6733, '2018-02-05', 'EG', 'plays', 75452);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6734, '2018-02-05', 'EG', 'clicks', 50582);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6738, '2018-02-05', 'FR', 'views', 35596);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6739, '2018-02-05', 'FR', 'plays', 45991);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6740, '2018-02-05', 'FR', 'clicks', 55273);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6726, '2018-02-05', 'HR', 'views', 74613);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6727, '2018-02-05', 'HR', 'plays', 78521);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6728, '2018-02-05', 'HR', 'clicks', 56346);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6735, '2018-02-05', 'RS', 'views', 44495);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6736, '2018-02-05', 'RS', 'plays', 67444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6737, '2018-02-05', 'RS', 'clicks', 70728);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6747, '2018-02-06', 'AT', 'views', 22985);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6748, '2018-02-06', 'AT', 'plays', 51213);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6749, '2018-02-06', 'AT', 'clicks', 49836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6744, '2018-02-06', 'AU', 'views', 29444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6745, '2018-02-06', 'AU', 'plays', 73481);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6746, '2018-02-06', 'AU', 'clicks', 85670);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6756, '2018-02-06', 'BA', 'views', 21305);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6757, '2018-02-06', 'BA', 'plays', 81227);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6758, '2018-02-06', 'BA', 'clicks', 44570);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6753, '2018-02-06', 'BE', 'views', 53388);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6754, '2018-02-06', 'BE', 'plays', 40112);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6755, '2018-02-06', 'BE', 'clicks', 33186);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6759, '2018-02-06', 'BR', 'views', 25783);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6760, '2018-02-06', 'BR', 'plays', 39326);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6761, '2018-02-06', 'BR', 'clicks', 55099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6750, '2018-02-06', 'CA', 'views', 75493);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6751, '2018-02-06', 'CA', 'plays', 41075);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6752, '2018-02-06', 'CA', 'clicks', 61597);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6765, '2018-02-06', 'CY', 'views', 29140);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6766, '2018-02-06', 'CY', 'plays', 33307);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6767, '2018-02-06', 'CY', 'clicks', 23334);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6777, '2018-02-06', 'DE', 'views', 49524);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6778, '2018-02-06', 'DE', 'plays', 27163);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6779, '2018-02-06', 'DE', 'clicks', 12943);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6768, '2018-02-06', 'EG', 'views', 62524);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6769, '2018-02-06', 'EG', 'plays', 18674);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6770, '2018-02-06', 'EG', 'clicks', 64893);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6774, '2018-02-06', 'FR', 'views', 53636);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6775, '2018-02-06', 'FR', 'plays', 94695);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6776, '2018-02-06', 'FR', 'clicks', 54996);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6762, '2018-02-06', 'HR', 'views', 12787);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6763, '2018-02-06', 'HR', 'plays', 31206);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6764, '2018-02-06', 'HR', 'clicks', 6669);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6771, '2018-02-06', 'RS', 'views', 25253);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6772, '2018-02-06', 'RS', 'plays', 43156);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6773, '2018-02-06', 'RS', 'clicks', 70096);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6783, '2018-02-07', 'AT', 'views', 35298);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6784, '2018-02-07', 'AT', 'plays', 98865);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6785, '2018-02-07', 'AT', 'clicks', 22843);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6780, '2018-02-07', 'AU', 'views', 14075);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6781, '2018-02-07', 'AU', 'plays', 22821);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6782, '2018-02-07', 'AU', 'clicks', 69730);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6792, '2018-02-07', 'BA', 'views', 88990);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6793, '2018-02-07', 'BA', 'plays', 76657);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6794, '2018-02-07', 'BA', 'clicks', 30566);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6789, '2018-02-07', 'BE', 'views', 39336);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6790, '2018-02-07', 'BE', 'plays', 49873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6791, '2018-02-07', 'BE', 'clicks', 21361);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6795, '2018-02-07', 'BR', 'views', 36325);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6796, '2018-02-07', 'BR', 'plays', 34270);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6797, '2018-02-07', 'BR', 'clicks', 28680);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6786, '2018-02-07', 'CA', 'views', 34584);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6787, '2018-02-07', 'CA', 'plays', 43909);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6788, '2018-02-07', 'CA', 'clicks', 37099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6801, '2018-02-07', 'CY', 'views', 50129);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6802, '2018-02-07', 'CY', 'plays', 53630);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6803, '2018-02-07', 'CY', 'clicks', 60468);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6813, '2018-02-07', 'DE', 'views', 45567);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6814, '2018-02-07', 'DE', 'plays', 67829);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6815, '2018-02-07', 'DE', 'clicks', 38263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6804, '2018-02-07', 'EG', 'views', 62326);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6805, '2018-02-07', 'EG', 'plays', 39828);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6806, '2018-02-07', 'EG', 'clicks', 24303);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6810, '2018-02-07', 'FR', 'views', 48197);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6811, '2018-02-07', 'FR', 'plays', 17900);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6812, '2018-02-07', 'FR', 'clicks', 73724);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6798, '2018-02-07', 'HR', 'views', 76875);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6799, '2018-02-07', 'HR', 'plays', 57130);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6800, '2018-02-07', 'HR', 'clicks', 72034);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6807, '2018-02-07', 'RS', 'views', 56027);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6808, '2018-02-07', 'RS', 'plays', 59072);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6809, '2018-02-07', 'RS', 'clicks', 53580);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6819, '2018-02-08', 'AT', 'views', 41406);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6820, '2018-02-08', 'AT', 'plays', 55751);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6821, '2018-02-08', 'AT', 'clicks', 30305);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6816, '2018-02-08', 'AU', 'views', 19444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6817, '2018-02-08', 'AU', 'plays', 66458);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6818, '2018-02-08', 'AU', 'clicks', 51744);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6828, '2018-02-08', 'BA', 'views', 20545);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6829, '2018-02-08', 'BA', 'plays', 56785);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6830, '2018-02-08', 'BA', 'clicks', 91373);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6825, '2018-02-08', 'BE', 'views', 39664);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6826, '2018-02-08', 'BE', 'plays', 40812);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6827, '2018-02-08', 'BE', 'clicks', 45671);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6831, '2018-02-08', 'BR', 'views', 6277);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6832, '2018-02-08', 'BR', 'plays', 47062);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6833, '2018-02-08', 'BR', 'clicks', 43168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6822, '2018-02-08', 'CA', 'views', 77308);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6823, '2018-02-08', 'CA', 'plays', 75265);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6824, '2018-02-08', 'CA', 'clicks', 36262);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6837, '2018-02-08', 'CY', 'views', 59440);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6838, '2018-02-08', 'CY', 'plays', 31996);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6839, '2018-02-08', 'CY', 'clicks', 59621);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6849, '2018-02-08', 'DE', 'views', 34369);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6850, '2018-02-08', 'DE', 'plays', 84070);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6851, '2018-02-08', 'DE', 'clicks', 52079);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6840, '2018-02-08', 'EG', 'views', 64338);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6841, '2018-02-08', 'EG', 'plays', 39383);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6842, '2018-02-08', 'EG', 'clicks', 36303);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6846, '2018-02-08', 'FR', 'views', 15868);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6847, '2018-02-08', 'FR', 'plays', 59070);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6848, '2018-02-08', 'FR', 'clicks', 47003);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6834, '2018-02-08', 'HR', 'views', 36761);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6835, '2018-02-08', 'HR', 'plays', 74753);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6836, '2018-02-08', 'HR', 'clicks', 33646);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6843, '2018-02-08', 'RS', 'views', 36047);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6844, '2018-02-08', 'RS', 'plays', 47602);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6845, '2018-02-08', 'RS', 'clicks', 38930);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6855, '2018-02-09', 'AT', 'views', 52693);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6856, '2018-02-09', 'AT', 'plays', 29986);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6857, '2018-02-09', 'AT', 'clicks', 83093);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6852, '2018-02-09', 'AU', 'views', 35391);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6853, '2018-02-09', 'AU', 'plays', 48071);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6854, '2018-02-09', 'AU', 'clicks', 43214);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6864, '2018-02-09', 'BA', 'views', 10436);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6865, '2018-02-09', 'BA', 'plays', 22154);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6866, '2018-02-09', 'BA', 'clicks', 68306);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6861, '2018-02-09', 'BE', 'views', 54995);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6862, '2018-02-09', 'BE', 'plays', 55671);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6863, '2018-02-09', 'BE', 'clicks', 10380);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6867, '2018-02-09', 'BR', 'views', 27411);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6868, '2018-02-09', 'BR', 'plays', 59054);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6869, '2018-02-09', 'BR', 'clicks', 66647);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6858, '2018-02-09', 'CA', 'views', 91124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6859, '2018-02-09', 'CA', 'plays', 1617);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6860, '2018-02-09', 'CA', 'clicks', 39794);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6873, '2018-02-09', 'CY', 'views', 64348);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6874, '2018-02-09', 'CY', 'plays', 83681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6875, '2018-02-09', 'CY', 'clicks', 17464);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6885, '2018-02-09', 'DE', 'views', 60897);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6886, '2018-02-09', 'DE', 'plays', 45332);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6887, '2018-02-09', 'DE', 'clicks', 40830);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6876, '2018-02-09', 'EG', 'views', 23727);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6877, '2018-02-09', 'EG', 'plays', 70900);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6878, '2018-02-09', 'EG', 'clicks', 70982);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6882, '2018-02-09', 'FR', 'views', 67566);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6883, '2018-02-09', 'FR', 'plays', 49454);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6884, '2018-02-09', 'FR', 'clicks', 52433);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6870, '2018-02-09', 'HR', 'views', 54554);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6871, '2018-02-09', 'HR', 'plays', 25850);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6872, '2018-02-09', 'HR', 'clicks', 9392);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6879, '2018-02-09', 'RS', 'views', 43028);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6880, '2018-02-09', 'RS', 'plays', 55455);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6881, '2018-02-09', 'RS', 'clicks', 59625);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6891, '2018-02-10', 'AT', 'views', 42469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6892, '2018-02-10', 'AT', 'plays', 46059);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6893, '2018-02-10', 'AT', 'clicks', 50388);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6888, '2018-02-10', 'AU', 'views', 46556);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6889, '2018-02-10', 'AU', 'plays', 43501);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6890, '2018-02-10', 'AU', 'clicks', 49997);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6900, '2018-02-10', 'BA', 'views', 45633);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6901, '2018-02-10', 'BA', 'plays', 65663);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6902, '2018-02-10', 'BA', 'clicks', 34128);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6897, '2018-02-10', 'BE', 'views', 30237);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6898, '2018-02-10', 'BE', 'plays', 23321);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6899, '2018-02-10', 'BE', 'clicks', 44998);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6903, '2018-02-10', 'BR', 'views', 37344);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6904, '2018-02-10', 'BR', 'plays', 13580);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6905, '2018-02-10', 'BR', 'clicks', 55264);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6894, '2018-02-10', 'CA', 'views', 51471);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6895, '2018-02-10', 'CA', 'plays', 64425);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6896, '2018-02-10', 'CA', 'clicks', 50200);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6909, '2018-02-10', 'CY', 'views', 38254);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6910, '2018-02-10', 'CY', 'plays', 78181);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6911, '2018-02-10', 'CY', 'clicks', 16733);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6921, '2018-02-10', 'DE', 'views', 4920);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6922, '2018-02-10', 'DE', 'plays', 25266);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6923, '2018-02-10', 'DE', 'clicks', 50694);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6912, '2018-02-10', 'EG', 'views', 70926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6913, '2018-02-10', 'EG', 'plays', 65825);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6914, '2018-02-10', 'EG', 'clicks', 73436);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6918, '2018-02-10', 'FR', 'views', 45695);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6919, '2018-02-10', 'FR', 'plays', 15530);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6920, '2018-02-10', 'FR', 'clicks', 7869);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6906, '2018-02-10', 'HR', 'views', 40770);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6907, '2018-02-10', 'HR', 'plays', 66084);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6908, '2018-02-10', 'HR', 'clicks', 49416);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6915, '2018-02-10', 'RS', 'views', 57780);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6916, '2018-02-10', 'RS', 'plays', 62942);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6917, '2018-02-10', 'RS', 'clicks', 44798);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6927, '2018-02-11', 'AT', 'views', 46343);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6928, '2018-02-11', 'AT', 'plays', 37445);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6929, '2018-02-11', 'AT', 'clicks', 72769);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6924, '2018-02-11', 'AU', 'views', 52524);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6925, '2018-02-11', 'AU', 'plays', 47523);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6926, '2018-02-11', 'AU', 'clicks', 82472);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6936, '2018-02-11', 'BA', 'views', 41590);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6937, '2018-02-11', 'BA', 'plays', 21230);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6938, '2018-02-11', 'BA', 'clicks', 37197);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6933, '2018-02-11', 'BE', 'views', 31653);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6934, '2018-02-11', 'BE', 'plays', 47738);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6935, '2018-02-11', 'BE', 'clicks', 47136);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6939, '2018-02-11', 'BR', 'views', 54857);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6940, '2018-02-11', 'BR', 'plays', 77631);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6941, '2018-02-11', 'BR', 'clicks', 42661);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6930, '2018-02-11', 'CA', 'views', 52002);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6931, '2018-02-11', 'CA', 'plays', 53931);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6932, '2018-02-11', 'CA', 'clicks', 61350);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6945, '2018-02-11', 'CY', 'views', 45298);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6946, '2018-02-11', 'CY', 'plays', 85868);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6947, '2018-02-11', 'CY', 'clicks', 10447);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6957, '2018-02-11', 'DE', 'views', 57137);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6958, '2018-02-11', 'DE', 'plays', 52338);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6959, '2018-02-11', 'DE', 'clicks', 95260);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6948, '2018-02-11', 'EG', 'views', 44923);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6949, '2018-02-11', 'EG', 'plays', 71448);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6950, '2018-02-11', 'EG', 'clicks', 61578);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6954, '2018-02-11', 'FR', 'views', 21738);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6955, '2018-02-11', 'FR', 'plays', 46664);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6956, '2018-02-11', 'FR', 'clicks', 33637);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6942, '2018-02-11', 'HR', 'views', 70737);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6943, '2018-02-11', 'HR', 'plays', 55666);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6944, '2018-02-11', 'HR', 'clicks', 93537);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6951, '2018-02-11', 'RS', 'views', 29932);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6952, '2018-02-11', 'RS', 'plays', 25677);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6953, '2018-02-11', 'RS', 'clicks', 52761);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6963, '2018-02-12', 'AT', 'views', 59176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6964, '2018-02-12', 'AT', 'plays', 54280);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6965, '2018-02-12', 'AT', 'clicks', 27079);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6960, '2018-02-12', 'AU', 'views', 61608);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6961, '2018-02-12', 'AU', 'plays', 62247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6962, '2018-02-12', 'AU', 'clicks', 85828);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6972, '2018-02-12', 'BA', 'views', 74644);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6973, '2018-02-12', 'BA', 'plays', 10355);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6974, '2018-02-12', 'BA', 'clicks', 27405);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6969, '2018-02-12', 'BE', 'views', 36352);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6970, '2018-02-12', 'BE', 'plays', 38398);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6971, '2018-02-12', 'BE', 'clicks', 51873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6975, '2018-02-12', 'BR', 'views', 67333);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6976, '2018-02-12', 'BR', 'plays', 37390);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6977, '2018-02-12', 'BR', 'clicks', 44208);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6966, '2018-02-12', 'CA', 'views', 46129);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6967, '2018-02-12', 'CA', 'plays', 59995);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6968, '2018-02-12', 'CA', 'clicks', 83289);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6981, '2018-02-12', 'CY', 'views', 51903);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6982, '2018-02-12', 'CY', 'plays', 28570);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6983, '2018-02-12', 'CY', 'clicks', 85745);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6993, '2018-02-12', 'DE', 'views', 58813);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6994, '2018-02-12', 'DE', 'plays', 52154);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6995, '2018-02-12', 'DE', 'clicks', 85011);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6984, '2018-02-12', 'EG', 'views', 23077);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6985, '2018-02-12', 'EG', 'plays', 36992);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6986, '2018-02-12', 'EG', 'clicks', 72315);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6990, '2018-02-12', 'FR', 'views', 68606);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6991, '2018-02-12', 'FR', 'plays', 31686);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6992, '2018-02-12', 'FR', 'clicks', 37024);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6978, '2018-02-12', 'HR', 'views', 37673);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6979, '2018-02-12', 'HR', 'plays', 53635);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6980, '2018-02-12', 'HR', 'clicks', 81026);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6987, '2018-02-12', 'RS', 'views', 56152);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6988, '2018-02-12', 'RS', 'plays', 56369);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6989, '2018-02-12', 'RS', 'clicks', 33172);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6999, '2018-02-13', 'AT', 'views', 15887);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7000, '2018-02-13', 'AT', 'plays', 71223);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7001, '2018-02-13', 'AT', 'clicks', 26991);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6996, '2018-02-13', 'AU', 'views', 26512);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6997, '2018-02-13', 'AU', 'plays', 39583);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(6998, '2018-02-13', 'AU', 'clicks', 41394);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7008, '2018-02-13', 'BA', 'views', 8011);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7009, '2018-02-13', 'BA', 'plays', 45981);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7010, '2018-02-13', 'BA', 'clicks', 33746);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7005, '2018-02-13', 'BE', 'views', 17418);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7006, '2018-02-13', 'BE', 'plays', 21939);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7007, '2018-02-13', 'BE', 'clicks', 21755);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7011, '2018-02-13', 'BR', 'views', 31378);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7012, '2018-02-13', 'BR', 'plays', 60330);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7013, '2018-02-13', 'BR', 'clicks', 69351);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7002, '2018-02-13', 'CA', 'views', 80749);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7003, '2018-02-13', 'CA', 'plays', 30290);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7004, '2018-02-13', 'CA', 'clicks', 56568);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7017, '2018-02-13', 'CY', 'views', 59865);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7018, '2018-02-13', 'CY', 'plays', 29098);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7019, '2018-02-13', 'CY', 'clicks', 41624);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7029, '2018-02-13', 'DE', 'views', 32908);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7030, '2018-02-13', 'DE', 'plays', 21314);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7031, '2018-02-13', 'DE', 'clicks', 37227);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7020, '2018-02-13', 'EG', 'views', 60618);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7021, '2018-02-13', 'EG', 'plays', 40585);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7022, '2018-02-13', 'EG', 'clicks', 64664);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7026, '2018-02-13', 'FR', 'views', 60515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7027, '2018-02-13', 'FR', 'plays', 56864);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7028, '2018-02-13', 'FR', 'clicks', 31719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7014, '2018-02-13', 'HR', 'views', 48668);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7015, '2018-02-13', 'HR', 'plays', 46836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7016, '2018-02-13', 'HR', 'clicks', 54233);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7023, '2018-02-13', 'RS', 'views', 52015);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7024, '2018-02-13', 'RS', 'plays', 44499);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7025, '2018-02-13', 'RS', 'clicks', 58689);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7035, '2018-02-14', 'AT', 'views', 72125);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7036, '2018-02-14', 'AT', 'plays', 46304);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7037, '2018-02-14', 'AT', 'clicks', 84701);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7032, '2018-02-14', 'AU', 'views', 58146);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7033, '2018-02-14', 'AU', 'plays', 85423);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7034, '2018-02-14', 'AU', 'clicks', 53765);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7044, '2018-02-14', 'BA', 'views', 58214);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7045, '2018-02-14', 'BA', 'plays', 46586);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7046, '2018-02-14', 'BA', 'clicks', 42168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7041, '2018-02-14', 'BE', 'views', 72141);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7042, '2018-02-14', 'BE', 'plays', 74635);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7043, '2018-02-14', 'BE', 'clicks', 31754);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7047, '2018-02-14', 'BR', 'views', 82623);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7048, '2018-02-14', 'BR', 'plays', 19001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7049, '2018-02-14', 'BR', 'clicks', 52392);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7038, '2018-02-14', 'CA', 'views', 75142);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7039, '2018-02-14', 'CA', 'plays', 40001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7040, '2018-02-14', 'CA', 'clicks', 40836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7053, '2018-02-14', 'CY', 'views', 10383);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7054, '2018-02-14', 'CY', 'plays', 61335);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7055, '2018-02-14', 'CY', 'clicks', 69154);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7065, '2018-02-14', 'DE', 'views', 31719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7066, '2018-02-14', 'DE', 'plays', 52338);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7067, '2018-02-14', 'DE', 'clicks', 40000);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7056, '2018-02-14', 'EG', 'views', 69394);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7057, '2018-02-14', 'EG', 'plays', 60506);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7058, '2018-02-14', 'EG', 'clicks', 40487);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7062, '2018-02-14', 'FR', 'views', 37640);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7063, '2018-02-14', 'FR', 'plays', 21675);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7064, '2018-02-14', 'FR', 'clicks', 25168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7050, '2018-02-14', 'HR', 'views', 15857);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7051, '2018-02-14', 'HR', 'plays', 12833);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7052, '2018-02-14', 'HR', 'clicks', 30983);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7059, '2018-02-14', 'RS', 'views', 23308);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7060, '2018-02-14', 'RS', 'plays', 45773);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7061, '2018-02-14', 'RS', 'clicks', 65051);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7071, '2018-02-15', 'AT', 'views', 74781);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7072, '2018-02-15', 'AT', 'plays', 55082);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7073, '2018-02-15', 'AT', 'clicks', 55123);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7068, '2018-02-15', 'AU', 'views', 30649);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7069, '2018-02-15', 'AU', 'plays', 48418);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7070, '2018-02-15', 'AU', 'clicks', 66285);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7080, '2018-02-15', 'BA', 'views', 51311);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7081, '2018-02-15', 'BA', 'plays', 68438);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7082, '2018-02-15', 'BA', 'clicks', 37250);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7077, '2018-02-15', 'BE', 'views', 94496);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7078, '2018-02-15', 'BE', 'plays', 61365);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7079, '2018-02-15', 'BE', 'clicks', 25844);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7083, '2018-02-15', 'BR', 'views', 44187);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7084, '2018-02-15', 'BR', 'plays', 66628);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7085, '2018-02-15', 'BR', 'clicks', 52249);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7074, '2018-02-15', 'CA', 'views', 42960);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7075, '2018-02-15', 'CA', 'plays', 74011);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7076, '2018-02-15', 'CA', 'clicks', 56474);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7089, '2018-02-15', 'CY', 'views', 44787);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7090, '2018-02-15', 'CY', 'plays', 48883);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7091, '2018-02-15', 'CY', 'clicks', 45308);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7101, '2018-02-15', 'DE', 'views', 66024);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7102, '2018-02-15', 'DE', 'plays', 52935);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7103, '2018-02-15', 'DE', 'clicks', 36854);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7092, '2018-02-15', 'EG', 'views', 60004);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7093, '2018-02-15', 'EG', 'plays', 63796);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7094, '2018-02-15', 'EG', 'clicks', 71915);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7098, '2018-02-15', 'FR', 'views', 72442);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7099, '2018-02-15', 'FR', 'plays', 13656);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7100, '2018-02-15', 'FR', 'clicks', 49553);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7086, '2018-02-15', 'HR', 'views', 56635);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7087, '2018-02-15', 'HR', 'plays', 85012);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7088, '2018-02-15', 'HR', 'clicks', 59155);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7095, '2018-02-15', 'RS', 'views', 82469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7096, '2018-02-15', 'RS', 'plays', 33882);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7097, '2018-02-15', 'RS', 'clicks', 77765);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7107, '2018-02-16', 'AT', 'views', 46743);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7108, '2018-02-16', 'AT', 'plays', 30465);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7109, '2018-02-16', 'AT', 'clicks', 27259);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7104, '2018-02-16', 'AU', 'views', 2517);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7105, '2018-02-16', 'AU', 'plays', 36710);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7106, '2018-02-16', 'AU', 'clicks', 65516);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7116, '2018-02-16', 'BA', 'views', 31396);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7117, '2018-02-16', 'BA', 'plays', 36836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7118, '2018-02-16', 'BA', 'clicks', 26815);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7113, '2018-02-16', 'BE', 'views', 53850);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7114, '2018-02-16', 'BE', 'plays', 64779);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7115, '2018-02-16', 'BE', 'clicks', 45940);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7119, '2018-02-16', 'BR', 'views', 75668);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7120, '2018-02-16', 'BR', 'plays', 22027);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7121, '2018-02-16', 'BR', 'clicks', 34415);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7110, '2018-02-16', 'CA', 'views', 19070);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7111, '2018-02-16', 'CA', 'plays', 73490);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7112, '2018-02-16', 'CA', 'clicks', 12668);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7125, '2018-02-16', 'CY', 'views', 40649);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7126, '2018-02-16', 'CY', 'plays', 69783);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7127, '2018-02-16', 'CY', 'clicks', 21639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7137, '2018-02-16', 'DE', 'views', 23096);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7138, '2018-02-16', 'DE', 'plays', 34855);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7139, '2018-02-16', 'DE', 'clicks', 40035);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7128, '2018-02-16', 'EG', 'views', 17343);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7129, '2018-02-16', 'EG', 'plays', 51820);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7130, '2018-02-16', 'EG', 'clicks', 42481);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7134, '2018-02-16', 'FR', 'views', 33784);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7135, '2018-02-16', 'FR', 'plays', 37307);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7136, '2018-02-16', 'FR', 'clicks', 49758);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7122, '2018-02-16', 'HR', 'views', 77315);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7123, '2018-02-16', 'HR', 'plays', 67583);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7124, '2018-02-16', 'HR', 'clicks', 60062);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7131, '2018-02-16', 'RS', 'views', 11990);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7132, '2018-02-16', 'RS', 'plays', 32130);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7133, '2018-02-16', 'RS', 'clicks', 47246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7143, '2018-02-17', 'AT', 'views', 34783);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7144, '2018-02-17', 'AT', 'plays', 64230);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7145, '2018-02-17', 'AT', 'clicks', 58007);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7140, '2018-02-17', 'AU', 'views', 43433);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7141, '2018-02-17', 'AU', 'plays', 34048);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7142, '2018-02-17', 'AU', 'clicks', 88118);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7152, '2018-02-17', 'BA', 'views', 54414);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7153, '2018-02-17', 'BA', 'plays', 82998);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7154, '2018-02-17', 'BA', 'clicks', 75845);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7149, '2018-02-17', 'BE', 'views', 1591);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7150, '2018-02-17', 'BE', 'plays', 29138);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7151, '2018-02-17', 'BE', 'clicks', 21929);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7155, '2018-02-17', 'BR', 'views', 28991);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7156, '2018-02-17', 'BR', 'plays', 30097);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7157, '2018-02-17', 'BR', 'clicks', 67010);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7146, '2018-02-17', 'CA', 'views', 67852);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7147, '2018-02-17', 'CA', 'plays', 52719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7148, '2018-02-17', 'CA', 'clicks', 45701);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7161, '2018-02-17', 'CY', 'views', 59902);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7162, '2018-02-17', 'CY', 'plays', 55189);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7163, '2018-02-17', 'CY', 'clicks', 73937);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7173, '2018-02-17', 'DE', 'views', 9531);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7174, '2018-02-17', 'DE', 'plays', 43490);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7175, '2018-02-17', 'DE', 'clicks', 62035);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7164, '2018-02-17', 'EG', 'views', 14678);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7165, '2018-02-17', 'EG', 'plays', 33838);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7166, '2018-02-17', 'EG', 'clicks', 27115);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7170, '2018-02-17', 'FR', 'views', 54374);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7171, '2018-02-17', 'FR', 'plays', 58308);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7172, '2018-02-17', 'FR', 'clicks', 19470);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7158, '2018-02-17', 'HR', 'views', 67516);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7159, '2018-02-17', 'HR', 'plays', 25281);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7160, '2018-02-17', 'HR', 'clicks', 57183);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7167, '2018-02-17', 'RS', 'views', 48736);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7168, '2018-02-17', 'RS', 'plays', 71175);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7169, '2018-02-17', 'RS', 'clicks', 29807);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7179, '2018-02-18', 'AT', 'views', 64547);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7180, '2018-02-18', 'AT', 'plays', 30881);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7181, '2018-02-18', 'AT', 'clicks', 52358);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7176, '2018-02-18', 'AU', 'views', 44207);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7177, '2018-02-18', 'AU', 'plays', 71424);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7178, '2018-02-18', 'AU', 'clicks', 53015);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7188, '2018-02-18', 'BA', 'views', 41514);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7189, '2018-02-18', 'BA', 'plays', 58837);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7190, '2018-02-18', 'BA', 'clicks', 25418);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7185, '2018-02-18', 'BE', 'views', 46146);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7186, '2018-02-18', 'BE', 'plays', 66630);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7187, '2018-02-18', 'BE', 'clicks', 60847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7191, '2018-02-18', 'BR', 'views', 58245);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7192, '2018-02-18', 'BR', 'plays', 44255);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7193, '2018-02-18', 'BR', 'clicks', 48118);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7182, '2018-02-18', 'CA', 'views', 43124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7183, '2018-02-18', 'CA', 'plays', 19518);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7184, '2018-02-18', 'CA', 'clicks', 50177);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7197, '2018-02-18', 'CY', 'views', 41938);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7198, '2018-02-18', 'CY', 'plays', 40602);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7199, '2018-02-18', 'CY', 'clicks', 54360);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7209, '2018-02-18', 'DE', 'views', 44823);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7210, '2018-02-18', 'DE', 'plays', 56765);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7211, '2018-02-18', 'DE', 'clicks', 44176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7200, '2018-02-18', 'EG', 'views', 8905);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7201, '2018-02-18', 'EG', 'plays', 43134);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7202, '2018-02-18', 'EG', 'clicks', 43313);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7206, '2018-02-18', 'FR', 'views', 71500);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7207, '2018-02-18', 'FR', 'plays', 66064);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7208, '2018-02-18', 'FR', 'clicks', 48075);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7194, '2018-02-18', 'HR', 'views', 88832);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7195, '2018-02-18', 'HR', 'plays', 49628);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7196, '2018-02-18', 'HR', 'clicks', 15697);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7203, '2018-02-18', 'RS', 'views', 59330);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7204, '2018-02-18', 'RS', 'plays', 77126);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7205, '2018-02-18', 'RS', 'clicks', 67610);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7215, '2018-02-19', 'AT', 'views', 16351);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7216, '2018-02-19', 'AT', 'plays', 47999);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7217, '2018-02-19', 'AT', 'clicks', 19755);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7212, '2018-02-19', 'AU', 'views', 26301);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7213, '2018-02-19', 'AU', 'plays', 82953);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7214, '2018-02-19', 'AU', 'clicks', 1650);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7224, '2018-02-19', 'BA', 'views', 35830);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7225, '2018-02-19', 'BA', 'plays', 84204);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7226, '2018-02-19', 'BA', 'clicks', 79732);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7221, '2018-02-19', 'BE', 'views', 80839);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7222, '2018-02-19', 'BE', 'plays', 54567);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7223, '2018-02-19', 'BE', 'clicks', 28688);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7227, '2018-02-19', 'BR', 'views', 44994);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7228, '2018-02-19', 'BR', 'plays', 77725);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7229, '2018-02-19', 'BR', 'clicks', 54319);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7218, '2018-02-19', 'CA', 'views', 41709);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7219, '2018-02-19', 'CA', 'plays', 40170);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7220, '2018-02-19', 'CA', 'clicks', 35165);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7233, '2018-02-19', 'CY', 'views', 57361);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7234, '2018-02-19', 'CY', 'plays', 40068);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7235, '2018-02-19', 'CY', 'clicks', 39700);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7245, '2018-02-19', 'DE', 'views', 65368);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7246, '2018-02-19', 'DE', 'plays', 33039);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7247, '2018-02-19', 'DE', 'clicks', 36775);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7236, '2018-02-19', 'EG', 'views', 62568);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7237, '2018-02-19', 'EG', 'plays', 47351);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7238, '2018-02-19', 'EG', 'clicks', 55323);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7242, '2018-02-19', 'FR', 'views', 57899);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7243, '2018-02-19', 'FR', 'plays', 45144);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7244, '2018-02-19', 'FR', 'clicks', 80924);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7230, '2018-02-19', 'HR', 'views', 35597);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7231, '2018-02-19', 'HR', 'plays', 41911);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7232, '2018-02-19', 'HR', 'clicks', 41818);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7239, '2018-02-19', 'RS', 'views', 94679);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7240, '2018-02-19', 'RS', 'plays', 67904);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7241, '2018-02-19', 'RS', 'clicks', 95408);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7251, '2018-02-20', 'AT', 'views', 36424);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7252, '2018-02-20', 'AT', 'plays', 13900);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7253, '2018-02-20', 'AT', 'clicks', 14113);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7248, '2018-02-20', 'AU', 'views', 60260);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7249, '2018-02-20', 'AU', 'plays', 48405);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7250, '2018-02-20', 'AU', 'clicks', 43648);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7260, '2018-02-20', 'BA', 'views', 58456);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7261, '2018-02-20', 'BA', 'plays', 60681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7262, '2018-02-20', 'BA', 'clicks', 62221);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7257, '2018-02-20', 'BE', 'views', 43239);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7258, '2018-02-20', 'BE', 'plays', 65424);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7259, '2018-02-20', 'BE', 'clicks', 76190);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7263, '2018-02-20', 'BR', 'views', 82927);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7264, '2018-02-20', 'BR', 'plays', 5749);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7265, '2018-02-20', 'BR', 'clicks', 82166);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7254, '2018-02-20', 'CA', 'views', 70332);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7255, '2018-02-20', 'CA', 'plays', 47583);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7256, '2018-02-20', 'CA', 'clicks', 25203);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7269, '2018-02-20', 'CY', 'views', 90172);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7270, '2018-02-20', 'CY', 'plays', 47380);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7271, '2018-02-20', 'CY', 'clicks', 63622);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7281, '2018-02-20', 'DE', 'views', 43556);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7282, '2018-02-20', 'DE', 'plays', 63283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7283, '2018-02-20', 'DE', 'clicks', 80873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7272, '2018-02-20', 'EG', 'views', 33929);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7273, '2018-02-20', 'EG', 'plays', 63236);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7274, '2018-02-20', 'EG', 'clicks', 87609);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7278, '2018-02-20', 'FR', 'views', 85690);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7279, '2018-02-20', 'FR', 'plays', 28657);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7280, '2018-02-20', 'FR', 'clicks', 66568);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7266, '2018-02-20', 'HR', 'views', 47334);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7267, '2018-02-20', 'HR', 'plays', 74157);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7268, '2018-02-20', 'HR', 'clicks', 91374);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7275, '2018-02-20', 'RS', 'views', 51089);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7276, '2018-02-20', 'RS', 'plays', 80682);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7277, '2018-02-20', 'RS', 'clicks', 33983);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7287, '2018-02-21', 'AT', 'views', 26096);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7288, '2018-02-21', 'AT', 'plays', 36507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7289, '2018-02-21', 'AT', 'clicks', 26469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7284, '2018-02-21', 'AU', 'views', 89604);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7285, '2018-02-21', 'AU', 'plays', 68980);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7286, '2018-02-21', 'AU', 'clicks', 37227);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7296, '2018-02-21', 'BA', 'views', 72245);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7297, '2018-02-21', 'BA', 'plays', 54230);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7298, '2018-02-21', 'BA', 'clicks', 44368);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7293, '2018-02-21', 'BE', 'views', 57909);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7294, '2018-02-21', 'BE', 'plays', 57327);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7295, '2018-02-21', 'BE', 'clicks', 54063);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7299, '2018-02-21', 'BR', 'views', 42819);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7300, '2018-02-21', 'BR', 'plays', 44129);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7301, '2018-02-21', 'BR', 'clicks', 16663);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7290, '2018-02-21', 'CA', 'views', 84396);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7291, '2018-02-21', 'CA', 'plays', 80088);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7292, '2018-02-21', 'CA', 'clicks', 17664);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7305, '2018-02-21', 'CY', 'views', 45972);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7306, '2018-02-21', 'CY', 'plays', 46084);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7307, '2018-02-21', 'CY', 'clicks', 39541);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7317, '2018-02-21', 'DE', 'views', 51560);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7318, '2018-02-21', 'DE', 'plays', 53017);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7319, '2018-02-21', 'DE', 'clicks', 78042);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7308, '2018-02-21', 'EG', 'views', 62054);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7309, '2018-02-21', 'EG', 'plays', 57873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7310, '2018-02-21', 'EG', 'clicks', 56087);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7314, '2018-02-21', 'FR', 'views', 79401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7315, '2018-02-21', 'FR', 'plays', 57581);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7316, '2018-02-21', 'FR', 'clicks', 48064);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7302, '2018-02-21', 'HR', 'views', 63417);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7303, '2018-02-21', 'HR', 'plays', 48936);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7304, '2018-02-21', 'HR', 'clicks', 28544);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7311, '2018-02-21', 'RS', 'views', 46998);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7312, '2018-02-21', 'RS', 'plays', 19931);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7313, '2018-02-21', 'RS', 'clicks', 69390);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7323, '2018-02-22', 'AT', 'views', 49248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7324, '2018-02-22', 'AT', 'plays', 15998);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7325, '2018-02-22', 'AT', 'clicks', 40069);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7320, '2018-02-22', 'AU', 'views', 46946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7321, '2018-02-22', 'AU', 'plays', 65488);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7322, '2018-02-22', 'AU', 'clicks', 53164);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7332, '2018-02-22', 'BA', 'views', 72251);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7333, '2018-02-22', 'BA', 'plays', 40683);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7334, '2018-02-22', 'BA', 'clicks', 58843);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7329, '2018-02-22', 'BE', 'views', 45865);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7330, '2018-02-22', 'BE', 'plays', 43877);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7331, '2018-02-22', 'BE', 'clicks', 41577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7335, '2018-02-22', 'BR', 'views', 28174);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7336, '2018-02-22', 'BR', 'plays', 28188);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7337, '2018-02-22', 'BR', 'clicks', 60975);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7326, '2018-02-22', 'CA', 'views', 49339);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7327, '2018-02-22', 'CA', 'plays', 37538);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7328, '2018-02-22', 'CA', 'clicks', 66994);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7341, '2018-02-22', 'CY', 'views', 19983);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7342, '2018-02-22', 'CY', 'plays', 87902);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7343, '2018-02-22', 'CY', 'clicks', 61796);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7353, '2018-02-22', 'DE', 'views', 67826);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7354, '2018-02-22', 'DE', 'plays', 31337);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7355, '2018-02-22', 'DE', 'clicks', 46251);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7344, '2018-02-22', 'EG', 'views', 41674);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7345, '2018-02-22', 'EG', 'plays', 17819);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7346, '2018-02-22', 'EG', 'clicks', 88581);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7350, '2018-02-22', 'FR', 'views', 65383);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7351, '2018-02-22', 'FR', 'plays', 59078);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7352, '2018-02-22', 'FR', 'clicks', 68069);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7338, '2018-02-22', 'HR', 'views', 54667);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7339, '2018-02-22', 'HR', 'plays', 57674);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7340, '2018-02-22', 'HR', 'clicks', 46164);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7347, '2018-02-22', 'RS', 'views', 58783);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7348, '2018-02-22', 'RS', 'plays', 66874);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7349, '2018-02-22', 'RS', 'clicks', 60834);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7359, '2018-02-23', 'AT', 'views', 76520);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7360, '2018-02-23', 'AT', 'plays', 73467);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7361, '2018-02-23', 'AT', 'clicks', 52964);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7356, '2018-02-23', 'AU', 'views', 39183);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7357, '2018-02-23', 'AU', 'plays', 33916);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7358, '2018-02-23', 'AU', 'clicks', 69293);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7368, '2018-02-23', 'BA', 'views', 40236);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7369, '2018-02-23', 'BA', 'plays', 68915);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7370, '2018-02-23', 'BA', 'clicks', 61779);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7365, '2018-02-23', 'BE', 'views', 35880);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7366, '2018-02-23', 'BE', 'plays', 7734);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7367, '2018-02-23', 'BE', 'clicks', 52629);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7371, '2018-02-23', 'BR', 'views', 45021);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7372, '2018-02-23', 'BR', 'plays', 94704);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7373, '2018-02-23', 'BR', 'clicks', 70283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7362, '2018-02-23', 'CA', 'views', 60055);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7363, '2018-02-23', 'CA', 'plays', 67707);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7364, '2018-02-23', 'CA', 'clicks', 50396);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7377, '2018-02-23', 'CY', 'views', 87348);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7378, '2018-02-23', 'CY', 'plays', 49289);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7379, '2018-02-23', 'CY', 'clicks', 53434);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7389, '2018-02-23', 'DE', 'views', 77444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7390, '2018-02-23', 'DE', 'plays', 65669);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7391, '2018-02-23', 'DE', 'clicks', 55456);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7380, '2018-02-23', 'EG', 'views', 58313);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7381, '2018-02-23', 'EG', 'plays', 69836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7382, '2018-02-23', 'EG', 'clicks', 85565);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7386, '2018-02-23', 'FR', 'views', 36114);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7387, '2018-02-23', 'FR', 'plays', 50139);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7388, '2018-02-23', 'FR', 'clicks', 73143);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7374, '2018-02-23', 'HR', 'views', 60551);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7375, '2018-02-23', 'HR', 'plays', 88022);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7376, '2018-02-23', 'HR', 'clicks', 65263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7383, '2018-02-23', 'RS', 'views', 14908);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7384, '2018-02-23', 'RS', 'plays', 78794);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7385, '2018-02-23', 'RS', 'clicks', 15101);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7395, '2018-02-24', 'AT', 'views', 68582);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7396, '2018-02-24', 'AT', 'plays', 57856);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7397, '2018-02-24', 'AT', 'clicks', 6653);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7392, '2018-02-24', 'AU', 'views', 76162);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7393, '2018-02-24', 'AU', 'plays', 65865);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7394, '2018-02-24', 'AU', 'clicks', 55829);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7404, '2018-02-24', 'BA', 'views', 21984);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7405, '2018-02-24', 'BA', 'plays', 72443);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7406, '2018-02-24', 'BA', 'clicks', 42703);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7401, '2018-02-24', 'BE', 'views', 86611);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7402, '2018-02-24', 'BE', 'plays', 44624);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7403, '2018-02-24', 'BE', 'clicks', 63733);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7407, '2018-02-24', 'BR', 'views', 68588);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7408, '2018-02-24', 'BR', 'plays', 58917);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7409, '2018-02-24', 'BR', 'clicks', 66002);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7398, '2018-02-24', 'CA', 'views', 50315);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7399, '2018-02-24', 'CA', 'plays', 58036);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7400, '2018-02-24', 'CA', 'clicks', 60019);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7413, '2018-02-24', 'CY', 'views', 68401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7414, '2018-02-24', 'CY', 'plays', 97484);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7415, '2018-02-24', 'CY', 'clicks', 83202);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7425, '2018-02-24', 'DE', 'views', 20609);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7426, '2018-02-24', 'DE', 'plays', 29079);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7427, '2018-02-24', 'DE', 'clicks', 16283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7416, '2018-02-24', 'EG', 'views', 59927);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7417, '2018-02-24', 'EG', 'plays', 72387);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7418, '2018-02-24', 'EG', 'clicks', 54560);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7422, '2018-02-24', 'FR', 'views', 72805);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7423, '2018-02-24', 'FR', 'plays', 49378);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7424, '2018-02-24', 'FR', 'clicks', 53251);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7410, '2018-02-24', 'HR', 'views', 49405);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7411, '2018-02-24', 'HR', 'plays', 26414);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7412, '2018-02-24', 'HR', 'clicks', 63151);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7419, '2018-02-24', 'RS', 'views', 19161);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7420, '2018-02-24', 'RS', 'plays', 39173);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7421, '2018-02-24', 'RS', 'clicks', 19246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7431, '2018-02-25', 'AT', 'views', 39499);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7432, '2018-02-25', 'AT', 'plays', 51423);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7433, '2018-02-25', 'AT', 'clicks', 57934);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7428, '2018-02-25', 'AU', 'views', 48571);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7429, '2018-02-25', 'AU', 'plays', 43168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7430, '2018-02-25', 'AU', 'clicks', 68360);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7440, '2018-02-25', 'BA', 'views', 92060);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7441, '2018-02-25', 'BA', 'plays', 68745);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7442, '2018-02-25', 'BA', 'clicks', 46144);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7437, '2018-02-25', 'BE', 'views', 27814);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7438, '2018-02-25', 'BE', 'plays', 59076);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7439, '2018-02-25', 'BE', 'clicks', 27237);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7443, '2018-02-25', 'BR', 'views', 37470);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7444, '2018-02-25', 'BR', 'plays', 46353);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7445, '2018-02-25', 'BR', 'clicks', 48083);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7434, '2018-02-25', 'CA', 'views', 47644);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7435, '2018-02-25', 'CA', 'plays', 93471);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7436, '2018-02-25', 'CA', 'clicks', 34124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7449, '2018-02-25', 'CY', 'views', 69833);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7450, '2018-02-25', 'CY', 'plays', 58576);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7451, '2018-02-25', 'CY', 'clicks', 87949);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7461, '2018-02-25', 'DE', 'views', 78011);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7462, '2018-02-25', 'DE', 'plays', 22741);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7463, '2018-02-25', 'DE', 'clicks', 28020);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7452, '2018-02-25', 'EG', 'views', 18287);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7453, '2018-02-25', 'EG', 'plays', 69462);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7454, '2018-02-25', 'EG', 'clicks', 54781);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7458, '2018-02-25', 'FR', 'views', 29612);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7459, '2018-02-25', 'FR', 'plays', 24782);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7460, '2018-02-25', 'FR', 'clicks', 55280);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7446, '2018-02-25', 'HR', 'views', 19711);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7447, '2018-02-25', 'HR', 'plays', 69021);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7448, '2018-02-25', 'HR', 'clicks', 58731);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7455, '2018-02-25', 'RS', 'views', 49469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7456, '2018-02-25', 'RS', 'plays', 37600);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7457, '2018-02-25', 'RS', 'clicks', 20499);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7467, '2018-02-26', 'AT', 'views', 50241);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7468, '2018-02-26', 'AT', 'plays', 8407);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7469, '2018-02-26', 'AT', 'clicks', 49192);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7464, '2018-02-26', 'AU', 'views', 23915);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7465, '2018-02-26', 'AU', 'plays', 40283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7466, '2018-02-26', 'AU', 'clicks', 15111);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7476, '2018-02-26', 'BA', 'views', 20109);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7477, '2018-02-26', 'BA', 'plays', 52001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7478, '2018-02-26', 'BA', 'clicks', 46479);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7473, '2018-02-26', 'BE', 'views', 47512);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7474, '2018-02-26', 'BE', 'plays', 51784);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7475, '2018-02-26', 'BE', 'clicks', 48205);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7479, '2018-02-26', 'BR', 'views', 72189);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7480, '2018-02-26', 'BR', 'plays', 38441);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7481, '2018-02-26', 'BR', 'clicks', 43525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7470, '2018-02-26', 'CA', 'views', 40222);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7471, '2018-02-26', 'CA', 'plays', 50848);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7472, '2018-02-26', 'CA', 'clicks', 67794);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7485, '2018-02-26', 'CY', 'views', 33388);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7486, '2018-02-26', 'CY', 'plays', 37234);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7487, '2018-02-26', 'CY', 'clicks', 63375);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7497, '2018-02-26', 'DE', 'views', 41973);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7498, '2018-02-26', 'DE', 'plays', 57878);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7499, '2018-02-26', 'DE', 'clicks', 31811);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7488, '2018-02-26', 'EG', 'views', 47951);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7489, '2018-02-26', 'EG', 'plays', 28808);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7490, '2018-02-26', 'EG', 'clicks', 57609);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7494, '2018-02-26', 'FR', 'views', 50910);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7495, '2018-02-26', 'FR', 'plays', 69320);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7496, '2018-02-26', 'FR', 'clicks', 36225);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7482, '2018-02-26', 'HR', 'views', 50266);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7483, '2018-02-26', 'HR', 'plays', 72522);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7484, '2018-02-26', 'HR', 'clicks', 80670);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7491, '2018-02-26', 'RS', 'views', 74365);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7492, '2018-02-26', 'RS', 'plays', 52921);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7493, '2018-02-26', 'RS', 'clicks', 53618);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7503, '2018-02-27', 'AT', 'views', 74575);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7504, '2018-02-27', 'AT', 'plays', 53522);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7505, '2018-02-27', 'AT', 'clicks', 46668);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7500, '2018-02-27', 'AU', 'views', 29666);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7501, '2018-02-27', 'AU', 'plays', 25811);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7502, '2018-02-27', 'AU', 'clicks', 80173);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7512, '2018-02-27', 'BA', 'views', 49325);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7513, '2018-02-27', 'BA', 'plays', 23239);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7514, '2018-02-27', 'BA', 'clicks', 80739);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7509, '2018-02-27', 'BE', 'views', 63373);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7510, '2018-02-27', 'BE', 'plays', 67835);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7511, '2018-02-27', 'BE', 'clicks', 10397);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7515, '2018-02-27', 'BR', 'views', 80954);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7516, '2018-02-27', 'BR', 'plays', 37533);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7517, '2018-02-27', 'BR', 'clicks', 58756);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7506, '2018-02-27', 'CA', 'views', 64143);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7507, '2018-02-27', 'CA', 'plays', 72071);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7508, '2018-02-27', 'CA', 'clicks', 19759);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7521, '2018-02-27', 'CY', 'views', 38817);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7522, '2018-02-27', 'CY', 'plays', 41527);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7523, '2018-02-27', 'CY', 'clicks', 30627);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7533, '2018-02-27', 'DE', 'views', 73946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7534, '2018-02-27', 'DE', 'plays', 41500);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7535, '2018-02-27', 'DE', 'clicks', 39171);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7524, '2018-02-27', 'EG', 'views', 30831);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7525, '2018-02-27', 'EG', 'plays', 56739);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7526, '2018-02-27', 'EG', 'clicks', 61398);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7530, '2018-02-27', 'FR', 'views', 15686);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7531, '2018-02-27', 'FR', 'plays', 25343);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7532, '2018-02-27', 'FR', 'clicks', 28065);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7518, '2018-02-27', 'HR', 'views', 32836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7519, '2018-02-27', 'HR', 'plays', 45704);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7520, '2018-02-27', 'HR', 'clicks', 74357);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7527, '2018-02-27', 'RS', 'views', 58138);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7528, '2018-02-27', 'RS', 'plays', 71698);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7529, '2018-02-27', 'RS', 'clicks', 53798);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7539, '2018-02-28', 'AT', 'views', 41870);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7540, '2018-02-28', 'AT', 'plays', 96019);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7541, '2018-02-28', 'AT', 'clicks', 27444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7536, '2018-02-28', 'AU', 'views', 62352);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7537, '2018-02-28', 'AU', 'plays', 49742);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7538, '2018-02-28', 'AU', 'clicks', 61575);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7548, '2018-02-28', 'BA', 'views', 67493);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7549, '2018-02-28', 'BA', 'plays', 84525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7550, '2018-02-28', 'BA', 'clicks', 58787);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7545, '2018-02-28', 'BE', 'views', 40037);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7546, '2018-02-28', 'BE', 'plays', 38922);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7547, '2018-02-28', 'BE', 'clicks', 31898);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7551, '2018-02-28', 'BR', 'views', 27772);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7552, '2018-02-28', 'BR', 'plays', 49271);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7553, '2018-02-28', 'BR', 'clicks', 40997);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7542, '2018-02-28', 'CA', 'views', 43075);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7543, '2018-02-28', 'CA', 'plays', 71115);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7544, '2018-02-28', 'CA', 'clicks', 31086);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7557, '2018-02-28', 'CY', 'views', 53673);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7558, '2018-02-28', 'CY', 'plays', 91711);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7559, '2018-02-28', 'CY', 'clicks', 40425);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7569, '2018-02-28', 'DE', 'views', 85321);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7570, '2018-02-28', 'DE', 'plays', 48515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7571, '2018-02-28', 'DE', 'clicks', 33083);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7560, '2018-02-28', 'EG', 'views', 25836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7561, '2018-02-28', 'EG', 'plays', 51379);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7562, '2018-02-28', 'EG', 'clicks', 50837);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7566, '2018-02-28', 'FR', 'views', 67206);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7567, '2018-02-28', 'FR', 'plays', 78050);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7568, '2018-02-28', 'FR', 'clicks', 58731);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7554, '2018-02-28', 'HR', 'views', 78359);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7555, '2018-02-28', 'HR', 'plays', 16753);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7556, '2018-02-28', 'HR', 'clicks', 35459);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7563, '2018-02-28', 'RS', 'views', 14548);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7564, '2018-02-28', 'RS', 'plays', 49487);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7565, '2018-02-28', 'RS', 'clicks', 65057);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7575, '2018-03-01', 'AT', 'views', 60156);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7576, '2018-03-01', 'AT', 'plays', 50706);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7577, '2018-03-01', 'AT', 'clicks', 51105);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7572, '2018-03-01', 'AU', 'views', 25085);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7573, '2018-03-01', 'AU', 'plays', 65334);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7574, '2018-03-01', 'AU', 'clicks', 62835);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7584, '2018-03-01', 'BA', 'views', 39654);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7585, '2018-03-01', 'BA', 'plays', 77359);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7586, '2018-03-01', 'BA', 'clicks', 66414);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7581, '2018-03-01', 'BE', 'views', 55241);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7582, '2018-03-01', 'BE', 'plays', 48727);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7583, '2018-03-01', 'BE', 'clicks', 24869);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7587, '2018-03-01', 'BR', 'views', 48830);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7588, '2018-03-01', 'BR', 'plays', 67275);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7589, '2018-03-01', 'BR', 'clicks', 70437);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7578, '2018-03-01', 'CA', 'views', 40074);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7579, '2018-03-01', 'CA', 'plays', 39696);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7580, '2018-03-01', 'CA', 'clicks', 44190);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7593, '2018-03-01', 'CY', 'views', 39661);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7594, '2018-03-01', 'CY', 'plays', 22836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7595, '2018-03-01', 'CY', 'clicks', 21012);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7605, '2018-03-01', 'DE', 'views', 60539);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7606, '2018-03-01', 'DE', 'plays', 68500);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7607, '2018-03-01', 'DE', 'clicks', 68719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7596, '2018-03-01', 'EG', 'views', 44408);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7597, '2018-03-01', 'EG', 'plays', 50580);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7598, '2018-03-01', 'EG', 'clicks', 33636);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7602, '2018-03-01', 'FR', 'views', 25897);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7603, '2018-03-01', 'FR', 'plays', 55223);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7604, '2018-03-01', 'FR', 'clicks', 67509);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7590, '2018-03-01', 'HR', 'views', 27900);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7591, '2018-03-01', 'HR', 'plays', 61883);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7592, '2018-03-01', 'HR', 'clicks', 57538);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7599, '2018-03-01', 'RS', 'views', 53337);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7600, '2018-03-01', 'RS', 'plays', 64477);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7601, '2018-03-01', 'RS', 'clicks', 72515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7611, '2018-03-02', 'AT', 'views', 43228);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7612, '2018-03-02', 'AT', 'plays', 57235);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7613, '2018-03-02', 'AT', 'clicks', 62250);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7608, '2018-03-02', 'AU', 'views', 41201);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7609, '2018-03-02', 'AU', 'plays', 38892);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7610, '2018-03-02', 'AU', 'clicks', 57052);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7620, '2018-03-02', 'BA', 'views', 56235);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7621, '2018-03-02', 'BA', 'plays', 39338);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7622, '2018-03-02', 'BA', 'clicks', 40866);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7617, '2018-03-02', 'BE', 'views', 26031);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7618, '2018-03-02', 'BE', 'plays', 52072);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7619, '2018-03-02', 'BE', 'clicks', 84035);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7623, '2018-03-02', 'BR', 'views', 52335);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7624, '2018-03-02', 'BR', 'plays', 31931);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7625, '2018-03-02', 'BR', 'clicks', 58781);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7614, '2018-03-02', 'CA', 'views', 45050);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7615, '2018-03-02', 'CA', 'plays', 18289);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7616, '2018-03-02', 'CA', 'clicks', 67227);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7629, '2018-03-02', 'CY', 'views', 20281);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7630, '2018-03-02', 'CY', 'plays', 18494);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7631, '2018-03-02', 'CY', 'clicks', 35740);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7641, '2018-03-02', 'DE', 'views', 48619);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7642, '2018-03-02', 'DE', 'plays', 66233);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7643, '2018-03-02', 'DE', 'clicks', 60072);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7632, '2018-03-02', 'EG', 'views', 61890);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7633, '2018-03-02', 'EG', 'plays', 51697);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7634, '2018-03-02', 'EG', 'clicks', 54037);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7638, '2018-03-02', 'FR', 'views', 35293);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7639, '2018-03-02', 'FR', 'plays', 65737);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7640, '2018-03-02', 'FR', 'clicks', 59621);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7626, '2018-03-02', 'HR', 'views', 33760);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7627, '2018-03-02', 'HR', 'plays', 47004);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7628, '2018-03-02', 'HR', 'clicks', 83602);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7635, '2018-03-02', 'RS', 'views', 72677);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7636, '2018-03-02', 'RS', 'plays', 27896);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7637, '2018-03-02', 'RS', 'clicks', 35359);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7647, '2018-03-03', 'AT', 'views', 45571);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7648, '2018-03-03', 'AT', 'plays', 74256);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7649, '2018-03-03', 'AT', 'clicks', 72862);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7644, '2018-03-03', 'AU', 'views', 56561);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7645, '2018-03-03', 'AU', 'plays', 51355);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7646, '2018-03-03', 'AU', 'clicks', 62416);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7656, '2018-03-03', 'BA', 'views', 78926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7657, '2018-03-03', 'BA', 'plays', 89920);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7658, '2018-03-03', 'BA', 'clicks', 40513);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7653, '2018-03-03', 'BE', 'views', 48944);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7654, '2018-03-03', 'BE', 'plays', 12784);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7655, '2018-03-03', 'BE', 'clicks', 77686);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7659, '2018-03-03', 'BR', 'views', 50248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7660, '2018-03-03', 'BR', 'plays', 33627);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7661, '2018-03-03', 'BR', 'clicks', 53842);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7650, '2018-03-03', 'CA', 'views', 58374);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7651, '2018-03-03', 'CA', 'plays', 36147);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7652, '2018-03-03', 'CA', 'clicks', 60315);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7665, '2018-03-03', 'CY', 'views', 41107);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7666, '2018-03-03', 'CY', 'plays', 64333);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7667, '2018-03-03', 'CY', 'clicks', 55516);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7677, '2018-03-03', 'DE', 'views', 37084);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7678, '2018-03-03', 'DE', 'plays', 19705);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7679, '2018-03-03', 'DE', 'clicks', 25740);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7668, '2018-03-03', 'EG', 'views', 63847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7669, '2018-03-03', 'EG', 'plays', 52507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7670, '2018-03-03', 'EG', 'clicks', 60759);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7674, '2018-03-03', 'FR', 'views', 12032);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7675, '2018-03-03', 'FR', 'plays', 49769);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7676, '2018-03-03', 'FR', 'clicks', 74985);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7662, '2018-03-03', 'HR', 'views', 48588);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7663, '2018-03-03', 'HR', 'plays', 38782);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7664, '2018-03-03', 'HR', 'clicks', 22828);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7671, '2018-03-03', 'RS', 'views', 94014);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7672, '2018-03-03', 'RS', 'plays', 80987);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7673, '2018-03-03', 'RS', 'clicks', 43188);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7683, '2018-03-04', 'AT', 'views', 44141);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7684, '2018-03-04', 'AT', 'plays', 50766);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7685, '2018-03-04', 'AT', 'clicks', 14347);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7680, '2018-03-04', 'AU', 'views', 51482);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7681, '2018-03-04', 'AU', 'plays', 51837);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7682, '2018-03-04', 'AU', 'clicks', 55985);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7692, '2018-03-04', 'BA', 'views', 18862);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7693, '2018-03-04', 'BA', 'plays', 80193);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7694, '2018-03-04', 'BA', 'clicks', 27314);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7689, '2018-03-04', 'BE', 'views', 39868);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7690, '2018-03-04', 'BE', 'plays', 83278);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7691, '2018-03-04', 'BE', 'clicks', 65890);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7695, '2018-03-04', 'BR', 'views', 43026);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7696, '2018-03-04', 'BR', 'plays', 27235);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7697, '2018-03-04', 'BR', 'clicks', 58681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7686, '2018-03-04', 'CA', 'views', 24213);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7687, '2018-03-04', 'CA', 'plays', 10948);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7688, '2018-03-04', 'CA', 'clicks', 65384);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7701, '2018-03-04', 'CY', 'views', 51836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7702, '2018-03-04', 'CY', 'plays', 58167);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7703, '2018-03-04', 'CY', 'clicks', 44810);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7713, '2018-03-04', 'DE', 'views', 62652);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7714, '2018-03-04', 'DE', 'plays', 21745);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7715, '2018-03-04', 'DE', 'clicks', 44964);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7704, '2018-03-04', 'EG', 'views', 63709);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7705, '2018-03-04', 'EG', 'plays', 37051);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7706, '2018-03-04', 'EG', 'clicks', 44676);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7710, '2018-03-04', 'FR', 'views', 84640);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7711, '2018-03-04', 'FR', 'plays', 91880);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7712, '2018-03-04', 'FR', 'clicks', 24146);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7698, '2018-03-04', 'HR', 'views', 57385);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7699, '2018-03-04', 'HR', 'plays', 44158);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7700, '2018-03-04', 'HR', 'clicks', 30258);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7707, '2018-03-04', 'RS', 'views', 45899);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7708, '2018-03-04', 'RS', 'plays', 74045);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7709, '2018-03-04', 'RS', 'clicks', 18837);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7719, '2018-03-05', 'AT', 'views', 57036);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7720, '2018-03-05', 'AT', 'plays', 31392);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7721, '2018-03-05', 'AT', 'clicks', 17516);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7716, '2018-03-05', 'AU', 'views', 64711);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7717, '2018-03-05', 'AU', 'plays', 54933);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7718, '2018-03-05', 'AU', 'clicks', 43120);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7728, '2018-03-05', 'BA', 'views', 52210);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7729, '2018-03-05', 'BA', 'plays', 82321);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7730, '2018-03-05', 'BA', 'clicks', 68475);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7725, '2018-03-05', 'BE', 'views', 60557);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7726, '2018-03-05', 'BE', 'plays', 58195);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7727, '2018-03-05', 'BE', 'clicks', 6417);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7731, '2018-03-05', 'BR', 'views', 22050);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7732, '2018-03-05', 'BR', 'plays', 43309);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7733, '2018-03-05', 'BR', 'clicks', 69319);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7722, '2018-03-05', 'CA', 'views', 77984);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7723, '2018-03-05', 'CA', 'plays', 4498);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7724, '2018-03-05', 'CA', 'clicks', 17867);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7737, '2018-03-05', 'CY', 'views', 64006);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7738, '2018-03-05', 'CY', 'plays', 14351);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7739, '2018-03-05', 'CY', 'clicks', 41274);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7749, '2018-03-05', 'DE', 'views', 20684);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7750, '2018-03-05', 'DE', 'plays', 86168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7751, '2018-03-05', 'DE', 'clicks', 62234);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7740, '2018-03-05', 'EG', 'views', 25631);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7741, '2018-03-05', 'EG', 'plays', 47684);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7742, '2018-03-05', 'EG', 'clicks', 40636);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7746, '2018-03-05', 'FR', 'views', 56083);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7747, '2018-03-05', 'FR', 'plays', 49229);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7748, '2018-03-05', 'FR', 'clicks', 42585);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7734, '2018-03-05', 'HR', 'views', 53854);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7735, '2018-03-05', 'HR', 'plays', 18862);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7736, '2018-03-05', 'HR', 'clicks', 50685);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7743, '2018-03-05', 'RS', 'views', 69125);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7744, '2018-03-05', 'RS', 'plays', 74554);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7745, '2018-03-05', 'RS', 'clicks', 70192);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7755, '2018-03-06', 'AT', 'views', 86422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7756, '2018-03-06', 'AT', 'plays', 45216);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7757, '2018-03-06', 'AT', 'clicks', 51999);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7752, '2018-03-06', 'AU', 'views', 73046);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7753, '2018-03-06', 'AU', 'plays', 61884);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7754, '2018-03-06', 'AU', 'clicks', 55624);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7764, '2018-03-06', 'BA', 'views', 48115);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7765, '2018-03-06', 'BA', 'plays', 44157);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7766, '2018-03-06', 'BA', 'clicks', 45573);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7761, '2018-03-06', 'BE', 'views', 80578);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7762, '2018-03-06', 'BE', 'plays', 67092);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7763, '2018-03-06', 'BE', 'clicks', 72014);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7767, '2018-03-06', 'BR', 'views', 34799);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7768, '2018-03-06', 'BR', 'plays', 92285);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7769, '2018-03-06', 'BR', 'clicks', 86768);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7758, '2018-03-06', 'CA', 'views', 42821);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7759, '2018-03-06', 'CA', 'plays', 55516);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7760, '2018-03-06', 'CA', 'clicks', 42215);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7773, '2018-03-06', 'CY', 'views', 69072);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7774, '2018-03-06', 'CY', 'plays', 63206);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7775, '2018-03-06', 'CY', 'clicks', 75630);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7785, '2018-03-06', 'DE', 'views', 52476);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7786, '2018-03-06', 'DE', 'plays', 33997);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7787, '2018-03-06', 'DE', 'clicks', 25416);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7776, '2018-03-06', 'EG', 'views', 44622);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7777, '2018-03-06', 'EG', 'plays', 28028);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7778, '2018-03-06', 'EG', 'clicks', 38163);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7782, '2018-03-06', 'FR', 'views', 75228);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7783, '2018-03-06', 'FR', 'plays', 38129);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7784, '2018-03-06', 'FR', 'clicks', 71674);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7770, '2018-03-06', 'HR', 'views', 51375);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7771, '2018-03-06', 'HR', 'plays', 33122);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7772, '2018-03-06', 'HR', 'clicks', 79247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7779, '2018-03-06', 'RS', 'views', 85794);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7780, '2018-03-06', 'RS', 'plays', 17593);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7781, '2018-03-06', 'RS', 'clicks', 39854);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7791, '2018-03-07', 'AT', 'views', 45410);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7792, '2018-03-07', 'AT', 'plays', 55685);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7793, '2018-03-07', 'AT', 'clicks', 50545);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7788, '2018-03-07', 'AU', 'views', 59491);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7789, '2018-03-07', 'AU', 'plays', 36922);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7790, '2018-03-07', 'AU', 'clicks', 88046);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7800, '2018-03-07', 'BA', 'views', 55663);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7801, '2018-03-07', 'BA', 'plays', 48109);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7802, '2018-03-07', 'BA', 'clicks', 77360);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7797, '2018-03-07', 'BE', 'views', 51505);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7798, '2018-03-07', 'BE', 'plays', 43144);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7799, '2018-03-07', 'BE', 'clicks', 19136);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7803, '2018-03-07', 'BR', 'views', 77052);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7804, '2018-03-07', 'BR', 'plays', 47354);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7805, '2018-03-07', 'BR', 'clicks', 40229);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7794, '2018-03-07', 'CA', 'views', 74104);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7795, '2018-03-07', 'CA', 'plays', 65072);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7796, '2018-03-07', 'CA', 'clicks', 30356);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7809, '2018-03-07', 'CY', 'views', 41323);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7810, '2018-03-07', 'CY', 'plays', 33565);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7811, '2018-03-07', 'CY', 'clicks', 66104);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7821, '2018-03-07', 'DE', 'views', 62543);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7822, '2018-03-07', 'DE', 'plays', 34791);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7823, '2018-03-07', 'DE', 'clicks', 25403);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7812, '2018-03-07', 'EG', 'views', 47533);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7813, '2018-03-07', 'EG', 'plays', 20251);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7814, '2018-03-07', 'EG', 'clicks', 73437);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7818, '2018-03-07', 'FR', 'views', 81049);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7819, '2018-03-07', 'FR', 'plays', 82599);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7820, '2018-03-07', 'FR', 'clicks', 93006);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7806, '2018-03-07', 'HR', 'views', 73323);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7807, '2018-03-07', 'HR', 'plays', 47331);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7808, '2018-03-07', 'HR', 'clicks', 55551);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7815, '2018-03-07', 'RS', 'views', 42640);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7816, '2018-03-07', 'RS', 'plays', 67857);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7817, '2018-03-07', 'RS', 'clicks', 43489);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7827, '2018-03-08', 'AT', 'views', 28473);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7828, '2018-03-08', 'AT', 'plays', 83765);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7829, '2018-03-08', 'AT', 'clicks', 10654);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7824, '2018-03-08', 'AU', 'views', 81338);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7825, '2018-03-08', 'AU', 'plays', 37180);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7826, '2018-03-08', 'AU', 'clicks', 77399);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7836, '2018-03-08', 'BA', 'views', 23835);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7837, '2018-03-08', 'BA', 'plays', 72433);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7838, '2018-03-08', 'BA', 'clicks', 57999);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7833, '2018-03-08', 'BE', 'views', 74564);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7834, '2018-03-08', 'BE', 'plays', 48379);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7835, '2018-03-08', 'BE', 'clicks', 87880);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7839, '2018-03-08', 'BR', 'views', 49131);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7840, '2018-03-08', 'BR', 'plays', 69624);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7841, '2018-03-08', 'BR', 'clicks', 33986);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7830, '2018-03-08', 'CA', 'views', 66411);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7831, '2018-03-08', 'CA', 'plays', 18604);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7832, '2018-03-08', 'CA', 'clicks', 67040);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7845, '2018-03-08', 'CY', 'views', 59781);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7846, '2018-03-08', 'CY', 'plays', 54686);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7847, '2018-03-08', 'CY', 'clicks', 37273);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7857, '2018-03-08', 'DE', 'views', 48884);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7858, '2018-03-08', 'DE', 'plays', 28956);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7859, '2018-03-08', 'DE', 'clicks', 52979);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7848, '2018-03-08', 'EG', 'views', 48008);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7849, '2018-03-08', 'EG', 'plays', 46520);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7850, '2018-03-08', 'EG', 'clicks', 32525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7854, '2018-03-08', 'FR', 'views', 71641);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7855, '2018-03-08', 'FR', 'plays', 36638);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7856, '2018-03-08', 'FR', 'clicks', 45435);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7842, '2018-03-08', 'HR', 'views', 13296);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7843, '2018-03-08', 'HR', 'plays', 44639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7844, '2018-03-08', 'HR', 'clicks', 29105);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7851, '2018-03-08', 'RS', 'views', 37057);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7852, '2018-03-08', 'RS', 'plays', 20316);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7853, '2018-03-08', 'RS', 'clicks', 66927);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7863, '2018-03-09', 'AT', 'views', 60443);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7864, '2018-03-09', 'AT', 'plays', 59218);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7865, '2018-03-09', 'AT', 'clicks', 37403);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7860, '2018-03-09', 'AU', 'views', 37060);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7861, '2018-03-09', 'AU', 'plays', 38153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7862, '2018-03-09', 'AU', 'clicks', 48700);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7872, '2018-03-09', 'BA', 'views', 25490);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7873, '2018-03-09', 'BA', 'plays', 20794);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7874, '2018-03-09', 'BA', 'clicks', 69649);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7869, '2018-03-09', 'BE', 'views', 35053);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7870, '2018-03-09', 'BE', 'plays', 53113);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7871, '2018-03-09', 'BE', 'clicks', 58185);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7875, '2018-03-09', 'BR', 'views', 84886);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7876, '2018-03-09', 'BR', 'plays', 59592);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7877, '2018-03-09', 'BR', 'clicks', 26759);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7866, '2018-03-09', 'CA', 'views', 16304);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7867, '2018-03-09', 'CA', 'plays', 33151);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7868, '2018-03-09', 'CA', 'clicks', 39365);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7881, '2018-03-09', 'CY', 'views', 70809);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7882, '2018-03-09', 'CY', 'plays', 8414);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7883, '2018-03-09', 'CY', 'clicks', 10246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7893, '2018-03-09', 'DE', 'views', 63593);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7894, '2018-03-09', 'DE', 'plays', 47247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7895, '2018-03-09', 'DE', 'clicks', 42630);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7884, '2018-03-09', 'EG', 'views', 91626);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7885, '2018-03-09', 'EG', 'plays', 33216);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7886, '2018-03-09', 'EG', 'clicks', 50136);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7890, '2018-03-09', 'FR', 'views', 43525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7891, '2018-03-09', 'FR', 'plays', 66123);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7892, '2018-03-09', 'FR', 'clicks', 75394);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7878, '2018-03-09', 'HR', 'views', 71435);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7879, '2018-03-09', 'HR', 'plays', 6862);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7880, '2018-03-09', 'HR', 'clicks', 93435);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7887, '2018-03-09', 'RS', 'views', 40886);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7888, '2018-03-09', 'RS', 'plays', 38282);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7889, '2018-03-09', 'RS', 'clicks', 38037);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7899, '2018-03-10', 'AT', 'views', 36528);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7900, '2018-03-10', 'AT', 'plays', 35083);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7901, '2018-03-10', 'AT', 'clicks', 89581);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7896, '2018-03-10', 'AU', 'views', 35880);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7897, '2018-03-10', 'AU', 'plays', 32188);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7898, '2018-03-10', 'AU', 'clicks', 52702);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7908, '2018-03-10', 'BA', 'views', 15843);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7909, '2018-03-10', 'BA', 'plays', 49212);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7910, '2018-03-10', 'BA', 'clicks', 56507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7905, '2018-03-10', 'BE', 'views', 82281);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7906, '2018-03-10', 'BE', 'plays', 86441);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7907, '2018-03-10', 'BE', 'clicks', 44089);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7911, '2018-03-10', 'BR', 'views', 41220);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7912, '2018-03-10', 'BR', 'plays', 38902);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7913, '2018-03-10', 'BR', 'clicks', 21593);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7902, '2018-03-10', 'CA', 'views', 67973);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7903, '2018-03-10', 'CA', 'plays', 72489);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7904, '2018-03-10', 'CA', 'clicks', 48373);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7917, '2018-03-10', 'CY', 'views', 41117);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7918, '2018-03-10', 'CY', 'plays', 49674);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7919, '2018-03-10', 'CY', 'clicks', 37953);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7929, '2018-03-10', 'DE', 'views', 58285);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7930, '2018-03-10', 'DE', 'plays', 51952);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7931, '2018-03-10', 'DE', 'clicks', 34072);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7920, '2018-03-10', 'EG', 'views', 42291);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7921, '2018-03-10', 'EG', 'plays', 67707);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7922, '2018-03-10', 'EG', 'clicks', 57273);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7926, '2018-03-10', 'FR', 'views', 78368);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7927, '2018-03-10', 'FR', 'plays', 44546);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7928, '2018-03-10', 'FR', 'clicks', 40019);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7914, '2018-03-10', 'HR', 'views', 45942);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7915, '2018-03-10', 'HR', 'plays', 22459);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7916, '2018-03-10', 'HR', 'clicks', 12873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7923, '2018-03-10', 'RS', 'views', 58904);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7924, '2018-03-10', 'RS', 'plays', 46332);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7925, '2018-03-10', 'RS', 'clicks', 41828);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7935, '2018-03-11', 'AT', 'views', 76488);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7936, '2018-03-11', 'AT', 'plays', 67059);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7937, '2018-03-11', 'AT', 'clicks', 64230);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7932, '2018-03-11', 'AU', 'views', 75611);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7933, '2018-03-11', 'AU', 'plays', 58338);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7934, '2018-03-11', 'AU', 'clicks', 60093);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7944, '2018-03-11', 'BA', 'views', 68841);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7945, '2018-03-11', 'BA', 'plays', 36512);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7946, '2018-03-11', 'BA', 'clicks', 74965);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7941, '2018-03-11', 'BE', 'views', 64511);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7942, '2018-03-11', 'BE', 'plays', 75604);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7943, '2018-03-11', 'BE', 'clicks', 38233);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7947, '2018-03-11', 'BR', 'views', 93501);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7948, '2018-03-11', 'BR', 'plays', 43628);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7949, '2018-03-11', 'BR', 'clicks', 60054);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7938, '2018-03-11', 'CA', 'views', 29941);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7939, '2018-03-11', 'CA', 'plays', 31175);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7940, '2018-03-11', 'CA', 'clicks', 61283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7953, '2018-03-11', 'CY', 'views', 68729);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7954, '2018-03-11', 'CY', 'plays', 54952);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7955, '2018-03-11', 'CY', 'clicks', 33390);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7965, '2018-03-11', 'DE', 'views', 88151);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7966, '2018-03-11', 'DE', 'plays', 56096);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7967, '2018-03-11', 'DE', 'clicks', 40034);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7956, '2018-03-11', 'EG', 'views', 87560);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7957, '2018-03-11', 'EG', 'plays', 44379);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7958, '2018-03-11', 'EG', 'clicks', 57780);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7962, '2018-03-11', 'FR', 'views', 12436);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7963, '2018-03-11', 'FR', 'plays', 47665);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7964, '2018-03-11', 'FR', 'clicks', 31924);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7950, '2018-03-11', 'HR', 'views', 24752);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7951, '2018-03-11', 'HR', 'plays', 57183);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7952, '2018-03-11', 'HR', 'clicks', 54744);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7959, '2018-03-11', 'RS', 'views', 53425);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7960, '2018-03-11', 'RS', 'plays', 70406);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7961, '2018-03-11', 'RS', 'clicks', 47989);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7971, '2018-03-12', 'AT', 'views', 23263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7972, '2018-03-12', 'AT', 'plays', 45770);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7973, '2018-03-12', 'AT', 'clicks', 53728);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7968, '2018-03-12', 'AU', 'views', 46105);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7969, '2018-03-12', 'AU', 'plays', 59609);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7970, '2018-03-12', 'AU', 'clicks', 32263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7980, '2018-03-12', 'BA', 'views', 44452);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7981, '2018-03-12', 'BA', 'plays', 22757);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7982, '2018-03-12', 'BA', 'clicks', 50875);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7977, '2018-03-12', 'BE', 'views', 33094);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7978, '2018-03-12', 'BE', 'plays', 66789);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7979, '2018-03-12', 'BE', 'clicks', 34197);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7983, '2018-03-12', 'BR', 'views', 50355);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7984, '2018-03-12', 'BR', 'plays', 82634);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7985, '2018-03-12', 'BR', 'clicks', 14773);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7974, '2018-03-12', 'CA', 'views', 55998);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7975, '2018-03-12', 'CA', 'plays', 39467);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7976, '2018-03-12', 'CA', 'clicks', 84289);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7989, '2018-03-12', 'CY', 'views', 70946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7990, '2018-03-12', 'CY', 'plays', 57971);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7991, '2018-03-12', 'CY', 'clicks', 68716);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8001, '2018-03-12', 'DE', 'views', 76699);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8002, '2018-03-12', 'DE', 'plays', 29718);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8003, '2018-03-12', 'DE', 'clicks', 92103);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7992, '2018-03-12', 'EG', 'views', 55894);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7993, '2018-03-12', 'EG', 'plays', 93814);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7994, '2018-03-12', 'EG', 'clicks', 49350);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7998, '2018-03-12', 'FR', 'views', 56961);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7999, '2018-03-12', 'FR', 'plays', 70471);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8000, '2018-03-12', 'FR', 'clicks', 60537);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7986, '2018-03-12', 'HR', 'views', 78508);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7987, '2018-03-12', 'HR', 'plays', 60413);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7988, '2018-03-12', 'HR', 'clicks', 29533);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7995, '2018-03-12', 'RS', 'views', 69359);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7996, '2018-03-12', 'RS', 'plays', 53938);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(7997, '2018-03-12', 'RS', 'clicks', 22373);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8007, '2018-03-13', 'AT', 'views', 49638);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8008, '2018-03-13', 'AT', 'plays', 27696);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8009, '2018-03-13', 'AT', 'clicks', 42714);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8004, '2018-03-13', 'AU', 'views', 65251);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8005, '2018-03-13', 'AU', 'plays', 43976);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8006, '2018-03-13', 'AU', 'clicks', 49478);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8016, '2018-03-13', 'BA', 'views', 86291);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8017, '2018-03-13', 'BA', 'plays', 45235);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8018, '2018-03-13', 'BA', 'clicks', 51511);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8013, '2018-03-13', 'BE', 'views', 49230);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8014, '2018-03-13', 'BE', 'plays', 21294);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8015, '2018-03-13', 'BE', 'clicks', 31368);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8019, '2018-03-13', 'BR', 'views', 36083);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8020, '2018-03-13', 'BR', 'plays', 59506);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8021, '2018-03-13', 'BR', 'clicks', 37437);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8010, '2018-03-13', 'CA', 'views', 8108);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8011, '2018-03-13', 'CA', 'plays', 50873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8012, '2018-03-13', 'CA', 'clicks', 79515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8025, '2018-03-13', 'CY', 'views', 62564);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8026, '2018-03-13', 'CY', 'plays', 83133);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8027, '2018-03-13', 'CY', 'clicks', 48553);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8037, '2018-03-13', 'DE', 'views', 33400);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8038, '2018-03-13', 'DE', 'plays', 59232);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8039, '2018-03-13', 'DE', 'clicks', 60943);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8028, '2018-03-13', 'EG', 'views', 51704);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8029, '2018-03-13', 'EG', 'plays', 64719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8030, '2018-03-13', 'EG', 'clicks', 54147);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8034, '2018-03-13', 'FR', 'views', 44545);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8035, '2018-03-13', 'FR', 'plays', 25572);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8036, '2018-03-13', 'FR', 'clicks', 60540);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8022, '2018-03-13', 'HR', 'views', 81737);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8023, '2018-03-13', 'HR', 'plays', 66377);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8024, '2018-03-13', 'HR', 'clicks', 44762);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8031, '2018-03-13', 'RS', 'views', 14520);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8032, '2018-03-13', 'RS', 'plays', 47577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8033, '2018-03-13', 'RS', 'clicks', 74739);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8043, '2018-03-14', 'AT', 'views', 25985);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8044, '2018-03-14', 'AT', 'plays', 56043);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8045, '2018-03-14', 'AT', 'clicks', 50910);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8040, '2018-03-14', 'AU', 'views', 40109);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8041, '2018-03-14', 'AU', 'plays', 30067);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8042, '2018-03-14', 'AU', 'clicks', 54239);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8052, '2018-03-14', 'BA', 'views', 60980);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8053, '2018-03-14', 'BA', 'plays', 80726);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8054, '2018-03-14', 'BA', 'clicks', 9448);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8049, '2018-03-14', 'BE', 'views', 88943);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8050, '2018-03-14', 'BE', 'plays', 37416);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8051, '2018-03-14', 'BE', 'clicks', 30761);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8055, '2018-03-14', 'BR', 'views', 35530);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8056, '2018-03-14', 'BR', 'plays', 92482);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8057, '2018-03-14', 'BR', 'clicks', 31422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8046, '2018-03-14', 'CA', 'views', 46389);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8047, '2018-03-14', 'CA', 'plays', 98462);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8048, '2018-03-14', 'CA', 'clicks', 59720);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8061, '2018-03-14', 'CY', 'views', 77968);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8062, '2018-03-14', 'CY', 'plays', 72490);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8063, '2018-03-14', 'CY', 'clicks', 26903);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8073, '2018-03-14', 'DE', 'views', 17982);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8074, '2018-03-14', 'DE', 'plays', 25665);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8075, '2018-03-14', 'DE', 'clicks', 40025);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8064, '2018-03-14', 'EG', 'views', 65947);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8065, '2018-03-14', 'EG', 'plays', 63979);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8066, '2018-03-14', 'EG', 'clicks', 72477);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8070, '2018-03-14', 'FR', 'views', 54055);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8071, '2018-03-14', 'FR', 'plays', 47886);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8072, '2018-03-14', 'FR', 'clicks', 34426);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8058, '2018-03-14', 'HR', 'views', 44670);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8059, '2018-03-14', 'HR', 'plays', 38133);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8060, '2018-03-14', 'HR', 'clicks', 27215);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8067, '2018-03-14', 'RS', 'views', 54788);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8068, '2018-03-14', 'RS', 'plays', 48015);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8069, '2018-03-14', 'RS', 'clicks', 48977);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8079, '2018-03-15', 'AT', 'views', 26201);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8080, '2018-03-15', 'AT', 'plays', 69078);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8081, '2018-03-15', 'AT', 'clicks', 29692);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8076, '2018-03-15', 'AU', 'views', 21533);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8077, '2018-03-15', 'AU', 'plays', 45548);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8078, '2018-03-15', 'AU', 'clicks', 61044);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8088, '2018-03-15', 'BA', 'views', 40457);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8089, '2018-03-15', 'BA', 'plays', 22305);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8090, '2018-03-15', 'BA', 'clicks', 78799);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8085, '2018-03-15', 'BE', 'views', 73868);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8086, '2018-03-15', 'BE', 'plays', 60457);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8087, '2018-03-15', 'BE', 'clicks', 44722);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8091, '2018-03-15', 'BR', 'views', 26832);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8092, '2018-03-15', 'BR', 'plays', 23469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8093, '2018-03-15', 'BR', 'clicks', 92009);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8082, '2018-03-15', 'CA', 'views', 76379);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8083, '2018-03-15', 'CA', 'plays', 61584);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8084, '2018-03-15', 'CA', 'clicks', 31880);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8097, '2018-03-15', 'CY', 'views', 30861);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8098, '2018-03-15', 'CY', 'plays', 37433);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8099, '2018-03-15', 'CY', 'clicks', 66093);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8109, '2018-03-15', 'DE', 'views', 35785);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8110, '2018-03-15', 'DE', 'plays', 81069);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8111, '2018-03-15', 'DE', 'clicks', 51503);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8100, '2018-03-15', 'EG', 'views', 52253);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8101, '2018-03-15', 'EG', 'plays', 55888);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8102, '2018-03-15', 'EG', 'clicks', 55943);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8106, '2018-03-15', 'FR', 'views', 70830);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8107, '2018-03-15', 'FR', 'plays', 73613);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8108, '2018-03-15', 'FR', 'clicks', 7386);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8094, '2018-03-15', 'HR', 'views', 56001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8095, '2018-03-15', 'HR', 'plays', 71865);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8096, '2018-03-15', 'HR', 'clicks', 56440);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8103, '2018-03-15', 'RS', 'views', 14401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8104, '2018-03-15', 'RS', 'plays', 24753);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8105, '2018-03-15', 'RS', 'clicks', 34176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8115, '2018-03-16', 'AT', 'views', 27312);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8116, '2018-03-16', 'AT', 'plays', 35599);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8117, '2018-03-16', 'AT', 'clicks', 45202);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8112, '2018-03-16', 'AU', 'views', 58678);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8113, '2018-03-16', 'AU', 'plays', 61970);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8114, '2018-03-16', 'AU', 'clicks', 56700);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8124, '2018-03-16', 'BA', 'views', 43594);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8125, '2018-03-16', 'BA', 'plays', 91390);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8126, '2018-03-16', 'BA', 'clicks', 62639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8121, '2018-03-16', 'BE', 'views', 38753);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8122, '2018-03-16', 'BE', 'plays', 44032);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8123, '2018-03-16', 'BE', 'clicks', 56810);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8127, '2018-03-16', 'BR', 'views', 2307);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8128, '2018-03-16', 'BR', 'plays', 80939);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8129, '2018-03-16', 'BR', 'clicks', 72360);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8118, '2018-03-16', 'CA', 'views', 7412);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8119, '2018-03-16', 'CA', 'plays', 52451);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8120, '2018-03-16', 'CA', 'clicks', 40745);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8133, '2018-03-16', 'CY', 'views', 32762);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8134, '2018-03-16', 'CY', 'plays', 44160);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8135, '2018-03-16', 'CY', 'clicks', 1890);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8145, '2018-03-16', 'DE', 'views', 42838);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8146, '2018-03-16', 'DE', 'plays', 45167);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8147, '2018-03-16', 'DE', 'clicks', 56600);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8136, '2018-03-16', 'EG', 'views', 54970);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8137, '2018-03-16', 'EG', 'plays', 65183);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8138, '2018-03-16', 'EG', 'clicks', 52297);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8142, '2018-03-16', 'FR', 'views', 75160);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8143, '2018-03-16', 'FR', 'plays', 20122);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8144, '2018-03-16', 'FR', 'clicks', 4908);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8130, '2018-03-16', 'HR', 'views', 30212);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8131, '2018-03-16', 'HR', 'plays', 22460);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8132, '2018-03-16', 'HR', 'clicks', 22946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8139, '2018-03-16', 'RS', 'views', 60129);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8140, '2018-03-16', 'RS', 'plays', 38904);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8141, '2018-03-16', 'RS', 'clicks', 50125);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8151, '2018-03-17', 'AT', 'views', 53504);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8152, '2018-03-17', 'AT', 'plays', 34627);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8153, '2018-03-17', 'AT', 'clicks', 30385);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8148, '2018-03-17', 'AU', 'views', 67413);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8149, '2018-03-17', 'AU', 'plays', 52526);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8150, '2018-03-17', 'AU', 'clicks', 31514);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8160, '2018-03-17', 'BA', 'views', 42831);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8161, '2018-03-17', 'BA', 'plays', 46163);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8162, '2018-03-17', 'BA', 'clicks', 63529);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8157, '2018-03-17', 'BE', 'views', 29872);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8158, '2018-03-17', 'BE', 'plays', 30354);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8159, '2018-03-17', 'BE', 'clicks', 28246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8163, '2018-03-17', 'BR', 'views', 18973);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8164, '2018-03-17', 'BR', 'plays', 81182);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8165, '2018-03-17', 'BR', 'clicks', 75112);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8154, '2018-03-17', 'CA', 'views', 44370);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8155, '2018-03-17', 'CA', 'plays', 56949);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8156, '2018-03-17', 'CA', 'clicks', 94804);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8169, '2018-03-17', 'CY', 'views', 21379);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8170, '2018-03-17', 'CY', 'plays', 30756);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8171, '2018-03-17', 'CY', 'clicks', 33495);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8181, '2018-03-17', 'DE', 'views', 43847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8182, '2018-03-17', 'DE', 'plays', 51529);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8183, '2018-03-17', 'DE', 'clicks', 70847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8172, '2018-03-17', 'EG', 'views', 57249);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8173, '2018-03-17', 'EG', 'plays', 39340);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8174, '2018-03-17', 'EG', 'clicks', 62799);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8178, '2018-03-17', 'FR', 'views', 78335);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8179, '2018-03-17', 'FR', 'plays', 34880);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8180, '2018-03-17', 'FR', 'clicks', 12581);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8166, '2018-03-17', 'HR', 'views', 7436);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8167, '2018-03-17', 'HR', 'plays', 50643);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8168, '2018-03-17', 'HR', 'clicks', 29741);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8175, '2018-03-17', 'RS', 'views', 5929);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8176, '2018-03-17', 'RS', 'plays', 64201);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8177, '2018-03-17', 'RS', 'clicks', 25365);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8187, '2018-03-18', 'AT', 'views', 51288);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8188, '2018-03-18', 'AT', 'plays', 71545);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8189, '2018-03-18', 'AT', 'clicks', 39356);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8184, '2018-03-18', 'AU', 'views', 54916);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8185, '2018-03-18', 'AU', 'plays', 57056);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8186, '2018-03-18', 'AU', 'clicks', 63076);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8196, '2018-03-18', 'BA', 'views', 49787);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8197, '2018-03-18', 'BA', 'plays', 38274);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8198, '2018-03-18', 'BA', 'clicks', 57301);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8193, '2018-03-18', 'BE', 'views', 71166);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8194, '2018-03-18', 'BE', 'plays', 54981);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8195, '2018-03-18', 'BE', 'clicks', 59542);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8199, '2018-03-18', 'BR', 'views', 86620);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8200, '2018-03-18', 'BR', 'plays', 15695);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8201, '2018-03-18', 'BR', 'clicks', 22862);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8190, '2018-03-18', 'CA', 'views', 91910);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8191, '2018-03-18', 'CA', 'plays', 45514);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8192, '2018-03-18', 'CA', 'clicks', 44690);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8205, '2018-03-18', 'CY', 'views', 78021);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8206, '2018-03-18', 'CY', 'plays', 13559);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8207, '2018-03-18', 'CY', 'clicks', 48620);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8217, '2018-03-18', 'DE', 'views', 54582);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8218, '2018-03-18', 'DE', 'plays', 63077);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8219, '2018-03-18', 'DE', 'clicks', 48662);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8208, '2018-03-18', 'EG', 'views', 74627);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8209, '2018-03-18', 'EG', 'plays', 89486);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8210, '2018-03-18', 'EG', 'clicks', 64662);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8214, '2018-03-18', 'FR', 'views', 62377);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8215, '2018-03-18', 'FR', 'plays', 28566);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8216, '2018-03-18', 'FR', 'clicks', 58946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8202, '2018-03-18', 'HR', 'views', 44283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8203, '2018-03-18', 'HR', 'plays', 15059);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8204, '2018-03-18', 'HR', 'clicks', 69763);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8211, '2018-03-18', 'RS', 'views', 77116);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8212, '2018-03-18', 'RS', 'plays', 88913);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8213, '2018-03-18', 'RS', 'clicks', 64613);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8223, '2018-03-19', 'AT', 'views', 38158);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8224, '2018-03-19', 'AT', 'plays', 28586);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8225, '2018-03-19', 'AT', 'clicks', 56259);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8220, '2018-03-19', 'AU', 'views', 51799);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8221, '2018-03-19', 'AU', 'plays', 64941);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8222, '2018-03-19', 'AU', 'clicks', 81080);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8232, '2018-03-19', 'BA', 'views', 66487);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8233, '2018-03-19', 'BA', 'plays', 65768);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8234, '2018-03-19', 'BA', 'clicks', 56712);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8229, '2018-03-19', 'BE', 'views', 73254);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8230, '2018-03-19', 'BE', 'plays', 76656);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8231, '2018-03-19', 'BE', 'clicks', 24902);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8235, '2018-03-19', 'BR', 'views', 38977);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8236, '2018-03-19', 'BR', 'plays', 12513);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8237, '2018-03-19', 'BR', 'clicks', 83669);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8226, '2018-03-19', 'CA', 'views', 41712);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8227, '2018-03-19', 'CA', 'plays', 62836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8228, '2018-03-19', 'CA', 'clicks', 44497);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8241, '2018-03-19', 'CY', 'views', 30016);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8242, '2018-03-19', 'CY', 'plays', 40811);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8243, '2018-03-19', 'CY', 'clicks', 24229);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8253, '2018-03-19', 'DE', 'views', 52535);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8254, '2018-03-19', 'DE', 'plays', 74806);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8255, '2018-03-19', 'DE', 'clicks', 56016);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8244, '2018-03-19', 'EG', 'views', 42117);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8245, '2018-03-19', 'EG', 'plays', 58738);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8246, '2018-03-19', 'EG', 'clicks', 41626);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8250, '2018-03-19', 'FR', 'views', 36477);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8251, '2018-03-19', 'FR', 'plays', 61354);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8252, '2018-03-19', 'FR', 'clicks', 52838);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8238, '2018-03-19', 'HR', 'views', 75357);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8239, '2018-03-19', 'HR', 'plays', 28401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8240, '2018-03-19', 'HR', 'clicks', 84548);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8247, '2018-03-19', 'RS', 'views', 60363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8248, '2018-03-19', 'RS', 'plays', 25644);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8249, '2018-03-19', 'RS', 'clicks', 82054);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8259, '2018-03-20', 'AT', 'views', 39233);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8260, '2018-03-20', 'AT', 'plays', 36632);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8261, '2018-03-20', 'AT', 'clicks', 31126);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8256, '2018-03-20', 'AU', 'views', 91598);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8257, '2018-03-20', 'AU', 'plays', 40513);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8258, '2018-03-20', 'AU', 'clicks', 72134);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8268, '2018-03-20', 'BA', 'views', 65045);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8269, '2018-03-20', 'BA', 'plays', 59696);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8270, '2018-03-20', 'BA', 'clicks', 65555);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8265, '2018-03-20', 'BE', 'views', 50494);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8266, '2018-03-20', 'BE', 'plays', 27851);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8267, '2018-03-20', 'BE', 'clicks', 32546);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8271, '2018-03-20', 'BR', 'views', 22142);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8272, '2018-03-20', 'BR', 'plays', 62572);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8273, '2018-03-20', 'BR', 'clicks', 45524);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8262, '2018-03-20', 'CA', 'views', 42578);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8263, '2018-03-20', 'CA', 'plays', 82949);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8264, '2018-03-20', 'CA', 'clicks', 38738);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8277, '2018-03-20', 'CY', 'views', 68144);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8278, '2018-03-20', 'CY', 'plays', 68136);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8279, '2018-03-20', 'CY', 'clicks', 35074);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8289, '2018-03-20', 'DE', 'views', 78747);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8290, '2018-03-20', 'DE', 'plays', 55587);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8291, '2018-03-20', 'DE', 'clicks', 52153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8280, '2018-03-20', 'EG', 'views', 57936);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8281, '2018-03-20', 'EG', 'plays', 16614);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8282, '2018-03-20', 'EG', 'clicks', 61381);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8286, '2018-03-20', 'FR', 'views', 42282);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8287, '2018-03-20', 'FR', 'plays', 71465);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8288, '2018-03-20', 'FR', 'clicks', 38080);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8274, '2018-03-20', 'HR', 'views', 29173);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8275, '2018-03-20', 'HR', 'plays', 70311);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8276, '2018-03-20', 'HR', 'clicks', 18097);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8283, '2018-03-20', 'RS', 'views', 18016);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8284, '2018-03-20', 'RS', 'plays', 49925);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8285, '2018-03-20', 'RS', 'clicks', 36598);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8295, '2018-03-21', 'AT', 'views', 43210);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8296, '2018-03-21', 'AT', 'plays', 70972);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8297, '2018-03-21', 'AT', 'clicks', 36361);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8292, '2018-03-21', 'AU', 'views', 69442);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8293, '2018-03-21', 'AU', 'plays', 41567);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8294, '2018-03-21', 'AU', 'clicks', 58716);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8304, '2018-03-21', 'BA', 'views', 39398);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8305, '2018-03-21', 'BA', 'plays', 53032);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8306, '2018-03-21', 'BA', 'clicks', 49934);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8301, '2018-03-21', 'BE', 'views', 58290);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8302, '2018-03-21', 'BE', 'plays', 67592);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8303, '2018-03-21', 'BE', 'clicks', 90295);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8307, '2018-03-21', 'BR', 'views', 34348);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8308, '2018-03-21', 'BR', 'plays', 18968);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8309, '2018-03-21', 'BR', 'clicks', 35660);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8298, '2018-03-21', 'CA', 'views', 43088);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8299, '2018-03-21', 'CA', 'plays', 88570);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8300, '2018-03-21', 'CA', 'clicks', 43728);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8313, '2018-03-21', 'CY', 'views', 26242);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8314, '2018-03-21', 'CY', 'plays', 82969);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8315, '2018-03-21', 'CY', 'clicks', 40790);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8325, '2018-03-21', 'DE', 'views', 73707);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8326, '2018-03-21', 'DE', 'plays', 32129);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8327, '2018-03-21', 'DE', 'clicks', 24881);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8316, '2018-03-21', 'EG', 'views', 50102);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8317, '2018-03-21', 'EG', 'plays', 75416);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8318, '2018-03-21', 'EG', 'clicks', 62053);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8322, '2018-03-21', 'FR', 'views', 61980);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8323, '2018-03-21', 'FR', 'plays', 50230);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8324, '2018-03-21', 'FR', 'clicks', 16432);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8310, '2018-03-21', 'HR', 'views', 38311);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8311, '2018-03-21', 'HR', 'plays', 81414);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8312, '2018-03-21', 'HR', 'clicks', 74099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8319, '2018-03-21', 'RS', 'views', 49302);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8320, '2018-03-21', 'RS', 'plays', 34422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8321, '2018-03-21', 'RS', 'clicks', 53888);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8331, '2018-03-22', 'AT', 'views', 80298);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8332, '2018-03-22', 'AT', 'plays', 45240);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8333, '2018-03-22', 'AT', 'clicks', 61015);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8328, '2018-03-22', 'AU', 'views', 99343);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8329, '2018-03-22', 'AU', 'plays', 52799);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8330, '2018-03-22', 'AU', 'clicks', 46378);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8340, '2018-03-22', 'BA', 'views', 17731);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8341, '2018-03-22', 'BA', 'plays', 58506);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8342, '2018-03-22', 'BA', 'clicks', 21378);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8337, '2018-03-22', 'BE', 'views', 76770);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8338, '2018-03-22', 'BE', 'plays', 47440);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8339, '2018-03-22', 'BE', 'clicks', 34236);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8343, '2018-03-22', 'BR', 'views', 40347);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8344, '2018-03-22', 'BR', 'plays', 55115);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8345, '2018-03-22', 'BR', 'clicks', 9552);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8334, '2018-03-22', 'CA', 'views', 30550);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8335, '2018-03-22', 'CA', 'plays', 73922);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8336, '2018-03-22', 'CA', 'clicks', 58983);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8349, '2018-03-22', 'CY', 'views', 63498);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8350, '2018-03-22', 'CY', 'plays', 64179);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8351, '2018-03-22', 'CY', 'clicks', 18838);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8361, '2018-03-22', 'DE', 'views', 24493);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8362, '2018-03-22', 'DE', 'plays', 57365);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8363, '2018-03-22', 'DE', 'clicks', 46930);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8352, '2018-03-22', 'EG', 'views', 60391);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8353, '2018-03-22', 'EG', 'plays', 37589);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8354, '2018-03-22', 'EG', 'clicks', 70920);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8358, '2018-03-22', 'FR', 'views', 23855);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8359, '2018-03-22', 'FR', 'plays', 55524);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8360, '2018-03-22', 'FR', 'clicks', 28748);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8346, '2018-03-22', 'HR', 'views', 80811);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8347, '2018-03-22', 'HR', 'plays', 91777);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8348, '2018-03-22', 'HR', 'clicks', 58715);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8355, '2018-03-22', 'RS', 'views', 48842);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8356, '2018-03-22', 'RS', 'plays', 60878);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8357, '2018-03-22', 'RS', 'clicks', 4388);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8367, '2018-03-23', 'AT', 'views', 83931);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8368, '2018-03-23', 'AT', 'plays', 65569);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8369, '2018-03-23', 'AT', 'clicks', 73339);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8364, '2018-03-23', 'AU', 'views', 53028);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8365, '2018-03-23', 'AU', 'plays', 10498);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8366, '2018-03-23', 'AU', 'clicks', 59794);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8376, '2018-03-23', 'BA', 'views', 49890);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8377, '2018-03-23', 'BA', 'plays', 37767);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8378, '2018-03-23', 'BA', 'clicks', 62263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8373, '2018-03-23', 'BE', 'views', 28106);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8374, '2018-03-23', 'BE', 'plays', 38594);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8375, '2018-03-23', 'BE', 'clicks', 96513);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8379, '2018-03-23', 'BR', 'views', 64887);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8380, '2018-03-23', 'BR', 'plays', 59989);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8381, '2018-03-23', 'BR', 'clicks', 46753);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8370, '2018-03-23', 'CA', 'views', 33302);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8371, '2018-03-23', 'CA', 'plays', 72460);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8372, '2018-03-23', 'CA', 'clicks', 29511);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8385, '2018-03-23', 'CY', 'views', 65422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8386, '2018-03-23', 'CY', 'plays', 63646);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8387, '2018-03-23', 'CY', 'clicks', 25929);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8397, '2018-03-23', 'DE', 'views', 19784);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8398, '2018-03-23', 'DE', 'plays', 16983);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8399, '2018-03-23', 'DE', 'clicks', 72998);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8388, '2018-03-23', 'EG', 'views', 60447);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8389, '2018-03-23', 'EG', 'plays', 80833);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8390, '2018-03-23', 'EG', 'clicks', 68975);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8394, '2018-03-23', 'FR', 'views', 24383);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8395, '2018-03-23', 'FR', 'plays', 51940);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8396, '2018-03-23', 'FR', 'clicks', 49434);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8382, '2018-03-23', 'HR', 'views', 33395);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8383, '2018-03-23', 'HR', 'plays', 46520);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8384, '2018-03-23', 'HR', 'clicks', 60375);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8391, '2018-03-23', 'RS', 'views', 71405);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8392, '2018-03-23', 'RS', 'plays', 58833);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8393, '2018-03-23', 'RS', 'clicks', 79581);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8403, '2018-03-24', 'AT', 'views', 31001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8404, '2018-03-24', 'AT', 'plays', 30984);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8405, '2018-03-24', 'AT', 'clicks', 65337);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8400, '2018-03-24', 'AU', 'views', 52963);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8401, '2018-03-24', 'AU', 'plays', 32758);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8402, '2018-03-24', 'AU', 'clicks', 38484);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8412, '2018-03-24', 'BA', 'views', 38174);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8413, '2018-03-24', 'BA', 'plays', 73595);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8414, '2018-03-24', 'BA', 'clicks', 57370);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8409, '2018-03-24', 'BE', 'views', 65484);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8410, '2018-03-24', 'BE', 'plays', 66685);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8411, '2018-03-24', 'BE', 'clicks', 46813);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8415, '2018-03-24', 'BR', 'views', 29084);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8416, '2018-03-24', 'BR', 'plays', 38123);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8417, '2018-03-24', 'BR', 'clicks', 31659);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8406, '2018-03-24', 'CA', 'views', 60067);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8407, '2018-03-24', 'CA', 'plays', 43452);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8408, '2018-03-24', 'CA', 'clicks', 22975);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8421, '2018-03-24', 'CY', 'views', 91662);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8422, '2018-03-24', 'CY', 'plays', 21982);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8423, '2018-03-24', 'CY', 'clicks', 51268);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8433, '2018-03-24', 'DE', 'views', 57492);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8434, '2018-03-24', 'DE', 'plays', 18375);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8435, '2018-03-24', 'DE', 'clicks', 62649);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8424, '2018-03-24', 'EG', 'views', 76029);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8425, '2018-03-24', 'EG', 'plays', 64129);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8426, '2018-03-24', 'EG', 'clicks', 88507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8430, '2018-03-24', 'FR', 'views', 81796);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8431, '2018-03-24', 'FR', 'plays', 48099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8432, '2018-03-24', 'FR', 'clicks', 92953);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8418, '2018-03-24', 'HR', 'views', 89083);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8419, '2018-03-24', 'HR', 'plays', 50649);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8420, '2018-03-24', 'HR', 'clicks', 61582);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8427, '2018-03-24', 'RS', 'views', 53513);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8428, '2018-03-24', 'RS', 'plays', 50746);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8429, '2018-03-24', 'RS', 'clicks', 34841);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8439, '2018-03-25', 'AT', 'views', 92590);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8440, '2018-03-25', 'AT', 'plays', 34968);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8441, '2018-03-25', 'AT', 'clicks', 36508);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8436, '2018-03-25', 'AU', 'views', 60314);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8437, '2018-03-25', 'AU', 'plays', 29797);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8438, '2018-03-25', 'AU', 'clicks', 36040);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8448, '2018-03-25', 'BA', 'views', 14707);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8449, '2018-03-25', 'BA', 'plays', 54613);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8450, '2018-03-25', 'BA', 'clicks', 50571);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8445, '2018-03-25', 'BE', 'views', 74124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8446, '2018-03-25', 'BE', 'plays', 63910);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8447, '2018-03-25', 'BE', 'clicks', 47574);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8451, '2018-03-25', 'BR', 'views', 14482);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8452, '2018-03-25', 'BR', 'plays', 31762);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8453, '2018-03-25', 'BR', 'clicks', 13667);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8442, '2018-03-25', 'CA', 'views', 35138);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8443, '2018-03-25', 'CA', 'plays', 68062);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8444, '2018-03-25', 'CA', 'clicks', 23695);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8457, '2018-03-25', 'CY', 'views', 27424);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8458, '2018-03-25', 'CY', 'plays', 24203);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8459, '2018-03-25', 'CY', 'clicks', 26241);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8469, '2018-03-25', 'DE', 'views', 73654);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8470, '2018-03-25', 'DE', 'plays', 46255);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8471, '2018-03-25', 'DE', 'clicks', 45979);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8460, '2018-03-25', 'EG', 'views', 37611);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8461, '2018-03-25', 'EG', 'plays', 29346);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8462, '2018-03-25', 'EG', 'clicks', 56906);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8466, '2018-03-25', 'FR', 'views', 7629);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8467, '2018-03-25', 'FR', 'plays', 73282);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8468, '2018-03-25', 'FR', 'clicks', 39750);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8454, '2018-03-25', 'HR', 'views', 70277);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8455, '2018-03-25', 'HR', 'plays', 28884);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8456, '2018-03-25', 'HR', 'clicks', 76976);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8463, '2018-03-25', 'RS', 'views', 48853);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8464, '2018-03-25', 'RS', 'plays', 54422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8465, '2018-03-25', 'RS', 'clicks', 43715);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8475, '2018-03-26', 'AT', 'views', 29708);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8476, '2018-03-26', 'AT', 'plays', 93209);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8477, '2018-03-26', 'AT', 'clicks', 38431);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8472, '2018-03-26', 'AU', 'views', 44968);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8473, '2018-03-26', 'AU', 'plays', 36422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8474, '2018-03-26', 'AU', 'clicks', 32235);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8484, '2018-03-26', 'BA', 'views', 59629);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8485, '2018-03-26', 'BA', 'plays', 40927);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8486, '2018-03-26', 'BA', 'clicks', 85433);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8481, '2018-03-26', 'BE', 'views', 26834);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8482, '2018-03-26', 'BE', 'plays', 21204);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8483, '2018-03-26', 'BE', 'clicks', 59979);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8487, '2018-03-26', 'BR', 'views', 51569);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8488, '2018-03-26', 'BR', 'plays', 44319);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8489, '2018-03-26', 'BR', 'clicks', 83020);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8478, '2018-03-26', 'CA', 'views', 46558);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8479, '2018-03-26', 'CA', 'plays', 54321);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8480, '2018-03-26', 'CA', 'clicks', 43096);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8493, '2018-03-26', 'CY', 'views', 42975);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8494, '2018-03-26', 'CY', 'plays', 54257);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8495, '2018-03-26', 'CY', 'clicks', 20546);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8505, '2018-03-26', 'DE', 'views', 52948);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8506, '2018-03-26', 'DE', 'plays', 16217);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8507, '2018-03-26', 'DE', 'clicks', 33133);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8496, '2018-03-26', 'EG', 'views', 85899);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8497, '2018-03-26', 'EG', 'plays', 24358);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8498, '2018-03-26', 'EG', 'clicks', 40977);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8502, '2018-03-26', 'FR', 'views', 47741);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8503, '2018-03-26', 'FR', 'plays', 64333);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8504, '2018-03-26', 'FR', 'clicks', 55342);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8490, '2018-03-26', 'HR', 'views', 85446);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8491, '2018-03-26', 'HR', 'plays', 43479);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8492, '2018-03-26', 'HR', 'clicks', 41126);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8499, '2018-03-26', 'RS', 'views', 15721);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8500, '2018-03-26', 'RS', 'plays', 28852);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8501, '2018-03-26', 'RS', 'clicks', 53316);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8511, '2018-03-27', 'AT', 'views', 28083);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8512, '2018-03-27', 'AT', 'plays', 53216);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8513, '2018-03-27', 'AT', 'clicks', 46438);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8508, '2018-03-27', 'AU', 'views', 70833);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8509, '2018-03-27', 'AU', 'plays', 22983);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8510, '2018-03-27', 'AU', 'clicks', 32853);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8520, '2018-03-27', 'BA', 'views', 50473);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8521, '2018-03-27', 'BA', 'plays', 62898);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8522, '2018-03-27', 'BA', 'clicks', 47102);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8517, '2018-03-27', 'BE', 'views', 58542);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8518, '2018-03-27', 'BE', 'plays', 44708);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8519, '2018-03-27', 'BE', 'clicks', 54077);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8523, '2018-03-27', 'BR', 'views', 45252);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8524, '2018-03-27', 'BR', 'plays', 52352);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8525, '2018-03-27', 'BR', 'clicks', 67647);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8514, '2018-03-27', 'CA', 'views', 81176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8515, '2018-03-27', 'CA', 'plays', 88341);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8516, '2018-03-27', 'CA', 'clicks', 29627);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8529, '2018-03-27', 'CY', 'views', 32247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8530, '2018-03-27', 'CY', 'plays', 47713);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8531, '2018-03-27', 'CY', 'clicks', 61108);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8541, '2018-03-27', 'DE', 'views', 51396);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8542, '2018-03-27', 'DE', 'plays', 87044);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8543, '2018-03-27', 'DE', 'clicks', 3618);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8532, '2018-03-27', 'EG', 'views', 46655);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8533, '2018-03-27', 'EG', 'plays', 57170);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8534, '2018-03-27', 'EG', 'clicks', 47323);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8538, '2018-03-27', 'FR', 'views', 38541);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8539, '2018-03-27', 'FR', 'plays', 61123);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8540, '2018-03-27', 'FR', 'clicks', 63007);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8526, '2018-03-27', 'HR', 'views', 32689);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8527, '2018-03-27', 'HR', 'plays', 25727);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8528, '2018-03-27', 'HR', 'clicks', 11185);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8535, '2018-03-27', 'RS', 'views', 94020);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8536, '2018-03-27', 'RS', 'plays', 60591);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8537, '2018-03-27', 'RS', 'clicks', 63825);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8547, '2018-03-28', 'AT', 'views', 59207);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8548, '2018-03-28', 'AT', 'plays', 34390);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8549, '2018-03-28', 'AT', 'clicks', 29458);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8544, '2018-03-28', 'AU', 'views', 58833);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8545, '2018-03-28', 'AU', 'plays', 70749);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8546, '2018-03-28', 'AU', 'clicks', 68780);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8556, '2018-03-28', 'BA', 'views', 61847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8557, '2018-03-28', 'BA', 'plays', 51030);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8558, '2018-03-28', 'BA', 'clicks', 49986);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8553, '2018-03-28', 'BE', 'views', 83038);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8554, '2018-03-28', 'BE', 'plays', 72777);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8555, '2018-03-28', 'BE', 'clicks', 34701);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8559, '2018-03-28', 'BR', 'views', 25430);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8560, '2018-03-28', 'BR', 'plays', 66083);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8561, '2018-03-28', 'BR', 'clicks', 39375);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8550, '2018-03-28', 'CA', 'views', 16525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8551, '2018-03-28', 'CA', 'plays', 70997);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8552, '2018-03-28', 'CA', 'clicks', 33748);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8565, '2018-03-28', 'CY', 'views', 46848);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8566, '2018-03-28', 'CY', 'plays', 55950);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8567, '2018-03-28', 'CY', 'clicks', 53746);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8577, '2018-03-28', 'DE', 'views', 40199);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8578, '2018-03-28', 'DE', 'plays', 52099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8579, '2018-03-28', 'DE', 'clicks', 84479);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8568, '2018-03-28', 'EG', 'views', 45390);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8569, '2018-03-28', 'EG', 'plays', 70429);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8570, '2018-03-28', 'EG', 'clicks', 37614);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8574, '2018-03-28', 'FR', 'views', 28123);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8575, '2018-03-28', 'FR', 'plays', 32508);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8576, '2018-03-28', 'FR', 'clicks', 50575);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8562, '2018-03-28', 'HR', 'views', 61361);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8563, '2018-03-28', 'HR', 'plays', 41873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8564, '2018-03-28', 'HR', 'clicks', 49261);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8571, '2018-03-28', 'RS', 'views', 46951);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8572, '2018-03-28', 'RS', 'plays', 70388);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8573, '2018-03-28', 'RS', 'clicks', 50926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8583, '2018-03-29', 'AT', 'views', 69304);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8584, '2018-03-29', 'AT', 'plays', 53507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8585, '2018-03-29', 'AT', 'clicks', 79001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8580, '2018-03-29', 'AU', 'views', 72400);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8581, '2018-03-29', 'AU', 'plays', 41688);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8582, '2018-03-29', 'AU', 'clicks', 22280);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8592, '2018-03-29', 'BA', 'views', 49417);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8593, '2018-03-29', 'BA', 'plays', 36649);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8594, '2018-03-29', 'BA', 'clicks', 33832);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8589, '2018-03-29', 'BE', 'views', 68085);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8590, '2018-03-29', 'BE', 'plays', 63507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8591, '2018-03-29', 'BE', 'clicks', 54242);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8595, '2018-03-29', 'BR', 'views', 53295);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8596, '2018-03-29', 'BR', 'plays', 18098);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8597, '2018-03-29', 'BR', 'clicks', 61388);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8586, '2018-03-29', 'CA', 'views', 74514);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8587, '2018-03-29', 'CA', 'plays', 16774);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8588, '2018-03-29', 'CA', 'clicks', 15483);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8601, '2018-03-29', 'CY', 'views', 94844);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8602, '2018-03-29', 'CY', 'plays', 65470);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8603, '2018-03-29', 'CY', 'clicks', 61687);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8613, '2018-03-29', 'DE', 'views', 48208);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8614, '2018-03-29', 'DE', 'plays', 32285);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8615, '2018-03-29', 'DE', 'clicks', 57260);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8604, '2018-03-29', 'EG', 'views', 64205);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8605, '2018-03-29', 'EG', 'plays', 71418);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8606, '2018-03-29', 'EG', 'clicks', 92960);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8610, '2018-03-29', 'FR', 'views', 19201);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8611, '2018-03-29', 'FR', 'plays', 66522);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8612, '2018-03-29', 'FR', 'clicks', 45474);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8598, '2018-03-29', 'HR', 'views', 17230);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8599, '2018-03-29', 'HR', 'plays', 57099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8600, '2018-03-29', 'HR', 'clicks', 70607);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8607, '2018-03-29', 'RS', 'views', 85390);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8608, '2018-03-29', 'RS', 'plays', 71793);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8609, '2018-03-29', 'RS', 'clicks', 20950);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8619, '2018-03-30', 'AT', 'views', 52599);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8620, '2018-03-30', 'AT', 'plays', 14809);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8621, '2018-03-30', 'AT', 'clicks', 45658);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8616, '2018-03-30', 'AU', 'views', 48425);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8617, '2018-03-30', 'AU', 'plays', 38650);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8618, '2018-03-30', 'AU', 'clicks', 67942);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8628, '2018-03-30', 'BA', 'views', 75728);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8629, '2018-03-30', 'BA', 'plays', 75107);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8630, '2018-03-30', 'BA', 'clicks', 60015);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8625, '2018-03-30', 'BE', 'views', 12428);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8626, '2018-03-30', 'BE', 'plays', 58128);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8627, '2018-03-30', 'BE', 'clicks', 38734);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8631, '2018-03-30', 'BR', 'views', 49211);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8632, '2018-03-30', 'BR', 'plays', 47224);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8633, '2018-03-30', 'BR', 'clicks', 24532);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8622, '2018-03-30', 'CA', 'views', 15889);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8623, '2018-03-30', 'CA', 'plays', 73699);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8624, '2018-03-30', 'CA', 'clicks', 45549);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8637, '2018-03-30', 'CY', 'views', 38760);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8638, '2018-03-30', 'CY', 'plays', 73443);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8639, '2018-03-30', 'CY', 'clicks', 14261);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8649, '2018-03-30', 'DE', 'views', 53283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8650, '2018-03-30', 'DE', 'plays', 64797);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8651, '2018-03-30', 'DE', 'clicks', 47935);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8640, '2018-03-30', 'EG', 'views', 69688);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8641, '2018-03-30', 'EG', 'plays', 18320);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8642, '2018-03-30', 'EG', 'clicks', 39302);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8646, '2018-03-30', 'FR', 'views', 66327);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8647, '2018-03-30', 'FR', 'plays', 39393);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8648, '2018-03-30', 'FR', 'clicks', 44674);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8634, '2018-03-30', 'HR', 'views', 47295);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8635, '2018-03-30', 'HR', 'plays', 22637);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8636, '2018-03-30', 'HR', 'clicks', 47293);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8643, '2018-03-30', 'RS', 'views', 43529);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8644, '2018-03-30', 'RS', 'plays', 14656);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8645, '2018-03-30', 'RS', 'clicks', 27325);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8655, '2018-03-31', 'AT', 'views', 51997);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8656, '2018-03-31', 'AT', 'plays', 48617);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8657, '2018-03-31', 'AT', 'clicks', 91042);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8652, '2018-03-31', 'AU', 'views', 63193);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8653, '2018-03-31', 'AU', 'plays', 57850);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8654, '2018-03-31', 'AU', 'clicks', 90284);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8664, '2018-03-31', 'BA', 'views', 64239);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8665, '2018-03-31', 'BA', 'plays', 35843);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8666, '2018-03-31', 'BA', 'clicks', 77656);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8661, '2018-03-31', 'BE', 'views', 79528);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8662, '2018-03-31', 'BE', 'plays', 48891);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8663, '2018-03-31', 'BE', 'clicks', 50157);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8667, '2018-03-31', 'BR', 'views', 39210);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8668, '2018-03-31', 'BR', 'plays', 40926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8669, '2018-03-31', 'BR', 'clicks', 64221);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8658, '2018-03-31', 'CA', 'views', 19194);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8659, '2018-03-31', 'CA', 'plays', 30579);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8660, '2018-03-31', 'CA', 'clicks', 39034);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8673, '2018-03-31', 'CY', 'views', 45835);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8674, '2018-03-31', 'CY', 'plays', 26385);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8675, '2018-03-31', 'CY', 'clicks', 71133);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8685, '2018-03-31', 'DE', 'views', 18262);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8686, '2018-03-31', 'DE', 'plays', 76445);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8687, '2018-03-31', 'DE', 'clicks', 22523);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8676, '2018-03-31', 'EG', 'views', 58293);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8677, '2018-03-31', 'EG', 'plays', 50962);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8678, '2018-03-31', 'EG', 'clicks', 72181);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8682, '2018-03-31', 'FR', 'views', 53698);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8683, '2018-03-31', 'FR', 'plays', 32425);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8684, '2018-03-31', 'FR', 'clicks', 20781);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8670, '2018-03-31', 'HR', 'views', 31550);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8671, '2018-03-31', 'HR', 'plays', 32385);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8672, '2018-03-31', 'HR', 'clicks', 50074);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8679, '2018-03-31', 'RS', 'views', 37599);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8680, '2018-03-31', 'RS', 'plays', 33246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8681, '2018-03-31', 'RS', 'clicks', 61385);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8691, '2018-04-01', 'AT', 'views', 76623);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8692, '2018-04-01', 'AT', 'plays', 90401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8693, '2018-04-01', 'AT', 'clicks', 42341);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8688, '2018-04-01', 'AU', 'views', 70097);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8689, '2018-04-01', 'AU', 'plays', 91367);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8690, '2018-04-01', 'AU', 'clicks', 60758);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8700, '2018-04-01', 'BA', 'views', 50250);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8701, '2018-04-01', 'BA', 'plays', 73870);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8702, '2018-04-01', 'BA', 'clicks', 80019);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8697, '2018-04-01', 'BE', 'views', 58993);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8698, '2018-04-01', 'BE', 'plays', 64206);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8699, '2018-04-01', 'BE', 'clicks', 26075);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8703, '2018-04-01', 'BR', 'views', 10358);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8704, '2018-04-01', 'BR', 'plays', 55226);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8705, '2018-04-01', 'BR', 'clicks', 54736);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8694, '2018-04-01', 'CA', 'views', 58999);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8695, '2018-04-01', 'CA', 'plays', 50274);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8696, '2018-04-01', 'CA', 'clicks', 40740);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8709, '2018-04-01', 'CY', 'views', 23755);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8710, '2018-04-01', 'CY', 'plays', 30382);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8711, '2018-04-01', 'CY', 'clicks', 31001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8721, '2018-04-01', 'DE', 'views', 64497);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8722, '2018-04-01', 'DE', 'plays', 55085);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8723, '2018-04-01', 'DE', 'clicks', 63611);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8712, '2018-04-01', 'EG', 'views', 69416);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8713, '2018-04-01', 'EG', 'plays', 44218);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8714, '2018-04-01', 'EG', 'clicks', 35544);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8718, '2018-04-01', 'FR', 'views', 64177);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8719, '2018-04-01', 'FR', 'plays', 22856);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8720, '2018-04-01', 'FR', 'clicks', 56986);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8706, '2018-04-01', 'HR', 'views', 76807);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8707, '2018-04-01', 'HR', 'plays', 13023);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8708, '2018-04-01', 'HR', 'clicks', 60354);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8715, '2018-04-01', 'RS', 'views', 39804);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8716, '2018-04-01', 'RS', 'plays', 42650);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8717, '2018-04-01', 'RS', 'clicks', 70432);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8727, '2018-04-02', 'AT', 'views', 9612);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8728, '2018-04-02', 'AT', 'plays', 19654);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8729, '2018-04-02', 'AT', 'clicks', 60781);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8724, '2018-04-02', 'AU', 'views', 59147);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8725, '2018-04-02', 'AU', 'plays', 67783);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8726, '2018-04-02', 'AU', 'clicks', 68739);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8736, '2018-04-02', 'BA', 'views', 73659);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8737, '2018-04-02', 'BA', 'plays', 55255);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8738, '2018-04-02', 'BA', 'clicks', 76029);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8733, '2018-04-02', 'BE', 'views', 76605);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8734, '2018-04-02', 'BE', 'plays', 85340);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8735, '2018-04-02', 'BE', 'clicks', 55911);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8739, '2018-04-02', 'BR', 'views', 53704);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8740, '2018-04-02', 'BR', 'plays', 30306);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8741, '2018-04-02', 'BR', 'clicks', 43924);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8730, '2018-04-02', 'CA', 'views', 26280);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8731, '2018-04-02', 'CA', 'plays', 58567);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8732, '2018-04-02', 'CA', 'clicks', 56056);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8745, '2018-04-02', 'CY', 'views', 51374);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8746, '2018-04-02', 'CY', 'plays', 21720);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8747, '2018-04-02', 'CY', 'clicks', 33686);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8757, '2018-04-02', 'DE', 'views', 32168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8758, '2018-04-02', 'DE', 'plays', 25692);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8759, '2018-04-02', 'DE', 'clicks', 59714);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8748, '2018-04-02', 'EG', 'views', 30853);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8749, '2018-04-02', 'EG', 'plays', 34499);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8750, '2018-04-02', 'EG', 'clicks', 94530);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8754, '2018-04-02', 'FR', 'views', 54354);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8755, '2018-04-02', 'FR', 'plays', 85615);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8756, '2018-04-02', 'FR', 'clicks', 8438);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8742, '2018-04-02', 'HR', 'views', 59682);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8743, '2018-04-02', 'HR', 'plays', 47949);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8744, '2018-04-02', 'HR', 'clicks', 53362);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8751, '2018-04-02', 'RS', 'views', 70904);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8752, '2018-04-02', 'RS', 'plays', 45897);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8753, '2018-04-02', 'RS', 'clicks', 79333);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8763, '2018-04-03', 'AT', 'views', 68034);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8764, '2018-04-03', 'AT', 'plays', 34610);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8765, '2018-04-03', 'AT', 'clicks', 34973);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8760, '2018-04-03', 'AU', 'views', 44044);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8761, '2018-04-03', 'AU', 'plays', 47837);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8762, '2018-04-03', 'AU', 'clicks', 77377);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8772, '2018-04-03', 'BA', 'views', 80639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8773, '2018-04-03', 'BA', 'plays', 85076);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8774, '2018-04-03', 'BA', 'clicks', 45755);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8769, '2018-04-03', 'BE', 'views', 73411);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8770, '2018-04-03', 'BE', 'plays', 55413);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8771, '2018-04-03', 'BE', 'clicks', 31602);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8775, '2018-04-03', 'BR', 'views', 62442);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8776, '2018-04-03', 'BR', 'plays', 51920);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8777, '2018-04-03', 'BR', 'clicks', 33663);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8766, '2018-04-03', 'CA', 'views', 33167);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8767, '2018-04-03', 'CA', 'plays', 46094);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8768, '2018-04-03', 'CA', 'clicks', 86323);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8781, '2018-04-03', 'CY', 'views', 39805);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8782, '2018-04-03', 'CY', 'plays', 20153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8783, '2018-04-03', 'CY', 'clicks', 20924);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8793, '2018-04-03', 'DE', 'views', 31707);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8794, '2018-04-03', 'DE', 'plays', 27930);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8795, '2018-04-03', 'DE', 'clicks', 63110);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8784, '2018-04-03', 'EG', 'views', 50820);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8785, '2018-04-03', 'EG', 'plays', 85457);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8786, '2018-04-03', 'EG', 'clicks', 45856);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8790, '2018-04-03', 'FR', 'views', 44439);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8791, '2018-04-03', 'FR', 'plays', 67115);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8792, '2018-04-03', 'FR', 'clicks', 58973);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8778, '2018-04-03', 'HR', 'views', 66477);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8779, '2018-04-03', 'HR', 'plays', 39972);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8780, '2018-04-03', 'HR', 'clicks', 54314);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8787, '2018-04-03', 'RS', 'views', 57120);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8788, '2018-04-03', 'RS', 'plays', 44530);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8789, '2018-04-03', 'RS', 'clicks', 98023);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8799, '2018-04-04', 'AT', 'views', 57739);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8800, '2018-04-04', 'AT', 'plays', 56397);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8801, '2018-04-04', 'AT', 'clicks', 31233);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8796, '2018-04-04', 'AU', 'views', 45625);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8797, '2018-04-04', 'AU', 'plays', 60899);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8798, '2018-04-04', 'AU', 'clicks', 27752);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8808, '2018-04-04', 'BA', 'views', 44941);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8809, '2018-04-04', 'BA', 'plays', 53738);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8810, '2018-04-04', 'BA', 'clicks', 76014);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8805, '2018-04-04', 'BE', 'views', 24910);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8806, '2018-04-04', 'BE', 'plays', 24264);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8807, '2018-04-04', 'BE', 'clicks', 75914);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8811, '2018-04-04', 'BR', 'views', 51983);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8812, '2018-04-04', 'BR', 'plays', 54570);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8813, '2018-04-04', 'BR', 'clicks', 43532);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8802, '2018-04-04', 'CA', 'views', 80960);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8803, '2018-04-04', 'CA', 'plays', 78460);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8804, '2018-04-04', 'CA', 'clicks', 44514);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8817, '2018-04-04', 'CY', 'views', 29084);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8818, '2018-04-04', 'CY', 'plays', 60593);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8819, '2018-04-04', 'CY', 'clicks', 52195);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8829, '2018-04-04', 'DE', 'views', 40055);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8830, '2018-04-04', 'DE', 'plays', 50727);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8831, '2018-04-04', 'DE', 'clicks', 56400);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8820, '2018-04-04', 'EG', 'views', 85526);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8821, '2018-04-04', 'EG', 'plays', 62625);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8822, '2018-04-04', 'EG', 'clicks', 31274);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8826, '2018-04-04', 'FR', 'views', 32493);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8827, '2018-04-04', 'FR', 'plays', 54396);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8828, '2018-04-04', 'FR', 'clicks', 63592);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8814, '2018-04-04', 'HR', 'views', 51936);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8815, '2018-04-04', 'HR', 'plays', 57604);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8816, '2018-04-04', 'HR', 'clicks', 53469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8823, '2018-04-04', 'RS', 'views', 66895);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8824, '2018-04-04', 'RS', 'plays', 60050);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8825, '2018-04-04', 'RS', 'clicks', 48061);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8835, '2018-04-05', 'AT', 'views', 26325);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8836, '2018-04-05', 'AT', 'plays', 48481);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8837, '2018-04-05', 'AT', 'clicks', 52920);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8832, '2018-04-05', 'AU', 'views', 29443);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8833, '2018-04-05', 'AU', 'plays', 30948);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8834, '2018-04-05', 'AU', 'clicks', 31436);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8844, '2018-04-05', 'BA', 'views', 50966);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8845, '2018-04-05', 'BA', 'plays', 56447);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8846, '2018-04-05', 'BA', 'clicks', 13520);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8841, '2018-04-05', 'BE', 'views', 60153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8842, '2018-04-05', 'BE', 'plays', 75674);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8843, '2018-04-05', 'BE', 'clicks', 75874);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8847, '2018-04-05', 'BR', 'views', 40457);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8848, '2018-04-05', 'BR', 'plays', 12981);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8849, '2018-04-05', 'BR', 'clicks', 10065);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8838, '2018-04-05', 'CA', 'views', 63008);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8839, '2018-04-05', 'CA', 'plays', 73151);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8840, '2018-04-05', 'CA', 'clicks', 52292);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8853, '2018-04-05', 'CY', 'views', 58952);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8854, '2018-04-05', 'CY', 'plays', 39722);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8855, '2018-04-05', 'CY', 'clicks', 61559);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8865, '2018-04-05', 'DE', 'views', 68445);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8866, '2018-04-05', 'DE', 'plays', 21560);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8867, '2018-04-05', 'DE', 'clicks', 61695);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8856, '2018-04-05', 'EG', 'views', 66932);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8857, '2018-04-05', 'EG', 'plays', 47363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8858, '2018-04-05', 'EG', 'clicks', 40392);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8862, '2018-04-05', 'FR', 'views', 31177);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8863, '2018-04-05', 'FR', 'plays', 70043);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8864, '2018-04-05', 'FR', 'clicks', 88711);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8850, '2018-04-05', 'HR', 'views', 65834);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8851, '2018-04-05', 'HR', 'plays', 8098);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8852, '2018-04-05', 'HR', 'clicks', 30841);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8859, '2018-04-05', 'RS', 'views', 44668);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8860, '2018-04-05', 'RS', 'plays', 56647);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8861, '2018-04-05', 'RS', 'clicks', 32259);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8871, '2018-04-06', 'AT', 'views', 91617);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8872, '2018-04-06', 'AT', 'plays', 31817);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8873, '2018-04-06', 'AT', 'clicks', 32422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8868, '2018-04-06', 'AU', 'views', 64334);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8869, '2018-04-06', 'AU', 'plays', 78409);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8870, '2018-04-06', 'AU', 'clicks', 74227);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8880, '2018-04-06', 'BA', 'views', 73343);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8881, '2018-04-06', 'BA', 'plays', 40758);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8882, '2018-04-06', 'BA', 'clicks', 35769);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8877, '2018-04-06', 'BE', 'views', 91535);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8878, '2018-04-06', 'BE', 'plays', 83004);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8879, '2018-04-06', 'BE', 'clicks', 72168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8883, '2018-04-06', 'BR', 'views', 46564);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8884, '2018-04-06', 'BR', 'plays', 52221);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8885, '2018-04-06', 'BR', 'clicks', 24125);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8874, '2018-04-06', 'CA', 'views', 43507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8875, '2018-04-06', 'CA', 'plays', 50199);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8876, '2018-04-06', 'CA', 'clicks', 49424);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8889, '2018-04-06', 'CY', 'views', 15392);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8890, '2018-04-06', 'CY', 'plays', 23077);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8891, '2018-04-06', 'CY', 'clicks', 27372);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8901, '2018-04-06', 'DE', 'views', 61717);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8902, '2018-04-06', 'DE', 'plays', 21926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8903, '2018-04-06', 'DE', 'clicks', 48507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8892, '2018-04-06', 'EG', 'views', 77212);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8893, '2018-04-06', 'EG', 'plays', 31928);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8894, '2018-04-06', 'EG', 'clicks', 93188);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8898, '2018-04-06', 'FR', 'views', 41640);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8899, '2018-04-06', 'FR', 'plays', 57409);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8900, '2018-04-06', 'FR', 'clicks', 35023);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8886, '2018-04-06', 'HR', 'views', 40877);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8887, '2018-04-06', 'HR', 'plays', 63624);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8888, '2018-04-06', 'HR', 'clicks', 57716);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8895, '2018-04-06', 'RS', 'views', 56372);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8896, '2018-04-06', 'RS', 'plays', 26225);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8897, '2018-04-06', 'RS', 'clicks', 48473);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8907, '2018-04-07', 'AT', 'views', 68085);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8908, '2018-04-07', 'AT', 'plays', 48459);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8909, '2018-04-07', 'AT', 'clicks', 49858);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8904, '2018-04-07', 'AU', 'views', 17928);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8905, '2018-04-07', 'AU', 'plays', 25777);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8906, '2018-04-07', 'AU', 'clicks', 90323);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8916, '2018-04-07', 'BA', 'views', 69803);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8917, '2018-04-07', 'BA', 'plays', 76555);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8918, '2018-04-07', 'BA', 'clicks', 69948);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8913, '2018-04-07', 'BE', 'views', 64872);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8914, '2018-04-07', 'BE', 'plays', 30348);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8915, '2018-04-07', 'BE', 'clicks', 67529);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8919, '2018-04-07', 'BR', 'views', 56398);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8920, '2018-04-07', 'BR', 'plays', 56759);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8921, '2018-04-07', 'BR', 'clicks', 38454);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8910, '2018-04-07', 'CA', 'views', 86428);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8911, '2018-04-07', 'CA', 'plays', 51919);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8912, '2018-04-07', 'CA', 'clicks', 69772);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8925, '2018-04-07', 'CY', 'views', 55404);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8926, '2018-04-07', 'CY', 'plays', 39442);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8927, '2018-04-07', 'CY', 'clicks', 38214);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8937, '2018-04-07', 'DE', 'views', 59889);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8938, '2018-04-07', 'DE', 'plays', 23295);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8939, '2018-04-07', 'DE', 'clicks', 62353);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8928, '2018-04-07', 'EG', 'views', 36065);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8929, '2018-04-07', 'EG', 'plays', 6958);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8930, '2018-04-07', 'EG', 'clicks', 54463);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8934, '2018-04-07', 'FR', 'views', 64550);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8935, '2018-04-07', 'FR', 'plays', 41129);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8936, '2018-04-07', 'FR', 'clicks', 72248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8922, '2018-04-07', 'HR', 'views', 47877);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8923, '2018-04-07', 'HR', 'plays', 78847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8924, '2018-04-07', 'HR', 'clicks', 57405);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8931, '2018-04-07', 'RS', 'views', 92250);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8932, '2018-04-07', 'RS', 'plays', 53744);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8933, '2018-04-07', 'RS', 'clicks', 30000);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8943, '2018-04-08', 'AT', 'views', 62989);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8944, '2018-04-08', 'AT', 'plays', 30152);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8945, '2018-04-08', 'AT', 'clicks', 22135);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8940, '2018-04-08', 'AU', 'views', 9027);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8941, '2018-04-08', 'AU', 'plays', 35762);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8942, '2018-04-08', 'AU', 'clicks', 11542);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8952, '2018-04-08', 'BA', 'views', 88789);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8953, '2018-04-08', 'BA', 'plays', 25575);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8954, '2018-04-08', 'BA', 'clicks', 40101);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8949, '2018-04-08', 'BE', 'views', 62038);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8950, '2018-04-08', 'BE', 'plays', 48919);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8951, '2018-04-08', 'BE', 'clicks', 41591);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8955, '2018-04-08', 'BR', 'views', 33580);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8956, '2018-04-08', 'BR', 'plays', 7094);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8957, '2018-04-08', 'BR', 'clicks', 30051);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8946, '2018-04-08', 'CA', 'views', 50981);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8947, '2018-04-08', 'CA', 'plays', 54401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8948, '2018-04-08', 'CA', 'clicks', 41071);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8961, '2018-04-08', 'CY', 'views', 70015);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8962, '2018-04-08', 'CY', 'plays', 67429);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8963, '2018-04-08', 'CY', 'clicks', 57785);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8973, '2018-04-08', 'DE', 'views', 59047);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8974, '2018-04-08', 'DE', 'plays', 10015);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8975, '2018-04-08', 'DE', 'clicks', 85637);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8964, '2018-04-08', 'EG', 'views', 51802);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8965, '2018-04-08', 'EG', 'plays', 38290);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8966, '2018-04-08', 'EG', 'clicks', 76918);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8970, '2018-04-08', 'FR', 'views', 21118);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8971, '2018-04-08', 'FR', 'plays', 69019);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8972, '2018-04-08', 'FR', 'clicks', 75090);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8958, '2018-04-08', 'HR', 'views', 50646);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8959, '2018-04-08', 'HR', 'plays', 58334);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8960, '2018-04-08', 'HR', 'clicks', 33165);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8967, '2018-04-08', 'RS', 'views', 44448);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8968, '2018-04-08', 'RS', 'plays', 55018);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8969, '2018-04-08', 'RS', 'clicks', 51614);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8979, '2018-04-09', 'AT', 'views', 21821);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8980, '2018-04-09', 'AT', 'plays', 5681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8981, '2018-04-09', 'AT', 'clicks', 87880);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8976, '2018-04-09', 'AU', 'views', 64217);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8977, '2018-04-09', 'AU', 'plays', 17538);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8978, '2018-04-09', 'AU', 'clicks', 25450);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8988, '2018-04-09', 'BA', 'views', 68145);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8989, '2018-04-09', 'BA', 'plays', 51932);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8990, '2018-04-09', 'BA', 'clicks', 55879);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8985, '2018-04-09', 'BE', 'views', 26623);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8986, '2018-04-09', 'BE', 'plays', 43661);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8987, '2018-04-09', 'BE', 'clicks', 54410);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8991, '2018-04-09', 'BR', 'views', 89557);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8992, '2018-04-09', 'BR', 'plays', 35386);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8993, '2018-04-09', 'BR', 'clicks', 39438);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8982, '2018-04-09', 'CA', 'views', 49236);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8983, '2018-04-09', 'CA', 'plays', 45917);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8984, '2018-04-09', 'CA', 'clicks', 44314);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8997, '2018-04-09', 'CY', 'views', 32705);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8998, '2018-04-09', 'CY', 'plays', 81947);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8999, '2018-04-09', 'CY', 'clicks', 61448);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9009, '2018-04-09', 'DE', 'views', 22448);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9010, '2018-04-09', 'DE', 'plays', 40262);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9011, '2018-04-09', 'DE', 'clicks', 43746);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9000, '2018-04-09', 'EG', 'views', 28648);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9001, '2018-04-09', 'EG', 'plays', 40329);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9002, '2018-04-09', 'EG', 'clicks', 63504);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9006, '2018-04-09', 'FR', 'views', 46705);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9007, '2018-04-09', 'FR', 'plays', 22750);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9008, '2018-04-09', 'FR', 'clicks', 19763);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8994, '2018-04-09', 'HR', 'views', 57881);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8995, '2018-04-09', 'HR', 'plays', 48354);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(8996, '2018-04-09', 'HR', 'clicks', 45252);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9003, '2018-04-09', 'RS', 'views', 60188);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9004, '2018-04-09', 'RS', 'plays', 79716);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9005, '2018-04-09', 'RS', 'clicks', 35245);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9015, '2018-04-10', 'AT', 'views', 76781);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9016, '2018-04-10', 'AT', 'plays', 90733);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9017, '2018-04-10', 'AT', 'clicks', 67541);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9012, '2018-04-10', 'AU', 'views', 68852);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9013, '2018-04-10', 'AU', 'plays', 82215);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9014, '2018-04-10', 'AU', 'clicks', 58406);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9024, '2018-04-10', 'BA', 'views', 41915);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9025, '2018-04-10', 'BA', 'plays', 78554);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9026, '2018-04-10', 'BA', 'clicks', 92878);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9021, '2018-04-10', 'BE', 'views', 40335);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9022, '2018-04-10', 'BE', 'plays', 52009);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9023, '2018-04-10', 'BE', 'clicks', 59275);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9027, '2018-04-10', 'BR', 'views', 54688);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9028, '2018-04-10', 'BR', 'plays', 20384);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9029, '2018-04-10', 'BR', 'clicks', 67808);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9018, '2018-04-10', 'CA', 'views', 19556);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9019, '2018-04-10', 'CA', 'plays', 44024);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9020, '2018-04-10', 'CA', 'clicks', 53157);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9033, '2018-04-10', 'CY', 'views', 65425);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9034, '2018-04-10', 'CY', 'plays', 75141);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9035, '2018-04-10', 'CY', 'clicks', 72854);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9045, '2018-04-10', 'DE', 'views', 76205);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9046, '2018-04-10', 'DE', 'plays', 31672);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9047, '2018-04-10', 'DE', 'clicks', 50920);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9036, '2018-04-10', 'EG', 'views', 47815);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9037, '2018-04-10', 'EG', 'plays', 64367);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9038, '2018-04-10', 'EG', 'clicks', 51623);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9042, '2018-04-10', 'FR', 'views', 68698);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9043, '2018-04-10', 'FR', 'plays', 26178);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9044, '2018-04-10', 'FR', 'clicks', 58905);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9030, '2018-04-10', 'HR', 'views', 55657);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9031, '2018-04-10', 'HR', 'plays', 6970);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9032, '2018-04-10', 'HR', 'clicks', 54164);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9039, '2018-04-10', 'RS', 'views', 39501);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9040, '2018-04-10', 'RS', 'plays', 48514);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9041, '2018-04-10', 'RS', 'clicks', 65259);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9051, '2018-04-11', 'AT', 'views', 51644);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9052, '2018-04-11', 'AT', 'plays', 41032);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9053, '2018-04-11', 'AT', 'clicks', 48152);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9048, '2018-04-11', 'AU', 'views', 79577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9049, '2018-04-11', 'AU', 'plays', 51103);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9050, '2018-04-11', 'AU', 'clicks', 86053);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9060, '2018-04-11', 'BA', 'views', 45713);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9061, '2018-04-11', 'BA', 'plays', 41547);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9062, '2018-04-11', 'BA', 'clicks', 36543);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9057, '2018-04-11', 'BE', 'views', 68482);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9058, '2018-04-11', 'BE', 'plays', 18017);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9059, '2018-04-11', 'BE', 'clicks', 61741);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9063, '2018-04-11', 'BR', 'views', 54193);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9064, '2018-04-11', 'BR', 'plays', 51554);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9065, '2018-04-11', 'BR', 'clicks', 42824);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9054, '2018-04-11', 'CA', 'views', 83649);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9055, '2018-04-11', 'CA', 'plays', 80965);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9056, '2018-04-11', 'CA', 'clicks', 64650);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9069, '2018-04-11', 'CY', 'views', 81130);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9070, '2018-04-11', 'CY', 'plays', 87666);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9071, '2018-04-11', 'CY', 'clicks', 45961);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9081, '2018-04-11', 'DE', 'views', 34492);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9082, '2018-04-11', 'DE', 'plays', 53947);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9083, '2018-04-11', 'DE', 'clicks', 54271);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9072, '2018-04-11', 'EG', 'views', 76069);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9073, '2018-04-11', 'EG', 'plays', 61979);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9074, '2018-04-11', 'EG', 'clicks', 18536);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9078, '2018-04-11', 'FR', 'views', 51248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9079, '2018-04-11', 'FR', 'plays', 42530);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9080, '2018-04-11', 'FR', 'clicks', 70056);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9066, '2018-04-11', 'HR', 'views', 68141);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9067, '2018-04-11', 'HR', 'plays', 75009);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9068, '2018-04-11', 'HR', 'clicks', 28099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9075, '2018-04-11', 'RS', 'views', 30426);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9076, '2018-04-11', 'RS', 'plays', 60028);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9077, '2018-04-11', 'RS', 'clicks', 42700);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9087, '2018-04-12', 'AT', 'views', 51073);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9088, '2018-04-12', 'AT', 'plays', 25721);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9089, '2018-04-12', 'AT', 'clicks', 27632);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9084, '2018-04-12', 'AU', 'views', 57455);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9085, '2018-04-12', 'AU', 'plays', 31973);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9086, '2018-04-12', 'AU', 'clicks', 70717);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9096, '2018-04-12', 'BA', 'views', 35003);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9097, '2018-04-12', 'BA', 'plays', 74173);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9098, '2018-04-12', 'BA', 'clicks', 70110);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9093, '2018-04-12', 'BE', 'views', 17522);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9094, '2018-04-12', 'BE', 'plays', 60741);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9095, '2018-04-12', 'BE', 'clicks', 62370);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9099, '2018-04-12', 'BR', 'views', 41635);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9100, '2018-04-12', 'BR', 'plays', 48968);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9101, '2018-04-12', 'BR', 'clicks', 79728);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9090, '2018-04-12', 'CA', 'views', 61878);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9091, '2018-04-12', 'CA', 'plays', 54311);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9092, '2018-04-12', 'CA', 'clicks', 87426);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9105, '2018-04-12', 'CY', 'views', 30188);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9106, '2018-04-12', 'CY', 'plays', 67719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9107, '2018-04-12', 'CY', 'clicks', 25800);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9117, '2018-04-12', 'DE', 'views', 73188);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9118, '2018-04-12', 'DE', 'plays', 19827);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9119, '2018-04-12', 'DE', 'clicks', 87481);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9108, '2018-04-12', 'EG', 'views', 46300);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9109, '2018-04-12', 'EG', 'plays', 95373);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9110, '2018-04-12', 'EG', 'clicks', 68873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9114, '2018-04-12', 'FR', 'views', 47340);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9115, '2018-04-12', 'FR', 'plays', 38383);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9116, '2018-04-12', 'FR', 'clicks', 72081);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9102, '2018-04-12', 'HR', 'views', 46975);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9103, '2018-04-12', 'HR', 'plays', 48787);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9104, '2018-04-12', 'HR', 'clicks', 76692);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9111, '2018-04-12', 'RS', 'views', 64882);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9112, '2018-04-12', 'RS', 'plays', 48700);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9113, '2018-04-12', 'RS', 'clicks', 71407);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9123, '2018-04-13', 'AT', 'views', 28389);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9124, '2018-04-13', 'AT', 'plays', 26006);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9125, '2018-04-13', 'AT', 'clicks', 52608);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9120, '2018-04-13', 'AU', 'views', 44992);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9121, '2018-04-13', 'AU', 'plays', 51130);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9122, '2018-04-13', 'AU', 'clicks', 33893);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9132, '2018-04-13', 'BA', 'views', 88606);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9133, '2018-04-13', 'BA', 'plays', 69101);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9134, '2018-04-13', 'BA', 'clicks', 57335);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9129, '2018-04-13', 'BE', 'views', 6391);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9130, '2018-04-13', 'BE', 'plays', 79751);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9131, '2018-04-13', 'BE', 'clicks', 72630);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9135, '2018-04-13', 'BR', 'views', 82452);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9136, '2018-04-13', 'BR', 'plays', 69890);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9137, '2018-04-13', 'BR', 'clicks', 30476);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9126, '2018-04-13', 'CA', 'views', 37600);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9127, '2018-04-13', 'CA', 'plays', 22466);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9128, '2018-04-13', 'CA', 'clicks', 40681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9141, '2018-04-13', 'CY', 'views', 46001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9142, '2018-04-13', 'CY', 'plays', 83912);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9143, '2018-04-13', 'CY', 'clicks', 72665);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9153, '2018-04-13', 'DE', 'views', 26431);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9154, '2018-04-13', 'DE', 'plays', 18810);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9155, '2018-04-13', 'DE', 'clicks', 57484);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9144, '2018-04-13', 'EG', 'views', 20414);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9145, '2018-04-13', 'EG', 'plays', 73770);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9146, '2018-04-13', 'EG', 'clicks', 83791);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9150, '2018-04-13', 'FR', 'views', 44280);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9151, '2018-04-13', 'FR', 'plays', 36956);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9152, '2018-04-13', 'FR', 'clicks', 48935);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9138, '2018-04-13', 'HR', 'views', 48155);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9139, '2018-04-13', 'HR', 'plays', 47742);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9140, '2018-04-13', 'HR', 'clicks', 71650);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9147, '2018-04-13', 'RS', 'views', 47984);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9148, '2018-04-13', 'RS', 'plays', 36215);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9149, '2018-04-13', 'RS', 'clicks', 64478);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9159, '2018-04-14', 'AT', 'views', 78640);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9160, '2018-04-14', 'AT', 'plays', 42970);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9161, '2018-04-14', 'AT', 'clicks', 29764);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9156, '2018-04-14', 'AU', 'views', 56569);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9157, '2018-04-14', 'AU', 'plays', 38182);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9158, '2018-04-14', 'AU', 'clicks', 9729);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9168, '2018-04-14', 'BA', 'views', 37465);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9169, '2018-04-14', 'BA', 'plays', 47762);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9170, '2018-04-14', 'BA', 'clicks', 38584);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9165, '2018-04-14', 'BE', 'views', 44762);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9166, '2018-04-14', 'BE', 'plays', 45809);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9167, '2018-04-14', 'BE', 'clicks', 59499);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9171, '2018-04-14', 'BR', 'views', 69679);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9172, '2018-04-14', 'BR', 'plays', 44448);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9173, '2018-04-14', 'BR', 'clicks', 49617);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9162, '2018-04-14', 'CA', 'views', 39303);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9163, '2018-04-14', 'CA', 'plays', 28366);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9164, '2018-04-14', 'CA', 'clicks', 3925);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9177, '2018-04-14', 'CY', 'views', 62293);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9178, '2018-04-14', 'CY', 'plays', 47257);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9179, '2018-04-14', 'CY', 'clicks', 65127);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9189, '2018-04-14', 'DE', 'views', 60575);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9190, '2018-04-14', 'DE', 'plays', 50552);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9191, '2018-04-14', 'DE', 'clicks', 20015);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9180, '2018-04-14', 'EG', 'views', 51050);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9181, '2018-04-14', 'EG', 'plays', 26573);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9182, '2018-04-14', 'EG', 'clicks', 39056);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9186, '2018-04-14', 'FR', 'views', 65941);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9187, '2018-04-14', 'FR', 'plays', 52082);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9188, '2018-04-14', 'FR', 'clicks', 75407);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9174, '2018-04-14', 'HR', 'views', 13160);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9175, '2018-04-14', 'HR', 'plays', 76062);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9176, '2018-04-14', 'HR', 'clicks', 77543);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9183, '2018-04-14', 'RS', 'views', 22269);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9184, '2018-04-14', 'RS', 'plays', 44648);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9185, '2018-04-14', 'RS', 'clicks', 79782);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9195, '2018-04-15', 'AT', 'views', 23656);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9196, '2018-04-15', 'AT', 'plays', 74150);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9197, '2018-04-15', 'AT', 'clicks', 78098);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9192, '2018-04-15', 'AU', 'views', 39338);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9193, '2018-04-15', 'AU', 'plays', 84798);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9194, '2018-04-15', 'AU', 'clicks', 43966);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9204, '2018-04-15', 'BA', 'views', 46857);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9205, '2018-04-15', 'BA', 'plays', 37890);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9206, '2018-04-15', 'BA', 'clicks', 86847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9201, '2018-04-15', 'BE', 'views', 63233);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9202, '2018-04-15', 'BE', 'plays', 64351);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9203, '2018-04-15', 'BE', 'clicks', 38347);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9207, '2018-04-15', 'BR', 'views', 52089);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9208, '2018-04-15', 'BR', 'plays', 69445);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9209, '2018-04-15', 'BR', 'clicks', 42934);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9198, '2018-04-15', 'CA', 'views', 32389);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9199, '2018-04-15', 'CA', 'plays', 18516);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9200, '2018-04-15', 'CA', 'clicks', 43676);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9213, '2018-04-15', 'CY', 'views', 23194);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9214, '2018-04-15', 'CY', 'plays', 35247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9215, '2018-04-15', 'CY', 'clicks', 11248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9225, '2018-04-15', 'DE', 'views', 28670);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9226, '2018-04-15', 'DE', 'plays', 26119);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9227, '2018-04-15', 'DE', 'clicks', 64851);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9216, '2018-04-15', 'EG', 'views', 54267);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9217, '2018-04-15', 'EG', 'plays', 15124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9218, '2018-04-15', 'EG', 'clicks', 20847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9222, '2018-04-15', 'FR', 'views', 51703);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9223, '2018-04-15', 'FR', 'plays', 40665);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9224, '2018-04-15', 'FR', 'clicks', 26346);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9210, '2018-04-15', 'HR', 'views', 82347);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9211, '2018-04-15', 'HR', 'plays', 50510);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9212, '2018-04-15', 'HR', 'clicks', 21564);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9219, '2018-04-15', 'RS', 'views', 17654);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9220, '2018-04-15', 'RS', 'plays', 63061);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9221, '2018-04-15', 'RS', 'clicks', 14479);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9231, '2018-04-16', 'AT', 'views', 73972);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9232, '2018-04-16', 'AT', 'plays', 35166);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9233, '2018-04-16', 'AT', 'clicks', 60723);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9228, '2018-04-16', 'AU', 'views', 67202);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9229, '2018-04-16', 'AU', 'plays', 44972);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9230, '2018-04-16', 'AU', 'clicks', 16754);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9240, '2018-04-16', 'BA', 'views', 41430);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9241, '2018-04-16', 'BA', 'plays', 19829);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9242, '2018-04-16', 'BA', 'clicks', 19091);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9237, '2018-04-16', 'BE', 'views', 72374);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9238, '2018-04-16', 'BE', 'plays', 73505);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9239, '2018-04-16', 'BE', 'clicks', 37560);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9243, '2018-04-16', 'BR', 'views', 50347);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9244, '2018-04-16', 'BR', 'plays', 26408);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9245, '2018-04-16', 'BR', 'clicks', 77392);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9234, '2018-04-16', 'CA', 'views', 40248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9235, '2018-04-16', 'CA', 'plays', 29760);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9236, '2018-04-16', 'CA', 'clicks', 31335);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9249, '2018-04-16', 'CY', 'views', 37873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9250, '2018-04-16', 'CY', 'plays', 40545);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9251, '2018-04-16', 'CY', 'clicks', 23958);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9261, '2018-04-16', 'DE', 'views', 65635);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9262, '2018-04-16', 'DE', 'plays', 58972);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9263, '2018-04-16', 'DE', 'clicks', 23547);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9252, '2018-04-16', 'EG', 'views', 22750);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9253, '2018-04-16', 'EG', 'plays', 68877);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9254, '2018-04-16', 'EG', 'clicks', 54984);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9258, '2018-04-16', 'FR', 'views', 37356);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9259, '2018-04-16', 'FR', 'plays', 51106);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9260, '2018-04-16', 'FR', 'clicks', 54531);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9246, '2018-04-16', 'HR', 'views', 3362);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9247, '2018-04-16', 'HR', 'plays', 55399);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9248, '2018-04-16', 'HR', 'clicks', 49394);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9255, '2018-04-16', 'RS', 'views', 61728);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9256, '2018-04-16', 'RS', 'plays', 35081);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9257, '2018-04-16', 'RS', 'clicks', 85009);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9267, '2018-04-17', 'AT', 'views', 14596);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9268, '2018-04-17', 'AT', 'plays', 6153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9269, '2018-04-17', 'AT', 'clicks', 72628);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9264, '2018-04-17', 'AU', 'views', 25573);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9265, '2018-04-17', 'AU', 'plays', 76309);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9266, '2018-04-17', 'AU', 'clicks', 79465);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9276, '2018-04-17', 'BA', 'views', 29579);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9277, '2018-04-17', 'BA', 'plays', 14097);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9278, '2018-04-17', 'BA', 'clicks', 84597);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9273, '2018-04-17', 'BE', 'views', 41611);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9274, '2018-04-17', 'BE', 'plays', 22873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9275, '2018-04-17', 'BE', 'clicks', 90738);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9279, '2018-04-17', 'BR', 'views', 68419);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9280, '2018-04-17', 'BR', 'plays', 47937);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9281, '2018-04-17', 'BR', 'clicks', 39179);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9270, '2018-04-17', 'CA', 'views', 57544);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9271, '2018-04-17', 'CA', 'plays', 31679);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9272, '2018-04-17', 'CA', 'clicks', 47811);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9285, '2018-04-17', 'CY', 'views', 7480);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9286, '2018-04-17', 'CY', 'plays', 47912);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9287, '2018-04-17', 'CY', 'clicks', 26097);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9297, '2018-04-17', 'DE', 'views', 19997);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9298, '2018-04-17', 'DE', 'plays', 45273);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9299, '2018-04-17', 'DE', 'clicks', 17717);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9288, '2018-04-17', 'EG', 'views', 48288);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9289, '2018-04-17', 'EG', 'plays', 37740);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9290, '2018-04-17', 'EG', 'clicks', 42695);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9294, '2018-04-17', 'FR', 'views', 44796);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9295, '2018-04-17', 'FR', 'plays', 44158);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9296, '2018-04-17', 'FR', 'clicks', 78900);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9282, '2018-04-17', 'HR', 'views', 32898);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9283, '2018-04-17', 'HR', 'plays', 62379);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9284, '2018-04-17', 'HR', 'clicks', 25887);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9291, '2018-04-17', 'RS', 'views', 38254);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9292, '2018-04-17', 'RS', 'plays', 74561);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9293, '2018-04-17', 'RS', 'clicks', 42929);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9303, '2018-04-18', 'AT', 'views', 18347);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9304, '2018-04-18', 'AT', 'plays', 44989);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9305, '2018-04-18', 'AT', 'clicks', 44004);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9300, '2018-04-18', 'AU', 'views', 86900);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9301, '2018-04-18', 'AU', 'plays', 22604);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9302, '2018-04-18', 'AU', 'clicks', 35043);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9312, '2018-04-18', 'BA', 'views', 77681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9313, '2018-04-18', 'BA', 'plays', 65657);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9314, '2018-04-18', 'BA', 'clicks', 49449);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9309, '2018-04-18', 'BE', 'views', 73877);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9310, '2018-04-18', 'BE', 'plays', 39296);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9311, '2018-04-18', 'BE', 'clicks', 30536);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9315, '2018-04-18', 'BR', 'views', 65489);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9316, '2018-04-18', 'BR', 'plays', 61060);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9317, '2018-04-18', 'BR', 'clicks', 66459);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9306, '2018-04-18', 'CA', 'views', 91006);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9307, '2018-04-18', 'CA', 'plays', 51343);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9308, '2018-04-18', 'CA', 'clicks', 53170);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9321, '2018-04-18', 'CY', 'views', 87577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9322, '2018-04-18', 'CY', 'plays', 34839);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9323, '2018-04-18', 'CY', 'clicks', 62298);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9333, '2018-04-18', 'DE', 'views', 38419);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9334, '2018-04-18', 'DE', 'plays', 918);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9335, '2018-04-18', 'DE', 'clicks', 47966);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9324, '2018-04-18', 'EG', 'views', 21054);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9325, '2018-04-18', 'EG', 'plays', 44724);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9326, '2018-04-18', 'EG', 'clicks', 71403);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9330, '2018-04-18', 'FR', 'views', 36626);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9331, '2018-04-18', 'FR', 'plays', 45380);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9332, '2018-04-18', 'FR', 'clicks', 39027);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9318, '2018-04-18', 'HR', 'views', 51555);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9319, '2018-04-18', 'HR', 'plays', 55430);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9320, '2018-04-18', 'HR', 'clicks', 71583);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9327, '2018-04-18', 'RS', 'views', 75501);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9328, '2018-04-18', 'RS', 'plays', 25569);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9329, '2018-04-18', 'RS', 'clicks', 30620);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9339, '2018-04-19', 'AT', 'views', 41493);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9340, '2018-04-19', 'AT', 'plays', 37943);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9341, '2018-04-19', 'AT', 'clicks', 56545);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9336, '2018-04-19', 'AU', 'views', 47520);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9337, '2018-04-19', 'AU', 'plays', 45826);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9338, '2018-04-19', 'AU', 'clicks', 53284);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9348, '2018-04-19', 'BA', 'views', 69547);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9349, '2018-04-19', 'BA', 'plays', 39378);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9350, '2018-04-19', 'BA', 'clicks', 43714);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9345, '2018-04-19', 'BE', 'views', 63956);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9346, '2018-04-19', 'BE', 'plays', 57281);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9347, '2018-04-19', 'BE', 'clicks', 43267);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9351, '2018-04-19', 'BR', 'views', 47541);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9352, '2018-04-19', 'BR', 'plays', 23837);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9353, '2018-04-19', 'BR', 'clicks', 50899);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9342, '2018-04-19', 'CA', 'views', 85610);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9343, '2018-04-19', 'CA', 'plays', 48928);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9344, '2018-04-19', 'CA', 'clicks', 48583);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9357, '2018-04-19', 'CY', 'views', 58247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9358, '2018-04-19', 'CY', 'plays', 47984);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9359, '2018-04-19', 'CY', 'clicks', 39039);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9369, '2018-04-19', 'DE', 'views', 71741);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9370, '2018-04-19', 'DE', 'plays', 38358);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9371, '2018-04-19', 'DE', 'clicks', 68937);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9360, '2018-04-19', 'EG', 'views', 46423);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9361, '2018-04-19', 'EG', 'plays', 55361);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9362, '2018-04-19', 'EG', 'clicks', 44526);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9366, '2018-04-19', 'FR', 'views', 62918);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9367, '2018-04-19', 'FR', 'plays', 14359);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9368, '2018-04-19', 'FR', 'clicks', 47745);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9354, '2018-04-19', 'HR', 'views', 38947);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9355, '2018-04-19', 'HR', 'plays', 20140);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9356, '2018-04-19', 'HR', 'clicks', 48323);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9363, '2018-04-19', 'RS', 'views', 36704);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9364, '2018-04-19', 'RS', 'plays', 55108);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9365, '2018-04-19', 'RS', 'clicks', 37942);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9375, '2018-04-20', 'AT', 'views', 64161);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9376, '2018-04-20', 'AT', 'plays', 47883);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9377, '2018-04-20', 'AT', 'clicks', 7464);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9372, '2018-04-20', 'AU', 'views', 29310);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9373, '2018-04-20', 'AU', 'plays', 35224);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9374, '2018-04-20', 'AU', 'clicks', 79504);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9384, '2018-04-20', 'BA', 'views', 75001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9385, '2018-04-20', 'BA', 'plays', 67348);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9386, '2018-04-20', 'BA', 'clicks', 29201);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9381, '2018-04-20', 'BE', 'views', 16223);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9382, '2018-04-20', 'BE', 'plays', 13385);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9383, '2018-04-20', 'BE', 'clicks', 30308);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9387, '2018-04-20', 'BR', 'views', 41776);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9388, '2018-04-20', 'BR', 'plays', 33919);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9389, '2018-04-20', 'BR', 'clicks', 83728);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9378, '2018-04-20', 'CA', 'views', 52509);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9379, '2018-04-20', 'CA', 'plays', 57901);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9380, '2018-04-20', 'CA', 'clicks', 32353);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9393, '2018-04-20', 'CY', 'views', 74012);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9394, '2018-04-20', 'CY', 'plays', 12161);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9395, '2018-04-20', 'CY', 'clicks', 53817);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9405, '2018-04-20', 'DE', 'views', 48186);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9406, '2018-04-20', 'DE', 'plays', 80683);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9407, '2018-04-20', 'DE', 'clicks', 34183);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9396, '2018-04-20', 'EG', 'views', 45080);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9397, '2018-04-20', 'EG', 'plays', 26485);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9398, '2018-04-20', 'EG', 'clicks', 59754);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9402, '2018-04-20', 'FR', 'views', 53231);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9403, '2018-04-20', 'FR', 'plays', 13633);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9404, '2018-04-20', 'FR', 'clicks', 66081);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9390, '2018-04-20', 'HR', 'views', 68204);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9391, '2018-04-20', 'HR', 'plays', 11107);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9392, '2018-04-20', 'HR', 'clicks', 60787);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9399, '2018-04-20', 'RS', 'views', 50769);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9400, '2018-04-20', 'RS', 'plays', 63097);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9401, '2018-04-20', 'RS', 'clicks', 48149);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9411, '2018-04-21', 'AT', 'views', 79703);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9412, '2018-04-21', 'AT', 'plays', 52437);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9413, '2018-04-21', 'AT', 'clicks', 57651);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9408, '2018-04-21', 'AU', 'views', 93102);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9409, '2018-04-21', 'AU', 'plays', 54468);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9410, '2018-04-21', 'AU', 'clicks', 61963);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9420, '2018-04-21', 'BA', 'views', 82131);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9421, '2018-04-21', 'BA', 'plays', 51353);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9422, '2018-04-21', 'BA', 'clicks', 10479);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9417, '2018-04-21', 'BE', 'views', 59087);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9418, '2018-04-21', 'BE', 'plays', 50639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9419, '2018-04-21', 'BE', 'clicks', 17977);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9423, '2018-04-21', 'BR', 'views', 29366);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9424, '2018-04-21', 'BR', 'plays', 64372);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9425, '2018-04-21', 'BR', 'clicks', 77963);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9414, '2018-04-21', 'CA', 'views', 58453);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9415, '2018-04-21', 'CA', 'plays', 84871);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9416, '2018-04-21', 'CA', 'clicks', 47009);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9429, '2018-04-21', 'CY', 'views', 31902);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9430, '2018-04-21', 'CY', 'plays', 34426);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9431, '2018-04-21', 'CY', 'clicks', 12486);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9441, '2018-04-21', 'DE', 'views', 76681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9442, '2018-04-21', 'DE', 'plays', 55679);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9443, '2018-04-21', 'DE', 'clicks', 63400);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9432, '2018-04-21', 'EG', 'views', 62157);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9433, '2018-04-21', 'EG', 'plays', 24833);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9434, '2018-04-21', 'EG', 'clicks', 33259);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9438, '2018-04-21', 'FR', 'views', 31263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9439, '2018-04-21', 'FR', 'plays', 52779);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9440, '2018-04-21', 'FR', 'clicks', 43024);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9426, '2018-04-21', 'HR', 'views', 89928);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9427, '2018-04-21', 'HR', 'plays', 44443);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9428, '2018-04-21', 'HR', 'clicks', 51078);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9435, '2018-04-21', 'RS', 'views', 10528);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9436, '2018-04-21', 'RS', 'plays', 53059);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9437, '2018-04-21', 'RS', 'clicks', 12470);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9447, '2018-04-22', 'AT', 'views', 65982);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9448, '2018-04-22', 'AT', 'plays', 25279);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9449, '2018-04-22', 'AT', 'clicks', 84664);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9444, '2018-04-22', 'AU', 'views', 59477);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9445, '2018-04-22', 'AU', 'plays', 47714);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9446, '2018-04-22', 'AU', 'clicks', 13588);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9456, '2018-04-22', 'BA', 'views', 66630);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9457, '2018-04-22', 'BA', 'plays', 43448);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9458, '2018-04-22', 'BA', 'clicks', 30671);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9453, '2018-04-22', 'BE', 'views', 29101);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9454, '2018-04-22', 'BE', 'plays', 80278);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9455, '2018-04-22', 'BE', 'clicks', 77060);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9459, '2018-04-22', 'BR', 'views', 89115);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9460, '2018-04-22', 'BR', 'plays', 21950);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9461, '2018-04-22', 'BR', 'clicks', 62608);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9450, '2018-04-22', 'CA', 'views', 31733);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9451, '2018-04-22', 'CA', 'plays', 32002);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9452, '2018-04-22', 'CA', 'clicks', 15675);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9465, '2018-04-22', 'CY', 'views', 56699);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9466, '2018-04-22', 'CY', 'plays', 25289);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9467, '2018-04-22', 'CY', 'clicks', 78849);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9477, '2018-04-22', 'DE', 'views', 61997);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9478, '2018-04-22', 'DE', 'plays', 74421);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9479, '2018-04-22', 'DE', 'clicks', 29464);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9468, '2018-04-22', 'EG', 'views', 79772);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9469, '2018-04-22', 'EG', 'plays', 54204);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9470, '2018-04-22', 'EG', 'clicks', 39623);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9474, '2018-04-22', 'FR', 'views', 57978);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9475, '2018-04-22', 'FR', 'plays', 35390);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9476, '2018-04-22', 'FR', 'clicks', 30162);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9462, '2018-04-22', 'HR', 'views', 80125);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9463, '2018-04-22', 'HR', 'plays', 22841);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9464, '2018-04-22', 'HR', 'clicks', 52428);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9471, '2018-04-22', 'RS', 'views', 17041);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9472, '2018-04-22', 'RS', 'plays', 30218);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9473, '2018-04-22', 'RS', 'clicks', 51492);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9483, '2018-04-23', 'AT', 'views', 52131);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9484, '2018-04-23', 'AT', 'plays', 37170);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9485, '2018-04-23', 'AT', 'clicks', 35003);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9480, '2018-04-23', 'AU', 'views', 24333);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9481, '2018-04-23', 'AU', 'plays', 46881);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9482, '2018-04-23', 'AU', 'clicks', 51321);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9492, '2018-04-23', 'BA', 'views', 64409);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9493, '2018-04-23', 'BA', 'plays', 71730);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9494, '2018-04-23', 'BA', 'clicks', 43516);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9489, '2018-04-23', 'BE', 'views', 43965);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9490, '2018-04-23', 'BE', 'plays', 31771);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9491, '2018-04-23', 'BE', 'clicks', 34702);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9495, '2018-04-23', 'BR', 'views', 48985);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9496, '2018-04-23', 'BR', 'plays', 34329);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9497, '2018-04-23', 'BR', 'clicks', 43323);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9486, '2018-04-23', 'CA', 'views', 7358);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9487, '2018-04-23', 'CA', 'plays', 88070);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9488, '2018-04-23', 'CA', 'clicks', 39971);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9501, '2018-04-23', 'CY', 'views', 34135);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9502, '2018-04-23', 'CY', 'plays', 47390);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9503, '2018-04-23', 'CY', 'clicks', 27080);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9513, '2018-04-23', 'DE', 'views', 26552);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9514, '2018-04-23', 'DE', 'plays', 59970);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9515, '2018-04-23', 'DE', 'clicks', 39462);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9504, '2018-04-23', 'EG', 'views', 61385);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9505, '2018-04-23', 'EG', 'plays', 21469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9506, '2018-04-23', 'EG', 'clicks', 78467);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9510, '2018-04-23', 'FR', 'views', 91847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9511, '2018-04-23', 'FR', 'plays', 56817);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9512, '2018-04-23', 'FR', 'clicks', 40357);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9498, '2018-04-23', 'HR', 'views', 27858);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9499, '2018-04-23', 'HR', 'plays', 68226);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9500, '2018-04-23', 'HR', 'clicks', 29479);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9507, '2018-04-23', 'RS', 'views', 46864);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9508, '2018-04-23', 'RS', 'plays', 31385);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9509, '2018-04-23', 'RS', 'clicks', 44347);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9519, '2018-04-24', 'AT', 'views', 62658);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9520, '2018-04-24', 'AT', 'plays', 52867);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9521, '2018-04-24', 'AT', 'clicks', 41219);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9516, '2018-04-24', 'AU', 'views', 32176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9517, '2018-04-24', 'AU', 'plays', 22658);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9518, '2018-04-24', 'AU', 'clicks', 74398);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9528, '2018-04-24', 'BA', 'views', 69743);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9529, '2018-04-24', 'BA', 'plays', 72883);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9530, '2018-04-24', 'BA', 'clicks', 28894);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9525, '2018-04-24', 'BE', 'views', 54624);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9526, '2018-04-24', 'BE', 'plays', 18787);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9527, '2018-04-24', 'BE', 'clicks', 60412);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9531, '2018-04-24', 'BR', 'views', 31525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9532, '2018-04-24', 'BR', 'plays', 36523);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9533, '2018-04-24', 'BR', 'clicks', 81797);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9522, '2018-04-24', 'CA', 'views', 2909);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9523, '2018-04-24', 'CA', 'plays', 19653);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9524, '2018-04-24', 'CA', 'clicks', 79870);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9537, '2018-04-24', 'CY', 'views', 49540);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9538, '2018-04-24', 'CY', 'plays', 23248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9539, '2018-04-24', 'CY', 'clicks', 34686);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9549, '2018-04-24', 'DE', 'views', 26380);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9550, '2018-04-24', 'DE', 'plays', 63304);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9551, '2018-04-24', 'DE', 'clicks', 50755);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9540, '2018-04-24', 'EG', 'views', 33531);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9541, '2018-04-24', 'EG', 'plays', 92736);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9542, '2018-04-24', 'EG', 'clicks', 35501);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9546, '2018-04-24', 'FR', 'views', 16250);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9547, '2018-04-24', 'FR', 'plays', 81145);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9548, '2018-04-24', 'FR', 'clicks', 18561);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9534, '2018-04-24', 'HR', 'views', 60818);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9535, '2018-04-24', 'HR', 'plays', 68415);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9536, '2018-04-24', 'HR', 'clicks', 36317);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9543, '2018-04-24', 'RS', 'views', 41247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9544, '2018-04-24', 'RS', 'plays', 21540);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9545, '2018-04-24', 'RS', 'clicks', 47504);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9555, '2018-04-25', 'AT', 'views', 52097);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9556, '2018-04-25', 'AT', 'plays', 26808);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9557, '2018-04-25', 'AT', 'clicks', 64244);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9552, '2018-04-25', 'AU', 'views', 37823);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9553, '2018-04-25', 'AU', 'plays', 64244);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9554, '2018-04-25', 'AU', 'clicks', 40375);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9564, '2018-04-25', 'BA', 'views', 40208);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9565, '2018-04-25', 'BA', 'plays', 48679);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9566, '2018-04-25', 'BA', 'clicks', 47309);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9561, '2018-04-25', 'BE', 'views', 58664);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9562, '2018-04-25', 'BE', 'plays', 90224);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9563, '2018-04-25', 'BE', 'clicks', 76730);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9567, '2018-04-25', 'BR', 'views', 21490);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9568, '2018-04-25', 'BR', 'plays', 75619);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9569, '2018-04-25', 'BR', 'clicks', 68435);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9558, '2018-04-25', 'CA', 'views', 74259);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9559, '2018-04-25', 'CA', 'plays', 29395);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9560, '2018-04-25', 'CA', 'clicks', 28377);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9573, '2018-04-25', 'CY', 'views', 12177);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9574, '2018-04-25', 'CY', 'plays', 73494);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9575, '2018-04-25', 'CY', 'clicks', 44561);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9585, '2018-04-25', 'DE', 'views', 71132);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9586, '2018-04-25', 'DE', 'plays', 74449);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9587, '2018-04-25', 'DE', 'clicks', 49696);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9576, '2018-04-25', 'EG', 'views', 71050);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9577, '2018-04-25', 'EG', 'plays', 68767);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9578, '2018-04-25', 'EG', 'clicks', 33110);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9582, '2018-04-25', 'FR', 'views', 38032);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9583, '2018-04-25', 'FR', 'plays', 64875);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9584, '2018-04-25', 'FR', 'clicks', 81299);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9570, '2018-04-25', 'HR', 'views', 42873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9571, '2018-04-25', 'HR', 'plays', 38357);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9572, '2018-04-25', 'HR', 'clicks', 71032);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9579, '2018-04-25', 'RS', 'views', 54306);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9580, '2018-04-25', 'RS', 'plays', 76600);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9581, '2018-04-25', 'RS', 'clicks', 74625);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9591, '2018-04-26', 'AT', 'views', 39312);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9592, '2018-04-26', 'AT', 'plays', 32187);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9593, '2018-04-26', 'AT', 'clicks', 76729);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9588, '2018-04-26', 'AU', 'views', 42336);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9589, '2018-04-26', 'AU', 'plays', 78514);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9590, '2018-04-26', 'AU', 'clicks', 55395);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9600, '2018-04-26', 'BA', 'views', 57912);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9601, '2018-04-26', 'BA', 'plays', 62577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9602, '2018-04-26', 'BA', 'clicks', 67530);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9597, '2018-04-26', 'BE', 'views', 60724);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9598, '2018-04-26', 'BE', 'plays', 73652);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9599, '2018-04-26', 'BE', 'clicks', 683);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9603, '2018-04-26', 'BR', 'views', 16951);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9604, '2018-04-26', 'BR', 'plays', 21193);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9605, '2018-04-26', 'BR', 'clicks', 33105);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9594, '2018-04-26', 'CA', 'views', 88295);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9595, '2018-04-26', 'CA', 'plays', 61051);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9596, '2018-04-26', 'CA', 'clicks', 43392);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9609, '2018-04-26', 'CY', 'views', 56391);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9610, '2018-04-26', 'CY', 'plays', 26589);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9611, '2018-04-26', 'CY', 'clicks', 57278);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9621, '2018-04-26', 'DE', 'views', 72080);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9622, '2018-04-26', 'DE', 'plays', 43593);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9623, '2018-04-26', 'DE', 'clicks', 32042);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9612, '2018-04-26', 'EG', 'views', 78935);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9613, '2018-04-26', 'EG', 'plays', 78162);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9614, '2018-04-26', 'EG', 'clicks', 39648);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9618, '2018-04-26', 'FR', 'views', 53355);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9619, '2018-04-26', 'FR', 'plays', 79383);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9620, '2018-04-26', 'FR', 'clicks', 47989);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9606, '2018-04-26', 'HR', 'views', 46491);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9607, '2018-04-26', 'HR', 'plays', 64969);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9608, '2018-04-26', 'HR', 'clicks', 65278);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9615, '2018-04-26', 'RS', 'views', 45456);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9616, '2018-04-26', 'RS', 'plays', 64886);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9617, '2018-04-26', 'RS', 'clicks', 29366);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9627, '2018-04-27', 'AT', 'views', 37106);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9628, '2018-04-27', 'AT', 'plays', 52216);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9629, '2018-04-27', 'AT', 'clicks', 69525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9624, '2018-04-27', 'AU', 'views', 27401);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9625, '2018-04-27', 'AU', 'plays', 40976);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9626, '2018-04-27', 'AU', 'clicks', 92590);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9636, '2018-04-27', 'BA', 'views', 32405);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9637, '2018-04-27', 'BA', 'plays', 58195);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9638, '2018-04-27', 'BA', 'clicks', 55168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9633, '2018-04-27', 'BE', 'views', 30550);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9634, '2018-04-27', 'BE', 'plays', 42596);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9635, '2018-04-27', 'BE', 'clicks', 72840);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9639, '2018-04-27', 'BR', 'views', 46659);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9640, '2018-04-27', 'BR', 'plays', 49799);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9641, '2018-04-27', 'BR', 'clicks', 47529);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9630, '2018-04-27', 'CA', 'views', 57986);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9631, '2018-04-27', 'CA', 'plays', 81280);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9632, '2018-04-27', 'CA', 'clicks', 39239);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9645, '2018-04-27', 'CY', 'views', 51746);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9646, '2018-04-27', 'CY', 'plays', 22528);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9647, '2018-04-27', 'CY', 'clicks', 31480);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9657, '2018-04-27', 'DE', 'views', 47261);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9658, '2018-04-27', 'DE', 'plays', 5460);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9659, '2018-04-27', 'DE', 'clicks', 15288);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9648, '2018-04-27', 'EG', 'views', 26708);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9649, '2018-04-27', 'EG', 'plays', 48727);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9650, '2018-04-27', 'EG', 'clicks', 52780);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9654, '2018-04-27', 'FR', 'views', 24289);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9655, '2018-04-27', 'FR', 'plays', 62164);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9656, '2018-04-27', 'FR', 'clicks', 96515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9642, '2018-04-27', 'HR', 'views', 58081);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9643, '2018-04-27', 'HR', 'plays', 46437);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9644, '2018-04-27', 'HR', 'clicks', 27608);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9651, '2018-04-27', 'RS', 'views', 12894);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9652, '2018-04-27', 'RS', 'plays', 57502);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9653, '2018-04-27', 'RS', 'clicks', 50377);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9663, '2018-04-28', 'AT', 'views', 41113);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9664, '2018-04-28', 'AT', 'plays', 75793);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9665, '2018-04-28', 'AT', 'clicks', 38964);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9660, '2018-04-28', 'AU', 'views', 45736);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9661, '2018-04-28', 'AU', 'plays', 57495);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9662, '2018-04-28', 'AU', 'clicks', 58053);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9672, '2018-04-28', 'BA', 'views', 20253);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9673, '2018-04-28', 'BA', 'plays', 81495);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9674, '2018-04-28', 'BA', 'clicks', 51287);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9669, '2018-04-28', 'BE', 'views', 10704);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9670, '2018-04-28', 'BE', 'plays', 83034);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9671, '2018-04-28', 'BE', 'clicks', 52392);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9675, '2018-04-28', 'BR', 'views', 70868);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9676, '2018-04-28', 'BR', 'plays', 58677);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9677, '2018-04-28', 'BR', 'clicks', 28510);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9666, '2018-04-28', 'CA', 'views', 28148);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9667, '2018-04-28', 'CA', 'plays', 93378);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9668, '2018-04-28', 'CA', 'clicks', 43971);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9681, '2018-04-28', 'CY', 'views', 76598);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9682, '2018-04-28', 'CY', 'plays', 46667);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9683, '2018-04-28', 'CY', 'clicks', 63928);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9693, '2018-04-28', 'DE', 'views', 56796);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9694, '2018-04-28', 'DE', 'plays', 64683);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9695, '2018-04-28', 'DE', 'clicks', 20207);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9684, '2018-04-28', 'EG', 'views', 20215);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9685, '2018-04-28', 'EG', 'plays', 43873);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9686, '2018-04-28', 'EG', 'clicks', 70863);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9690, '2018-04-28', 'FR', 'views', 14290);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9691, '2018-04-28', 'FR', 'plays', 46946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9692, '2018-04-28', 'FR', 'clicks', 25074);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9678, '2018-04-28', 'HR', 'views', 57383);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9679, '2018-04-28', 'HR', 'plays', 52656);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9680, '2018-04-28', 'HR', 'clicks', 46991);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9687, '2018-04-28', 'RS', 'views', 54639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9688, '2018-04-28', 'RS', 'plays', 52751);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9689, '2018-04-28', 'RS', 'clicks', 25549);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9699, '2018-04-29', 'AT', 'views', 76860);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9700, '2018-04-29', 'AT', 'plays', 30995);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9701, '2018-04-29', 'AT', 'clicks', 34540);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9696, '2018-04-29', 'AU', 'views', 39535);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9697, '2018-04-29', 'AU', 'plays', 74293);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9698, '2018-04-29', 'AU', 'clicks', 48339);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9708, '2018-04-29', 'BA', 'views', 82304);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9709, '2018-04-29', 'BA', 'plays', 54454);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9710, '2018-04-29', 'BA', 'clicks', 64921);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9705, '2018-04-29', 'BE', 'views', 67753);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9706, '2018-04-29', 'BE', 'plays', 48966);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9707, '2018-04-29', 'BE', 'clicks', 79617);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9711, '2018-04-29', 'BR', 'views', 53404);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9712, '2018-04-29', 'BR', 'plays', 7917);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9713, '2018-04-29', 'BR', 'clicks', 80755);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9702, '2018-04-29', 'CA', 'views', 54060);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9703, '2018-04-29', 'CA', 'plays', 63363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9704, '2018-04-29', 'CA', 'clicks', 41624);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9717, '2018-04-29', 'CY', 'views', 80065);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9718, '2018-04-29', 'CY', 'plays', 44735);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9719, '2018-04-29', 'CY', 'clicks', 14431);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9729, '2018-04-29', 'DE', 'views', 36211);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9730, '2018-04-29', 'DE', 'plays', 20235);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9731, '2018-04-29', 'DE', 'clicks', 79211);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9720, '2018-04-29', 'EG', 'views', 60620);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9721, '2018-04-29', 'EG', 'plays', 24737);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9722, '2018-04-29', 'EG', 'clicks', 62537);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9726, '2018-04-29', 'FR', 'views', 17583);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9727, '2018-04-29', 'FR', 'plays', 35350);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9728, '2018-04-29', 'FR', 'clicks', 56233);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9714, '2018-04-29', 'HR', 'views', 53862);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9715, '2018-04-29', 'HR', 'plays', 31841);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9716, '2018-04-29', 'HR', 'clicks', 50527);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9723, '2018-04-29', 'RS', 'views', 42216);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9724, '2018-04-29', 'RS', 'plays', 74863);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9725, '2018-04-29', 'RS', 'clicks', 77339);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9735, '2018-04-30', 'AT', 'views', 24232);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9736, '2018-04-30', 'AT', 'plays', 43335);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9737, '2018-04-30', 'AT', 'clicks', 23946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9732, '2018-04-30', 'AU', 'views', 12374);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9733, '2018-04-30', 'AU', 'plays', 44843);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9734, '2018-04-30', 'AU', 'clicks', 74708);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9744, '2018-04-30', 'BA', 'views', 29915);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9745, '2018-04-30', 'BA', 'plays', 55698);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9746, '2018-04-30', 'BA', 'clicks', 29219);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9741, '2018-04-30', 'BE', 'views', 44505);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9742, '2018-04-30', 'BE', 'plays', 61611);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9743, '2018-04-30', 'BE', 'clicks', 23150);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9747, '2018-04-30', 'BR', 'views', 41694);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9748, '2018-04-30', 'BR', 'plays', 79719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9749, '2018-04-30', 'BR', 'clicks', 83372);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9738, '2018-04-30', 'CA', 'views', 49033);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9739, '2018-04-30', 'CA', 'plays', 83945);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9740, '2018-04-30', 'CA', 'clicks', 13896);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9753, '2018-04-30', 'CY', 'views', 42909);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9754, '2018-04-30', 'CY', 'plays', 41962);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9755, '2018-04-30', 'CY', 'clicks', 76558);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9765, '2018-04-30', 'DE', 'views', 28779);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9766, '2018-04-30', 'DE', 'plays', 33005);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9767, '2018-04-30', 'DE', 'clicks', 67536);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9756, '2018-04-30', 'EG', 'views', 57255);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9757, '2018-04-30', 'EG', 'plays', 56034);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9758, '2018-04-30', 'EG', 'clicks', 64301);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9762, '2018-04-30', 'FR', 'views', 63320);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9763, '2018-04-30', 'FR', 'plays', 60731);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9764, '2018-04-30', 'FR', 'clicks', 56067);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9750, '2018-04-30', 'HR', 'views', 15942);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9751, '2018-04-30', 'HR', 'plays', 96153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9752, '2018-04-30', 'HR', 'clicks', 22495);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9759, '2018-04-30', 'RS', 'views', 35799);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9760, '2018-04-30', 'RS', 'plays', 48947);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9761, '2018-04-30', 'RS', 'clicks', 72023);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9771, '2018-05-01', 'AT', 'views', 34128);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9772, '2018-05-01', 'AT', 'plays', 43805);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9773, '2018-05-01', 'AT', 'clicks', 52617);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9768, '2018-05-01', 'AU', 'views', 95696);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9769, '2018-05-01', 'AU', 'plays', 77515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9770, '2018-05-01', 'AU', 'clicks', 58976);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9780, '2018-05-01', 'BA', 'views', 46597);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9781, '2018-05-01', 'BA', 'plays', 71381);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9782, '2018-05-01', 'BA', 'clicks', 61260);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9777, '2018-05-01', 'BE', 'views', 91580);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9778, '2018-05-01', 'BE', 'plays', 36028);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9779, '2018-05-01', 'BE', 'clicks', 49687);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9783, '2018-05-01', 'BR', 'views', 41501);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9784, '2018-05-01', 'BR', 'plays', 13200);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9785, '2018-05-01', 'BR', 'clicks', 55295);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9774, '2018-05-01', 'CA', 'views', 90632);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9775, '2018-05-01', 'CA', 'plays', 56305);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9776, '2018-05-01', 'CA', 'clicks', 58391);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9789, '2018-05-01', 'CY', 'views', 22885);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9790, '2018-05-01', 'CY', 'plays', 21189);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9791, '2018-05-01', 'CY', 'clicks', 59556);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9801, '2018-05-01', 'DE', 'views', 83130);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9802, '2018-05-01', 'DE', 'plays', 12428);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9803, '2018-05-01', 'DE', 'clicks', 97726);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9792, '2018-05-01', 'EG', 'views', 28720);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9793, '2018-05-01', 'EG', 'plays', 19016);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9794, '2018-05-01', 'EG', 'clicks', 31032);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9798, '2018-05-01', 'FR', 'views', 50836);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9799, '2018-05-01', 'FR', 'plays', 41007);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9800, '2018-05-01', 'FR', 'clicks', 59987);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9786, '2018-05-01', 'HR', 'views', 11805);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9787, '2018-05-01', 'HR', 'plays', 73113);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9788, '2018-05-01', 'HR', 'clicks', 23424);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9795, '2018-05-01', 'RS', 'views', 55909);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9796, '2018-05-01', 'RS', 'plays', 76572);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9797, '2018-05-01', 'RS', 'clicks', 28250);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9807, '2018-05-02', 'AT', 'views', 73306);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9808, '2018-05-02', 'AT', 'plays', 60136);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9809, '2018-05-02', 'AT', 'clicks', 86317);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9804, '2018-05-02', 'AU', 'views', 58243);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9805, '2018-05-02', 'AU', 'plays', 49384);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9806, '2018-05-02', 'AU', 'clicks', 19911);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9816, '2018-05-02', 'BA', 'views', 8132);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9817, '2018-05-02', 'BA', 'plays', 47152);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9818, '2018-05-02', 'BA', 'clicks', 60533);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9813, '2018-05-02', 'BE', 'views', 39206);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9814, '2018-05-02', 'BE', 'plays', 29612);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9815, '2018-05-02', 'BE', 'clicks', 56418);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9819, '2018-05-02', 'BR', 'views', 41427);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9820, '2018-05-02', 'BR', 'plays', 88540);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9821, '2018-05-02', 'BR', 'clicks', 38900);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9810, '2018-05-02', 'CA', 'views', 27394);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9811, '2018-05-02', 'CA', 'plays', 40996);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9812, '2018-05-02', 'CA', 'clicks', 44804);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9825, '2018-05-02', 'CY', 'views', 55185);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9826, '2018-05-02', 'CY', 'plays', 69318);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9827, '2018-05-02', 'CY', 'clicks', 47628);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9837, '2018-05-02', 'DE', 'views', 59230);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9838, '2018-05-02', 'DE', 'plays', 51289);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9839, '2018-05-02', 'DE', 'clicks', 59795);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9828, '2018-05-02', 'EG', 'views', 29283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9829, '2018-05-02', 'EG', 'plays', 60026);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9830, '2018-05-02', 'EG', 'clicks', 75549);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9834, '2018-05-02', 'FR', 'views', 51735);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9835, '2018-05-02', 'FR', 'plays', 45785);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9836, '2018-05-02', 'FR', 'clicks', 62529);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9822, '2018-05-02', 'HR', 'views', 35091);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9823, '2018-05-02', 'HR', 'plays', 45034);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9824, '2018-05-02', 'HR', 'clicks', 45482);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9831, '2018-05-02', 'RS', 'views', 86292);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9832, '2018-05-02', 'RS', 'plays', 61569);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9833, '2018-05-02', 'RS', 'clicks', 86663);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9843, '2018-05-03', 'AT', 'views', 78154);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9844, '2018-05-03', 'AT', 'plays', 46177);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9845, '2018-05-03', 'AT', 'clicks', 79674);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9840, '2018-05-03', 'AU', 'views', 59951);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9841, '2018-05-03', 'AU', 'plays', 24258);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9842, '2018-05-03', 'AU', 'clicks', 55987);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9852, '2018-05-03', 'BA', 'views', 51143);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9853, '2018-05-03', 'BA', 'plays', 66365);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9854, '2018-05-03', 'BA', 'clicks', 74268);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9849, '2018-05-03', 'BE', 'views', 79434);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9850, '2018-05-03', 'BE', 'plays', 36367);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9851, '2018-05-03', 'BE', 'clicks', 84093);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9855, '2018-05-03', 'BR', 'views', 53380);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9856, '2018-05-03', 'BR', 'plays', 24055);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9857, '2018-05-03', 'BR', 'clicks', 29615);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9846, '2018-05-03', 'CA', 'views', 70206);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9847, '2018-05-03', 'CA', 'plays', 49033);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9848, '2018-05-03', 'CA', 'clicks', 47414);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9861, '2018-05-03', 'CY', 'views', 42197);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9862, '2018-05-03', 'CY', 'plays', 10974);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9863, '2018-05-03', 'CY', 'clicks', 70435);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9873, '2018-05-03', 'DE', 'views', 32525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9874, '2018-05-03', 'DE', 'plays', 26372);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9875, '2018-05-03', 'DE', 'clicks', 74966);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9864, '2018-05-03', 'EG', 'views', 51435);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9865, '2018-05-03', 'EG', 'plays', 47755);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9866, '2018-05-03', 'EG', 'clicks', 25264);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9870, '2018-05-03', 'FR', 'views', 62719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9871, '2018-05-03', 'FR', 'plays', 7665);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9872, '2018-05-03', 'FR', 'clicks', 37128);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9858, '2018-05-03', 'HR', 'views', 36508);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9859, '2018-05-03', 'HR', 'plays', 39740);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9860, '2018-05-03', 'HR', 'clicks', 51951);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9867, '2018-05-03', 'RS', 'views', 62685);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9868, '2018-05-03', 'RS', 'plays', 29497);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9869, '2018-05-03', 'RS', 'clicks', 72917);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9879, '2018-05-04', 'AT', 'views', 49383);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9880, '2018-05-04', 'AT', 'plays', 52235);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9881, '2018-05-04', 'AT', 'clicks', 35087);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9876, '2018-05-04', 'AU', 'views', 67099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9877, '2018-05-04', 'AU', 'plays', 9932);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9878, '2018-05-04', 'AU', 'clicks', 63246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9888, '2018-05-04', 'BA', 'views', 45827);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9889, '2018-05-04', 'BA', 'plays', 16001);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9890, '2018-05-04', 'BA', 'clicks', 33139);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9885, '2018-05-04', 'BE', 'views', 83932);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9886, '2018-05-04', 'BE', 'plays', 23691);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9887, '2018-05-04', 'BE', 'clicks', 49568);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9891, '2018-05-04', 'BR', 'views', 49144);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9892, '2018-05-04', 'BR', 'plays', 49153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9893, '2018-05-04', 'BR', 'clicks', 74656);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9882, '2018-05-04', 'CA', 'views', 90011);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9883, '2018-05-04', 'CA', 'plays', 74187);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9884, '2018-05-04', 'CA', 'clicks', 84366);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9897, '2018-05-04', 'CY', 'views', 83293);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9898, '2018-05-04', 'CY', 'plays', 31820);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9899, '2018-05-04', 'CY', 'clicks', 19770);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9909, '2018-05-04', 'DE', 'views', 36820);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9910, '2018-05-04', 'DE', 'plays', 38838);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9911, '2018-05-04', 'DE', 'clicks', 36110);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9900, '2018-05-04', 'EG', 'views', 70275);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9901, '2018-05-04', 'EG', 'plays', 66729);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9902, '2018-05-04', 'EG', 'clicks', 46731);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9906, '2018-05-04', 'FR', 'views', 80875);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9907, '2018-05-04', 'FR', 'plays', 34156);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9908, '2018-05-04', 'FR', 'clicks', 56255);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9894, '2018-05-04', 'HR', 'views', 36297);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9895, '2018-05-04', 'HR', 'plays', 25739);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9896, '2018-05-04', 'HR', 'clicks', 79601);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9903, '2018-05-04', 'RS', 'views', 26357);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9904, '2018-05-04', 'RS', 'plays', 74267);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9905, '2018-05-04', 'RS', 'clicks', 50558);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9915, '2018-05-05', 'AT', 'views', 57222);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9916, '2018-05-05', 'AT', 'plays', 30620);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9917, '2018-05-05', 'AT', 'clicks', 69785);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9912, '2018-05-05', 'AU', 'views', 69937);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9913, '2018-05-05', 'AU', 'plays', 70762);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9914, '2018-05-05', 'AU', 'clicks', 74142);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9924, '2018-05-05', 'BA', 'views', 22029);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9925, '2018-05-05', 'BA', 'plays', 37979);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9926, '2018-05-05', 'BA', 'clicks', 24444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9921, '2018-05-05', 'BE', 'views', 74774);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9922, '2018-05-05', 'BE', 'plays', 53879);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9923, '2018-05-05', 'BE', 'clicks', 64040);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9927, '2018-05-05', 'BR', 'views', 34280);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9928, '2018-05-05', 'BR', 'plays', 46491);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9929, '2018-05-05', 'BR', 'clicks', 14231);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9918, '2018-05-05', 'CA', 'views', 37562);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9919, '2018-05-05', 'CA', 'plays', 64031);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9920, '2018-05-05', 'CA', 'clicks', 58074);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9933, '2018-05-05', 'CY', 'views', 72681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9934, '2018-05-05', 'CY', 'plays', 70494);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9935, '2018-05-05', 'CY', 'clicks', 61793);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9945, '2018-05-05', 'DE', 'views', 68892);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9946, '2018-05-05', 'DE', 'plays', 72162);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9947, '2018-05-05', 'DE', 'clicks', 38698);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9936, '2018-05-05', 'EG', 'views', 59482);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9937, '2018-05-05', 'EG', 'plays', 35219);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9938, '2018-05-05', 'EG', 'clicks', 47774);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9942, '2018-05-05', 'FR', 'views', 95448);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9943, '2018-05-05', 'FR', 'plays', 73352);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9944, '2018-05-05', 'FR', 'clicks', 35685);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9930, '2018-05-05', 'HR', 'views', 41566);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9931, '2018-05-05', 'HR', 'plays', 54187);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9932, '2018-05-05', 'HR', 'clicks', 27360);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9939, '2018-05-05', 'RS', 'views', 74374);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9940, '2018-05-05', 'RS', 'plays', 43081);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9941, '2018-05-05', 'RS', 'clicks', 61806);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9951, '2018-05-06', 'AT', 'views', 40635);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9952, '2018-05-06', 'AT', 'plays', 43407);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9953, '2018-05-06', 'AT', 'clicks', 65699);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9948, '2018-05-06', 'AU', 'views', 48290);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9949, '2018-05-06', 'AU', 'plays', 45209);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9950, '2018-05-06', 'AU', 'clicks', 69906);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9960, '2018-05-06', 'BA', 'views', 76245);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9961, '2018-05-06', 'BA', 'plays', 64192);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9962, '2018-05-06', 'BA', 'clicks', 66787);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9957, '2018-05-06', 'BE', 'views', 32812);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9958, '2018-05-06', 'BE', 'plays', 52572);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9959, '2018-05-06', 'BE', 'clicks', 49033);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9963, '2018-05-06', 'BR', 'views', 84977);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9964, '2018-05-06', 'BR', 'plays', 41486);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9965, '2018-05-06', 'BR', 'clicks', 33153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9954, '2018-05-06', 'CA', 'views', 57456);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9955, '2018-05-06', 'CA', 'plays', 12125);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9956, '2018-05-06', 'CA', 'clicks', 36142);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9969, '2018-05-06', 'CY', 'views', 16853);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9970, '2018-05-06', 'CY', 'plays', 16809);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9971, '2018-05-06', 'CY', 'clicks', 40213);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9981, '2018-05-06', 'DE', 'views', 37973);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9982, '2018-05-06', 'DE', 'plays', 53858);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9983, '2018-05-06', 'DE', 'clicks', 74497);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9972, '2018-05-06', 'EG', 'views', 60863);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9973, '2018-05-06', 'EG', 'plays', 35525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9974, '2018-05-06', 'EG', 'clicks', 22593);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9978, '2018-05-06', 'FR', 'views', 31556);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9979, '2018-05-06', 'FR', 'plays', 65571);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9980, '2018-05-06', 'FR', 'clicks', 35655);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9966, '2018-05-06', 'HR', 'views', 62105);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9967, '2018-05-06', 'HR', 'plays', 33603);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9968, '2018-05-06', 'HR', 'clicks', 73694);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9975, '2018-05-06', 'RS', 'views', 32459);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9976, '2018-05-06', 'RS', 'plays', 59116);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9977, '2018-05-06', 'RS', 'clicks', 79449);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9987, '2018-05-07', 'AT', 'views', 85013);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9988, '2018-05-07', 'AT', 'plays', 7415);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9989, '2018-05-07', 'AT', 'clicks', 93052);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9984, '2018-05-07', 'AU', 'views', 30880);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9985, '2018-05-07', 'AU', 'plays', 27150);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9986, '2018-05-07', 'AU', 'clicks', 23272);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9996, '2018-05-07', 'BA', 'views', 89630);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9997, '2018-05-07', 'BA', 'plays', 89868);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9998, '2018-05-07', 'BA', 'clicks', 36238);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9993, '2018-05-07', 'BE', 'views', 85675);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9994, '2018-05-07', 'BE', 'plays', 57316);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9995, '2018-05-07', 'BE', 'clicks', 39063);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9999, '2018-05-07', 'BR', 'views', 56293);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10000, '2018-05-07', 'BR', 'plays', 80229);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10001, '2018-05-07', 'BR', 'clicks', 55756);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9990, '2018-05-07', 'CA', 'views', 37487);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9991, '2018-05-07', 'CA', 'plays', 50715);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(9992, '2018-05-07', 'CA', 'clicks', 17545);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10005, '2018-05-07', 'CY', 'views', 40486);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10006, '2018-05-07', 'CY', 'plays', 40456);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10007, '2018-05-07', 'CY', 'clicks', 23512);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10017, '2018-05-07', 'DE', 'views', 26820);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10018, '2018-05-07', 'DE', 'plays', 48931);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10019, '2018-05-07', 'DE', 'clicks', 57487);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10008, '2018-05-07', 'EG', 'views', 51207);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10009, '2018-05-07', 'EG', 'plays', 40925);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10010, '2018-05-07', 'EG', 'clicks', 62178);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10014, '2018-05-07', 'FR', 'views', 22427);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10015, '2018-05-07', 'FR', 'plays', 53372);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10016, '2018-05-07', 'FR', 'clicks', 50336);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10002, '2018-05-07', 'HR', 'views', 68763);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10003, '2018-05-07', 'HR', 'plays', 69824);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10004, '2018-05-07', 'HR', 'clicks', 13175);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10011, '2018-05-07', 'RS', 'views', 31562);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10012, '2018-05-07', 'RS', 'plays', 35041);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10013, '2018-05-07', 'RS', 'clicks', 9353);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10023, '2018-05-08', 'AT', 'views', 14583);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10024, '2018-05-08', 'AT', 'plays', 58760);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10025, '2018-05-08', 'AT', 'clicks', 73499);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10020, '2018-05-08', 'AU', 'views', 71954);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10021, '2018-05-08', 'AU', 'plays', 75938);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10022, '2018-05-08', 'AU', 'clicks', 54690);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10032, '2018-05-08', 'BA', 'views', 84525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10033, '2018-05-08', 'BA', 'plays', 24137);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10034, '2018-05-08', 'BA', 'clicks', 70026);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10029, '2018-05-08', 'BE', 'views', 44624);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10030, '2018-05-08', 'BE', 'plays', 53641);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10031, '2018-05-08', 'BE', 'clicks', 20058);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10035, '2018-05-08', 'BR', 'views', 38789);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10036, '2018-05-08', 'BR', 'plays', 45684);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10037, '2018-05-08', 'BR', 'clicks', 29023);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10026, '2018-05-08', 'CA', 'views', 14281);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10027, '2018-05-08', 'CA', 'plays', 45813);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10028, '2018-05-08', 'CA', 'clicks', 29018);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10041, '2018-05-08', 'CY', 'views', 53259);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10042, '2018-05-08', 'CY', 'plays', 53924);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10043, '2018-05-08', 'CY', 'clicks', 29752);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10053, '2018-05-08', 'DE', 'views', 31606);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10054, '2018-05-08', 'DE', 'plays', 44282);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10055, '2018-05-08', 'DE', 'clicks', 57078);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10044, '2018-05-08', 'EG', 'views', 36227);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10045, '2018-05-08', 'EG', 'plays', 81088);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10046, '2018-05-08', 'EG', 'clicks', 20157);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10050, '2018-05-08', 'FR', 'views', 77515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10051, '2018-05-08', 'FR', 'plays', 45118);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10052, '2018-05-08', 'FR', 'clicks', 60924);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10038, '2018-05-08', 'HR', 'views', 44948);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10039, '2018-05-08', 'HR', 'plays', 46394);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10040, '2018-05-08', 'HR', 'clicks', 30282);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10047, '2018-05-08', 'RS', 'views', 90304);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10048, '2018-05-08', 'RS', 'plays', 36762);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10049, '2018-05-08', 'RS', 'clicks', 21360);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10059, '2018-05-09', 'AT', 'views', 14599);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10060, '2018-05-09', 'AT', 'plays', 51816);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10061, '2018-05-09', 'AT', 'clicks', 71969);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10056, '2018-05-09', 'AU', 'views', 70439);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10057, '2018-05-09', 'AU', 'plays', 37826);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10058, '2018-05-09', 'AU', 'clicks', 60967);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10068, '2018-05-09', 'BA', 'views', 37475);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10069, '2018-05-09', 'BA', 'plays', 46226);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10070, '2018-05-09', 'BA', 'clicks', 62467);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10065, '2018-05-09', 'BE', 'views', 27117);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10066, '2018-05-09', 'BE', 'plays', 51249);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10067, '2018-05-09', 'BE', 'clicks', 27824);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10071, '2018-05-09', 'BR', 'views', 51511);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10072, '2018-05-09', 'BR', 'plays', 34577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10073, '2018-05-09', 'BR', 'clicks', 39448);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10062, '2018-05-09', 'CA', 'views', 46574);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10063, '2018-05-09', 'CA', 'plays', 37257);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10064, '2018-05-09', 'CA', 'clicks', 56444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10077, '2018-05-09', 'CY', 'views', 58893);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10078, '2018-05-09', 'CY', 'plays', 32692);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10079, '2018-05-09', 'CY', 'clicks', 47786);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10089, '2018-05-09', 'DE', 'views', 70847);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10090, '2018-05-09', 'DE', 'plays', 36263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10091, '2018-05-09', 'DE', 'clicks', 27153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10080, '2018-05-09', 'EG', 'views', 82231);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10081, '2018-05-09', 'EG', 'plays', 58593);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10082, '2018-05-09', 'EG', 'clicks', 83099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10086, '2018-05-09', 'FR', 'views', 56819);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10087, '2018-05-09', 'FR', 'plays', 28669);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10088, '2018-05-09', 'FR', 'clicks', 36655);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10074, '2018-05-09', 'HR', 'views', 48485);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10075, '2018-05-09', 'HR', 'plays', 15460);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10076, '2018-05-09', 'HR', 'clicks', 35647);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10083, '2018-05-09', 'RS', 'views', 32548);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10084, '2018-05-09', 'RS', 'plays', 5769);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10085, '2018-05-09', 'RS', 'clicks', 33514);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10095, '2018-05-10', 'AT', 'views', 20515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10096, '2018-05-10', 'AT', 'plays', 63407);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10097, '2018-05-10', 'AT', 'clicks', 16142);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10092, '2018-05-10', 'AU', 'views', 61012);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10093, '2018-05-10', 'AU', 'plays', 54924);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10094, '2018-05-10', 'AU', 'clicks', 88825);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10104, '2018-05-10', 'BA', 'views', 15829);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10105, '2018-05-10', 'BA', 'plays', 40911);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10106, '2018-05-10', 'BA', 'clicks', 42825);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10101, '2018-05-10', 'BE', 'views', 27291);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10102, '2018-05-10', 'BE', 'plays', 42447);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10103, '2018-05-10', 'BE', 'clicks', 68448);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10107, '2018-05-10', 'BR', 'views', 82996);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10108, '2018-05-10', 'BR', 'plays', 44185);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10109, '2018-05-10', 'BR', 'clicks', 95559);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10098, '2018-05-10', 'CA', 'views', 62710);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10099, '2018-05-10', 'CA', 'plays', 37050);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10100, '2018-05-10', 'CA', 'clicks', 84549);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10113, '2018-05-10', 'CY', 'views', 47291);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10114, '2018-05-10', 'CY', 'plays', 18186);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10115, '2018-05-10', 'CY', 'clicks', 46466);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10125, '2018-05-10', 'DE', 'views', 46705);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10126, '2018-05-10', 'DE', 'plays', 20246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10127, '2018-05-10', 'DE', 'clicks', 62822);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10116, '2018-05-10', 'EG', 'views', 36887);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10117, '2018-05-10', 'EG', 'plays', 29202);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10118, '2018-05-10', 'EG', 'clicks', 15382);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10122, '2018-05-10', 'FR', 'views', 42105);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10123, '2018-05-10', 'FR', 'plays', 42475);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10124, '2018-05-10', 'FR', 'clicks', 33344);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10110, '2018-05-10', 'HR', 'views', 79953);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10111, '2018-05-10', 'HR', 'plays', 44186);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10112, '2018-05-10', 'HR', 'clicks', 31467);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10119, '2018-05-10', 'RS', 'views', 73747);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10120, '2018-05-10', 'RS', 'plays', 57885);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10121, '2018-05-10', 'RS', 'clicks', 57903);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10131, '2018-05-11', 'AT', 'views', 57554);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10132, '2018-05-11', 'AT', 'plays', 65973);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10133, '2018-05-11', 'AT', 'clicks', 42863);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10128, '2018-05-11', 'AU', 'views', 54421);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10129, '2018-05-11', 'AU', 'plays', 33066);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10130, '2018-05-11', 'AU', 'clicks', 53564);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10140, '2018-05-11', 'BA', 'views', 51981);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10141, '2018-05-11', 'BA', 'plays', 36490);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10142, '2018-05-11', 'BA', 'clicks', 44751);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10137, '2018-05-11', 'BE', 'views', 73032);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10138, '2018-05-11', 'BE', 'plays', 61314);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10139, '2018-05-11', 'BE', 'clicks', 63535);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10143, '2018-05-11', 'BR', 'views', 56800);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10144, '2018-05-11', 'BR', 'plays', 34320);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10145, '2018-05-11', 'BR', 'clicks', 50340);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10134, '2018-05-11', 'CA', 'views', 42812);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10135, '2018-05-11', 'CA', 'plays', 63620);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10136, '2018-05-11', 'CA', 'clicks', 55214);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10149, '2018-05-11', 'CY', 'views', 51216);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10150, '2018-05-11', 'CY', 'plays', 31324);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10151, '2018-05-11', 'CY', 'clicks', 60787);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10161, '2018-05-11', 'DE', 'views', 70772);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10162, '2018-05-11', 'DE', 'plays', 27084);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10163, '2018-05-11', 'DE', 'clicks', 46147);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10152, '2018-05-11', 'EG', 'views', 31318);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10153, '2018-05-11', 'EG', 'plays', 17784);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10154, '2018-05-11', 'EG', 'clicks', 79114);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10158, '2018-05-11', 'FR', 'views', 49363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10159, '2018-05-11', 'FR', 'plays', 41287);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10160, '2018-05-11', 'FR', 'clicks', 46137);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10146, '2018-05-11', 'HR', 'views', 86572);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10147, '2018-05-11', 'HR', 'plays', 11681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10148, '2018-05-11', 'HR', 'clicks', 47530);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10155, '2018-05-11', 'RS', 'views', 65510);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10156, '2018-05-11', 'RS', 'plays', 31101);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10157, '2018-05-11', 'RS', 'clicks', 16929);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10167, '2018-05-12', 'AT', 'views', 48054);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10168, '2018-05-12', 'AT', 'plays', 49992);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10169, '2018-05-12', 'AT', 'clicks', 51062);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10164, '2018-05-12', 'AU', 'views', 39229);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10165, '2018-05-12', 'AU', 'plays', 47124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10166, '2018-05-12', 'AU', 'clicks', 31391);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10176, '2018-05-12', 'BA', 'views', 70025);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10177, '2018-05-12', 'BA', 'plays', 81087);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10178, '2018-05-12', 'BA', 'clicks', 36163);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10173, '2018-05-12', 'BE', 'views', 81255);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10174, '2018-05-12', 'BE', 'plays', 53614);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10175, '2018-05-12', 'BE', 'clicks', 33746);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10179, '2018-05-12', 'BR', 'views', 7416);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10180, '2018-05-12', 'BR', 'plays', 77926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10181, '2018-05-12', 'BR', 'clicks', 87831);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10170, '2018-05-12', 'CA', 'views', 39403);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10171, '2018-05-12', 'CA', 'plays', 54644);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10172, '2018-05-12', 'CA', 'clicks', 49854);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10185, '2018-05-12', 'CY', 'views', 47360);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10186, '2018-05-12', 'CY', 'plays', 65264);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10187, '2018-05-12', 'CY', 'clicks', 43519);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10197, '2018-05-12', 'DE', 'views', 36436);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10198, '2018-05-12', 'DE', 'plays', 88926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10199, '2018-05-12', 'DE', 'clicks', 66159);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10188, '2018-05-12', 'EG', 'views', 60507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10189, '2018-05-12', 'EG', 'plays', 82450);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10190, '2018-05-12', 'EG', 'clicks', 35029);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10194, '2018-05-12', 'FR', 'views', 21371);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10195, '2018-05-12', 'FR', 'plays', 72420);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10196, '2018-05-12', 'FR', 'clicks', 74162);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10182, '2018-05-12', 'HR', 'views', 47291);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10183, '2018-05-12', 'HR', 'plays', 54444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10184, '2018-05-12', 'HR', 'clicks', 48218);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10191, '2018-05-12', 'RS', 'views', 36369);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10192, '2018-05-12', 'RS', 'plays', 40689);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10193, '2018-05-12', 'RS', 'clicks', 47727);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10203, '2018-05-13', 'AT', 'views', 62493);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10204, '2018-05-13', 'AT', 'plays', 10720);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10205, '2018-05-13', 'AT', 'clicks', 57827);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10200, '2018-05-13', 'AU', 'views', 48828);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10201, '2018-05-13', 'AU', 'plays', 31876);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10202, '2018-05-13', 'AU', 'clicks', 63510);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10212, '2018-05-13', 'BA', 'views', 54421);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10213, '2018-05-13', 'BA', 'plays', 11327);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10214, '2018-05-13', 'BA', 'clicks', 74749);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10209, '2018-05-13', 'BE', 'views', 36076);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10210, '2018-05-13', 'BE', 'plays', 84140);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10211, '2018-05-13', 'BE', 'clicks', 51450);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10215, '2018-05-13', 'BR', 'views', 22522);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10216, '2018-05-13', 'BR', 'plays', 18510);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10217, '2018-05-13', 'BR', 'clicks', 31097);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10206, '2018-05-13', 'CA', 'views', 51691);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10207, '2018-05-13', 'CA', 'plays', 31156);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10208, '2018-05-13', 'CA', 'clicks', 45932);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10221, '2018-05-13', 'CY', 'views', 34025);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10222, '2018-05-13', 'CY', 'plays', 77677);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10223, '2018-05-13', 'CY', 'clicks', 81285);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10233, '2018-05-13', 'DE', 'views', 55812);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10234, '2018-05-13', 'DE', 'plays', 54150);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10235, '2018-05-13', 'DE', 'clicks', 87260);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10224, '2018-05-13', 'EG', 'views', 45054);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10225, '2018-05-13', 'EG', 'plays', 70022);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10226, '2018-05-13', 'EG', 'clicks', 77395);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10230, '2018-05-13', 'FR', 'views', 72346);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10231, '2018-05-13', 'FR', 'plays', 65950);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10232, '2018-05-13', 'FR', 'clicks', 44912);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10218, '2018-05-13', 'HR', 'views', 45726);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10219, '2018-05-13', 'HR', 'plays', 54028);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10220, '2018-05-13', 'HR', 'clicks', 85180);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10227, '2018-05-13', 'RS', 'views', 74767);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10228, '2018-05-13', 'RS', 'plays', 24511);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10229, '2018-05-13', 'RS', 'clicks', 30017);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10239, '2018-05-14', 'AT', 'views', 39105);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10240, '2018-05-14', 'AT', 'plays', 47281);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10241, '2018-05-14', 'AT', 'clicks', 62909);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10236, '2018-05-14', 'AU', 'views', 87152);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10237, '2018-05-14', 'AU', 'plays', 47122);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10238, '2018-05-14', 'AU', 'clicks', 40612);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10248, '2018-05-14', 'BA', 'views', 70209);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10249, '2018-05-14', 'BA', 'plays', 9752);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10250, '2018-05-14', 'BA', 'clicks', 20145);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10245, '2018-05-14', 'BE', 'views', 38913);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10246, '2018-05-14', 'BE', 'plays', 68580);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10247, '2018-05-14', 'BE', 'clicks', 82549);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10251, '2018-05-14', 'BR', 'views', 55246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10252, '2018-05-14', 'BR', 'plays', 36533);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10253, '2018-05-14', 'BR', 'clicks', 66121);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10242, '2018-05-14', 'CA', 'views', 61142);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10243, '2018-05-14', 'CA', 'plays', 26640);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10244, '2018-05-14', 'CA', 'clicks', 71455);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10257, '2018-05-14', 'CY', 'views', 34417);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10258, '2018-05-14', 'CY', 'plays', 50124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10259, '2018-05-14', 'CY', 'clicks', 61045);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10269, '2018-05-14', 'DE', 'views', 59203);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10270, '2018-05-14', 'DE', 'plays', 73885);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10271, '2018-05-14', 'DE', 'clicks', 57943);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10260, '2018-05-14', 'EG', 'views', 66876);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10261, '2018-05-14', 'EG', 'plays', 62563);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10262, '2018-05-14', 'EG', 'clicks', 52177);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10266, '2018-05-14', 'FR', 'views', 95625);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10267, '2018-05-14', 'FR', 'plays', 53515);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10268, '2018-05-14', 'FR', 'clicks', 11303);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10254, '2018-05-14', 'HR', 'views', 50209);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10255, '2018-05-14', 'HR', 'plays', 52926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10256, '2018-05-14', 'HR', 'clicks', 40311);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10263, '2018-05-14', 'RS', 'views', 50819);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10264, '2018-05-14', 'RS', 'plays', 52587);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10265, '2018-05-14', 'RS', 'clicks', 49663);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10275, '2018-05-15', 'AT', 'views', 37725);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10276, '2018-05-15', 'AT', 'plays', 14683);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10277, '2018-05-15', 'AT', 'clicks', 65773);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10272, '2018-05-15', 'AU', 'views', 43796);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10273, '2018-05-15', 'AU', 'plays', 60114);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10274, '2018-05-15', 'AU', 'clicks', 51153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10284, '2018-05-15', 'BA', 'views', 66416);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10285, '2018-05-15', 'BA', 'plays', 86330);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10286, '2018-05-15', 'BA', 'clicks', 62684);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10281, '2018-05-15', 'BE', 'views', 29062);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10282, '2018-05-15', 'BE', 'plays', 69032);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10283, '2018-05-15', 'BE', 'clicks', 75618);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10287, '2018-05-15', 'BR', 'views', 73714);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10288, '2018-05-15', 'BR', 'plays', 77669);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10289, '2018-05-15', 'BR', 'clicks', 40314);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10278, '2018-05-15', 'CA', 'views', 12658);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10279, '2018-05-15', 'CA', 'plays', 47634);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10280, '2018-05-15', 'CA', 'clicks', 41902);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10293, '2018-05-15', 'CY', 'views', 47857);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10294, '2018-05-15', 'CY', 'plays', 35167);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10295, '2018-05-15', 'CY', 'clicks', 39936);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10305, '2018-05-15', 'DE', 'views', 91178);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10306, '2018-05-15', 'DE', 'plays', 13595);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10307, '2018-05-15', 'DE', 'clicks', 61061);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10296, '2018-05-15', 'EG', 'views', 36558);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10297, '2018-05-15', 'EG', 'plays', 18682);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10298, '2018-05-15', 'EG', 'clicks', 70133);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10302, '2018-05-15', 'FR', 'views', 41540);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10303, '2018-05-15', 'FR', 'plays', 54408);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10304, '2018-05-15', 'FR', 'clicks', 47502);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10290, '2018-05-15', 'HR', 'views', 46577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10291, '2018-05-15', 'HR', 'plays', 21834);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10292, '2018-05-15', 'HR', 'clicks', 48919);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10299, '2018-05-15', 'RS', 'views', 34671);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10300, '2018-05-15', 'RS', 'plays', 52309);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10301, '2018-05-15', 'RS', 'clicks', 57586);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10311, '2018-05-16', 'AT', 'views', 30731);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10312, '2018-05-16', 'AT', 'plays', 34085);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10313, '2018-05-16', 'AT', 'clicks', 56643);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10308, '2018-05-16', 'AU', 'views', 84901);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10309, '2018-05-16', 'AU', 'plays', 16683);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10310, '2018-05-16', 'AU', 'clicks', 38885);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10320, '2018-05-16', 'BA', 'views', 26859);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10321, '2018-05-16', 'BA', 'plays', 50966);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10322, '2018-05-16', 'BA', 'clicks', 53505);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10317, '2018-05-16', 'BE', 'views', 52243);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10318, '2018-05-16', 'BE', 'plays', 71629);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10319, '2018-05-16', 'BE', 'clicks', 61263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10323, '2018-05-16', 'BR', 'views', 41428);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10324, '2018-05-16', 'BR', 'plays', 63846);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10325, '2018-05-16', 'BR', 'clicks', 58326);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10314, '2018-05-16', 'CA', 'views', 37352);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10315, '2018-05-16', 'CA', 'plays', 51777);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10316, '2018-05-16', 'CA', 'clicks', 65804);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10329, '2018-05-16', 'CY', 'views', 51336);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10330, '2018-05-16', 'CY', 'plays', 19957);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10331, '2018-05-16', 'CY', 'clicks', 65035);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10341, '2018-05-16', 'DE', 'views', 29337);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10342, '2018-05-16', 'DE', 'plays', 38951);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10343, '2018-05-16', 'DE', 'clicks', 89592);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10332, '2018-05-16', 'EG', 'views', 66911);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10333, '2018-05-16', 'EG', 'plays', 81634);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10334, '2018-05-16', 'EG', 'clicks', 63908);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10338, '2018-05-16', 'FR', 'views', 43020);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10339, '2018-05-16', 'FR', 'plays', 38576);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10340, '2018-05-16', 'FR', 'clicks', 41442);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10326, '2018-05-16', 'HR', 'views', 49764);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10327, '2018-05-16', 'HR', 'plays', 22700);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10328, '2018-05-16', 'HR', 'clicks', 64803);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10335, '2018-05-16', 'RS', 'views', 15772);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10336, '2018-05-16', 'RS', 'plays', 43304);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10337, '2018-05-16', 'RS', 'clicks', 3253);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10347, '2018-05-17', 'AT', 'views', 57101);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10348, '2018-05-17', 'AT', 'plays', 41234);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10349, '2018-05-17', 'AT', 'clicks', 66877);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10344, '2018-05-17', 'AU', 'views', 79428);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10345, '2018-05-17', 'AU', 'plays', 25978);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10346, '2018-05-17', 'AU', 'clicks', 30387);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10356, '2018-05-17', 'BA', 'views', 70388);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10357, '2018-05-17', 'BA', 'plays', 50218);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10358, '2018-05-17', 'BA', 'clicks', 59917);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10353, '2018-05-17', 'BE', 'views', 69205);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10354, '2018-05-17', 'BE', 'plays', 18869);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10355, '2018-05-17', 'BE', 'clicks', 62612);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10359, '2018-05-17', 'BR', 'views', 11247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10360, '2018-05-17', 'BR', 'plays', 75511);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10361, '2018-05-17', 'BR', 'clicks', 28346);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10350, '2018-05-17', 'CA', 'views', 33757);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10351, '2018-05-17', 'CA', 'plays', 29452);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10352, '2018-05-17', 'CA', 'clicks', 67154);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10365, '2018-05-17', 'CY', 'views', 14374);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10366, '2018-05-17', 'CY', 'plays', 77768);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10367, '2018-05-17', 'CY', 'clicks', 39244);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10377, '2018-05-17', 'DE', 'views', 33640);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10378, '2018-05-17', 'DE', 'plays', 53192);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10379, '2018-05-17', 'DE', 'clicks', 15671);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10368, '2018-05-17', 'EG', 'views', 16436);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10369, '2018-05-17', 'EG', 'plays', 13099);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10370, '2018-05-17', 'EG', 'clicks', 28265);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10374, '2018-05-17', 'FR', 'views', 71687);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10375, '2018-05-17', 'FR', 'plays', 53935);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10376, '2018-05-17', 'FR', 'clicks', 5584);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10362, '2018-05-17', 'HR', 'views', 19551);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10363, '2018-05-17', 'HR', 'plays', 39523);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10364, '2018-05-17', 'HR', 'clicks', 31551);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10371, '2018-05-17', 'RS', 'views', 53155);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10372, '2018-05-17', 'RS', 'plays', 69437);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10373, '2018-05-17', 'RS', 'clicks', 71065);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10383, '2018-05-18', 'AT', 'views', 36602);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10384, '2018-05-18', 'AT', 'plays', 29766);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10385, '2018-05-18', 'AT', 'clicks', 65411);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10380, '2018-05-18', 'AU', 'views', 49611);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10381, '2018-05-18', 'AU', 'plays', 27888);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10382, '2018-05-18', 'AU', 'clicks', 75932);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10392, '2018-05-18', 'BA', 'views', 54162);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10393, '2018-05-18', 'BA', 'plays', 26087);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10394, '2018-05-18', 'BA', 'clicks', 20354);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10389, '2018-05-18', 'BE', 'views', 80286);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10390, '2018-05-18', 'BE', 'plays', 38451);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10391, '2018-05-18', 'BE', 'clicks', 90196);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10395, '2018-05-18', 'BR', 'views', 63113);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10396, '2018-05-18', 'BR', 'plays', 40689);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10397, '2018-05-18', 'BR', 'clicks', 40329);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10386, '2018-05-18', 'CA', 'views', 83612);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10387, '2018-05-18', 'CA', 'plays', 54948);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10388, '2018-05-18', 'CA', 'clicks', 34545);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10401, '2018-05-18', 'CY', 'views', 79268);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10402, '2018-05-18', 'CY', 'plays', 52296);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10403, '2018-05-18', 'CY', 'clicks', 69422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10413, '2018-05-18', 'DE', 'views', 33897);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10414, '2018-05-18', 'DE', 'plays', 91539);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10415, '2018-05-18', 'DE', 'clicks', 71958);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10404, '2018-05-18', 'EG', 'views', 91795);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10405, '2018-05-18', 'EG', 'plays', 74027);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10406, '2018-05-18', 'EG', 'clicks', 71848);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10410, '2018-05-18', 'FR', 'views', 62272);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10411, '2018-05-18', 'FR', 'plays', 39359);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10412, '2018-05-18', 'FR', 'clicks', 70889);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10398, '2018-05-18', 'HR', 'views', 36660);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10399, '2018-05-18', 'HR', 'plays', 68790);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10400, '2018-05-18', 'HR', 'clicks', 39812);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10407, '2018-05-18', 'RS', 'views', 39190);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10408, '2018-05-18', 'RS', 'plays', 79190);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10409, '2018-05-18', 'RS', 'clicks', 40538);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10419, '2018-05-19', 'AT', 'views', 8043);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10420, '2018-05-19', 'AT', 'plays', 43752);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10421, '2018-05-19', 'AT', 'clicks', 50649);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10416, '2018-05-19', 'AU', 'views', 48057);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10417, '2018-05-19', 'AU', 'plays', 46660);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10418, '2018-05-19', 'AU', 'clicks', 57510);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10428, '2018-05-19', 'BA', 'views', 77580);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10429, '2018-05-19', 'BA', 'plays', 26953);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10430, '2018-05-19', 'BA', 'clicks', 64490);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10425, '2018-05-19', 'BE', 'views', 63559);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10426, '2018-05-19', 'BE', 'plays', 49525);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10427, '2018-05-19', 'BE', 'clicks', 47499);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10431, '2018-05-19', 'BR', 'views', 43124);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10432, '2018-05-19', 'BR', 'plays', 25856);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10433, '2018-05-19', 'BR', 'clicks', 47207);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10422, '2018-05-19', 'CA', 'views', 26381);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10423, '2018-05-19', 'CA', 'plays', 75897);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10424, '2018-05-19', 'CA', 'clicks', 44585);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10437, '2018-05-19', 'CY', 'views', 89291);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10438, '2018-05-19', 'CY', 'plays', 44910);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10439, '2018-05-19', 'CY', 'clicks', 44746);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10449, '2018-05-19', 'DE', 'views', 76408);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10450, '2018-05-19', 'DE', 'plays', 53315);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10451, '2018-05-19', 'DE', 'clicks', 39414);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10440, '2018-05-19', 'EG', 'views', 4576);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10441, '2018-05-19', 'EG', 'plays', 9372);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10442, '2018-05-19', 'EG', 'clicks', 42687);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10446, '2018-05-19', 'FR', 'views', 15914);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10447, '2018-05-19', 'FR', 'plays', 34407);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10448, '2018-05-19', 'FR', 'clicks', 67721);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10434, '2018-05-19', 'HR', 'views', 31166);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10435, '2018-05-19', 'HR', 'plays', 71246);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10436, '2018-05-19', 'HR', 'clicks', 31467);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10443, '2018-05-19', 'RS', 'views', 69044);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10444, '2018-05-19', 'RS', 'plays', 32460);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10445, '2018-05-19', 'RS', 'clicks', 86615);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10455, '2018-05-20', 'AT', 'views', 28185);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10456, '2018-05-20', 'AT', 'plays', 18423);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10457, '2018-05-20', 'AT', 'clicks', 15419);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10452, '2018-05-20', 'AU', 'views', 46312);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10453, '2018-05-20', 'AU', 'plays', 72556);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10454, '2018-05-20', 'AU', 'clicks', 87055);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10464, '2018-05-20', 'BA', 'views', 39633);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10465, '2018-05-20', 'BA', 'plays', 43049);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10466, '2018-05-20', 'BA', 'clicks', 45185);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10461, '2018-05-20', 'BE', 'views', 27150);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10462, '2018-05-20', 'BE', 'plays', 80313);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10463, '2018-05-20', 'BE', 'clicks', 38188);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10467, '2018-05-20', 'BR', 'views', 72577);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10468, '2018-05-20', 'BR', 'plays', 39473);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10469, '2018-05-20', 'BR', 'clicks', 46490);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10458, '2018-05-20', 'CA', 'views', 46112);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10459, '2018-05-20', 'CA', 'plays', 72970);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10460, '2018-05-20', 'CA', 'clicks', 52754);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10473, '2018-05-20', 'CY', 'views', 19444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10474, '2018-05-20', 'CY', 'plays', 60392);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10475, '2018-05-20', 'CY', 'clicks', 96766);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10485, '2018-05-20', 'DE', 'views', 65622);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10486, '2018-05-20', 'DE', 'plays', 58143);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10487, '2018-05-20', 'DE', 'clicks', 51366);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10476, '2018-05-20', 'EG', 'views', 59986);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10477, '2018-05-20', 'EG', 'plays', 53952);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10478, '2018-05-20', 'EG', 'clicks', 39916);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10482, '2018-05-20', 'FR', 'views', 40243);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10483, '2018-05-20', 'FR', 'plays', 43759);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10484, '2018-05-20', 'FR', 'clicks', 59055);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10470, '2018-05-20', 'HR', 'views', 47963);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10471, '2018-05-20', 'HR', 'plays', 67668);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10472, '2018-05-20', 'HR', 'clicks', 60781);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10479, '2018-05-20', 'RS', 'views', 47693);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10480, '2018-05-20', 'RS', 'plays', 83097);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10481, '2018-05-20', 'RS', 'clicks', 48631);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10491, '2018-05-21', 'AT', 'views', 66710);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10492, '2018-05-21', 'AT', 'plays', 5145);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10493, '2018-05-21', 'AT', 'clicks', 27567);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10488, '2018-05-21', 'AU', 'views', 47039);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10489, '2018-05-21', 'AU', 'plays', 50208);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10490, '2018-05-21', 'AU', 'clicks', 79916);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10500, '2018-05-21', 'BA', 'views', 32406);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10501, '2018-05-21', 'BA', 'plays', 39248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10502, '2018-05-21', 'BA', 'clicks', 20946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10497, '2018-05-21', 'BE', 'views', 86870);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10498, '2018-05-21', 'BE', 'plays', 32807);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10499, '2018-05-21', 'BE', 'clicks', 7486);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10503, '2018-05-21', 'BR', 'views', 7854);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10504, '2018-05-21', 'BR', 'plays', 42661);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10505, '2018-05-21', 'BR', 'clicks', 79772);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10494, '2018-05-21', 'CA', 'views', 56004);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10495, '2018-05-21', 'CA', 'plays', 60330);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10496, '2018-05-21', 'CA', 'clicks', 41614);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10509, '2018-05-21', 'CY', 'views', 82009);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10510, '2018-05-21', 'CY', 'plays', 49625);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10511, '2018-05-21', 'CY', 'clicks', 29239);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10521, '2018-05-21', 'DE', 'views', 33366);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10522, '2018-05-21', 'DE', 'plays', 53185);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10523, '2018-05-21', 'DE', 'clicks', 40946);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10512, '2018-05-21', 'EG', 'views', 80158);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10513, '2018-05-21', 'EG', 'plays', 39603);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10514, '2018-05-21', 'EG', 'clicks', 18094);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10518, '2018-05-21', 'FR', 'views', 53077);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10519, '2018-05-21', 'FR', 'plays', 36894);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10520, '2018-05-21', 'FR', 'clicks', 44385);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10506, '2018-05-21', 'HR', 'views', 75547);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10507, '2018-05-21', 'HR', 'plays', 29251);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10508, '2018-05-21', 'HR', 'clicks', 80910);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10515, '2018-05-21', 'RS', 'views', 17621);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10516, '2018-05-21', 'RS', 'plays', 45418);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10517, '2018-05-21', 'RS', 'clicks', 36794);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10527, '2018-05-22', 'AT', 'views', 24103);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10528, '2018-05-22', 'AT', 'plays', 84088);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10529, '2018-05-22', 'AT', 'clicks', 89210);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10524, '2018-05-22', 'AU', 'views', 52248);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10525, '2018-05-22', 'AU', 'plays', 55105);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10526, '2018-05-22', 'AU', 'clicks', 47359);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10536, '2018-05-22', 'BA', 'views', 71300);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10537, '2018-05-22', 'BA', 'plays', 11755);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10538, '2018-05-22', 'BA', 'clicks', 60012);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10533, '2018-05-22', 'BE', 'views', 90405);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10534, '2018-05-22', 'BE', 'plays', 93031);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10535, '2018-05-22', 'BE', 'clicks', 56487);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10539, '2018-05-22', 'BR', 'views', 13362);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10540, '2018-05-22', 'BR', 'plays', 26146);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10541, '2018-05-22', 'BR', 'clicks', 73876);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10530, '2018-05-22', 'CA', 'views', 59107);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10531, '2018-05-22', 'CA', 'plays', 35449);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10532, '2018-05-22', 'CA', 'clicks', 68288);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10545, '2018-05-22', 'CY', 'views', 82942);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10546, '2018-05-22', 'CY', 'plays', 40896);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10547, '2018-05-22', 'CY', 'clicks', 75214);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10557, '2018-05-22', 'DE', 'views', 48193);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10558, '2018-05-22', 'DE', 'plays', 17414);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10559, '2018-05-22', 'DE', 'clicks', 63108);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10548, '2018-05-22', 'EG', 'views', 79079);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10549, '2018-05-22', 'EG', 'plays', 35638);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10550, '2018-05-22', 'EG', 'clicks', 94474);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10554, '2018-05-22', 'FR', 'views', 82509);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10555, '2018-05-22', 'FR', 'plays', 85197);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10556, '2018-05-22', 'FR', 'clicks', 94598);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10542, '2018-05-22', 'HR', 'views', 42152);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10543, '2018-05-22', 'HR', 'plays', 85885);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10544, '2018-05-22', 'HR', 'clicks', 84442);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10551, '2018-05-22', 'RS', 'views', 86910);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10552, '2018-05-22', 'RS', 'plays', 27261);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10553, '2018-05-22', 'RS', 'clicks', 40614);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10563, '2018-05-23', 'AT', 'views', 69760);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10564, '2018-05-23', 'AT', 'plays', 60283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10565, '2018-05-23', 'AT', 'clicks', 52958);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10560, '2018-05-23', 'AU', 'views', 84639);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10561, '2018-05-23', 'AU', 'plays', 56242);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10562, '2018-05-23', 'AU', 'clicks', 80250);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10572, '2018-05-23', 'BA', 'views', 69838);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10573, '2018-05-23', 'BA', 'plays', 72633);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10574, '2018-05-23', 'BA', 'clicks', 84694);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10569, '2018-05-23', 'BE', 'views', 54501);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10570, '2018-05-23', 'BE', 'plays', 14263);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10571, '2018-05-23', 'BE', 'clicks', 49351);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10575, '2018-05-23', 'BR', 'views', 59046);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10576, '2018-05-23', 'BR', 'plays', 66880);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10577, '2018-05-23', 'BR', 'clicks', 52909);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10566, '2018-05-23', 'CA', 'views', 39969);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10567, '2018-05-23', 'CA', 'plays', 27788);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10568, '2018-05-23', 'CA', 'clicks', 88991);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10581, '2018-05-23', 'CY', 'views', 65155);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10582, '2018-05-23', 'CY', 'plays', 52759);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10583, '2018-05-23', 'CY', 'clicks', 42938);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10593, '2018-05-23', 'DE', 'views', 77903);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10594, '2018-05-23', 'DE', 'plays', 30435);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10595, '2018-05-23', 'DE', 'clicks', 15874);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10584, '2018-05-23', 'EG', 'views', 51877);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10585, '2018-05-23', 'EG', 'plays', 9601);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10586, '2018-05-23', 'EG', 'clicks', 33798);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10590, '2018-05-23', 'FR', 'views', 95989);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10591, '2018-05-23', 'FR', 'plays', 50601);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10592, '2018-05-23', 'FR', 'clicks', 80963);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10578, '2018-05-23', 'HR', 'views', 44919);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10579, '2018-05-23', 'HR', 'plays', 62132);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10580, '2018-05-23', 'HR', 'clicks', 44458);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10587, '2018-05-23', 'RS', 'views', 40463);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10588, '2018-05-23', 'RS', 'plays', 74492);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10589, '2018-05-23', 'RS', 'clicks', 73052);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10599, '2018-05-24', 'AT', 'views', 80738);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10600, '2018-05-24', 'AT', 'plays', 39908);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10601, '2018-05-24', 'AT', 'clicks', 74995);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10596, '2018-05-24', 'AU', 'views', 16301);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10597, '2018-05-24', 'AU', 'plays', 54704);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10598, '2018-05-24', 'AU', 'clicks', 42400);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10608, '2018-05-24', 'BA', 'views', 53507);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10609, '2018-05-24', 'BA', 'plays', 52081);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10610, '2018-05-24', 'BA', 'clicks', 81199);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10605, '2018-05-24', 'BE', 'views', 24164);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10606, '2018-05-24', 'BE', 'plays', 32137);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10607, '2018-05-24', 'BE', 'clicks', 67608);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10611, '2018-05-24', 'BR', 'views', 70055);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10612, '2018-05-24', 'BR', 'plays', 81377);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10613, '2018-05-24', 'BR', 'clicks', 49069);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10602, '2018-05-24', 'CA', 'views', 58642);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10603, '2018-05-24', 'CA', 'plays', 47535);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10604, '2018-05-24', 'CA', 'clicks', 45198);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10617, '2018-05-24', 'CY', 'views', 64603);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10618, '2018-05-24', 'CY', 'plays', 69915);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10619, '2018-05-24', 'CY', 'clicks', 34422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10629, '2018-05-24', 'DE', 'views', 44810);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10630, '2018-05-24', 'DE', 'plays', 22626);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10631, '2018-05-24', 'DE', 'clicks', 65926);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10620, '2018-05-24', 'EG', 'views', 36342);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10621, '2018-05-24', 'EG', 'plays', 59896);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10622, '2018-05-24', 'EG', 'clicks', 41942);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10626, '2018-05-24', 'FR', 'views', 60033);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10627, '2018-05-24', 'FR', 'plays', 66681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10628, '2018-05-24', 'FR', 'clicks', 47398);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10614, '2018-05-24', 'HR', 'views', 31422);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10615, '2018-05-24', 'HR', 'plays', 51676);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10616, '2018-05-24', 'HR', 'clicks', 41344);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10623, '2018-05-24', 'RS', 'views', 23561);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10624, '2018-05-24', 'RS', 'plays', 36572);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10625, '2018-05-24', 'RS', 'clicks', 48730);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10635, '2018-05-25', 'AT', 'views', 49414);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10636, '2018-05-25', 'AT', 'plays', 27363);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10637, '2018-05-25', 'AT', 'clicks', 43865);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10632, '2018-05-25', 'AU', 'views', 62907);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10633, '2018-05-25', 'AU', 'plays', 73367);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10634, '2018-05-25', 'AU', 'clicks', 60900);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10644, '2018-05-25', 'BA', 'views', 52283);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10645, '2018-05-25', 'BA', 'plays', 26294);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10646, '2018-05-25', 'BA', 'clicks', 57167);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10641, '2018-05-25', 'BE', 'views', 23277);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10642, '2018-05-25', 'BE', 'plays', 72139);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10643, '2018-05-25', 'BE', 'clicks', 85106);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10647, '2018-05-25', 'BR', 'views', 44527);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10648, '2018-05-25', 'BR', 'plays', 88340);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10649, '2018-05-25', 'BR', 'clicks', 43484);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10638, '2018-05-25', 'CA', 'views', 55721);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10639, '2018-05-25', 'CA', 'plays', 17708);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10640, '2018-05-25', 'CA', 'clicks', 76432);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10653, '2018-05-25', 'CY', 'views', 33075);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10654, '2018-05-25', 'CY', 'plays', 81270);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10655, '2018-05-25', 'CY', 'clicks', 48379);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10665, '2018-05-25', 'DE', 'views', 31885);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10666, '2018-05-25', 'DE', 'plays', 88151);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10667, '2018-05-25', 'DE', 'clicks', 32711);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10656, '2018-05-25', 'EG', 'views', 25563);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10657, '2018-05-25', 'EG', 'plays', 55593);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10658, '2018-05-25', 'EG', 'clicks', 69145);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10662, '2018-05-25', 'FR', 'views', 54431);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10663, '2018-05-25', 'FR', 'plays', 38897);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10664, '2018-05-25', 'FR', 'clicks', 49133);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10650, '2018-05-25', 'HR', 'views', 63872);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10651, '2018-05-25', 'HR', 'plays', 37895);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10652, '2018-05-25', 'HR', 'clicks', 63592);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10659, '2018-05-25', 'RS', 'views', 79409);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10660, '2018-05-25', 'RS', 'plays', 58988);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10661, '2018-05-25', 'RS', 'clicks', 89831);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10671, '2018-05-26', 'AT', 'views', 68289);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10672, '2018-05-26', 'AT', 'plays', 83308);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10673, '2018-05-26', 'AT', 'clicks', 18513);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10668, '2018-05-26', 'AU', 'views', 81676);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10669, '2018-05-26', 'AU', 'plays', 47920);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10670, '2018-05-26', 'AU', 'clicks', 61002);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10680, '2018-05-26', 'BA', 'views', 71938);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10681, '2018-05-26', 'BA', 'plays', 66607);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10682, '2018-05-26', 'BA', 'clicks', 55932);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10677, '2018-05-26', 'BE', 'views', 47048);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10678, '2018-05-26', 'BE', 'plays', 42734);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10679, '2018-05-26', 'BE', 'clicks', 73990);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10683, '2018-05-26', 'BR', 'views', 38153);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10684, '2018-05-26', 'BR', 'plays', 45492);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10685, '2018-05-26', 'BR', 'clicks', 46786);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10674, '2018-05-26', 'CA', 'views', 43032);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10675, '2018-05-26', 'CA', 'plays', 36520);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10676, '2018-05-26', 'CA', 'clicks', 31483);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10689, '2018-05-26', 'CY', 'views', 32934);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10690, '2018-05-26', 'CY', 'plays', 27938);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10691, '2018-05-26', 'CY', 'clicks', 34911);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10701, '2018-05-26', 'DE', 'views', 39860);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10702, '2018-05-26', 'DE', 'plays', 77291);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10703, '2018-05-26', 'DE', 'clicks', 54458);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10692, '2018-05-26', 'EG', 'views', 38110);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10693, '2018-05-26', 'EG', 'plays', 58437);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10694, '2018-05-26', 'EG', 'clicks', 89257);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10698, '2018-05-26', 'FR', 'views', 61730);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10699, '2018-05-26', 'FR', 'plays', 60319);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10700, '2018-05-26', 'FR', 'clicks', 59571);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10686, '2018-05-26', 'HR', 'views', 71951);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10687, '2018-05-26', 'HR', 'plays', 70897);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10688, '2018-05-26', 'HR', 'clicks', 90269);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10695, '2018-05-26', 'RS', 'views', 45811);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10696, '2018-05-26', 'RS', 'plays', 15385);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10697, '2018-05-26', 'RS', 'clicks', 47654);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10707, '2018-05-27', 'AT', 'views', 54891);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10708, '2018-05-27', 'AT', 'plays', 82130);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10709, '2018-05-27', 'AT', 'clicks', 47832);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10704, '2018-05-27', 'AU', 'views', 54184);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10705, '2018-05-27', 'AU', 'plays', 31670);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10706, '2018-05-27', 'AU', 'clicks', 43512);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10716, '2018-05-27', 'BA', 'views', 59819);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10717, '2018-05-27', 'BA', 'plays', 86968);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10718, '2018-05-27', 'BA', 'clicks', 46743);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10713, '2018-05-27', 'BE', 'views', 28831);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10714, '2018-05-27', 'BE', 'plays', 92775);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10715, '2018-05-27', 'BE', 'clicks', 62308);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10719, '2018-05-27', 'BR', 'views', 76730);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10720, '2018-05-27', 'BR', 'plays', 30132);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10721, '2018-05-27', 'BR', 'clicks', 21682);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10710, '2018-05-27', 'CA', 'views', 51543);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10711, '2018-05-27', 'CA', 'plays', 27015);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10712, '2018-05-27', 'CA', 'clicks', 17469);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10725, '2018-05-27', 'CY', 'views', 43081);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10726, '2018-05-27', 'CY', 'plays', 51575);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10727, '2018-05-27', 'CY', 'clicks', 57485);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10737, '2018-05-27', 'DE', 'views', 31824);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10738, '2018-05-27', 'DE', 'plays', 24086);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10739, '2018-05-27', 'DE', 'clicks', 75749);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10728, '2018-05-27', 'EG', 'views', 47200);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10729, '2018-05-27', 'EG', 'plays', 59701);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10730, '2018-05-27', 'EG', 'clicks', 88546);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10734, '2018-05-27', 'FR', 'views', 83579);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10735, '2018-05-27', 'FR', 'plays', 25878);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10736, '2018-05-27', 'FR', 'clicks', 47944);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10722, '2018-05-27', 'HR', 'views', 49892);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10723, '2018-05-27', 'HR', 'plays', 52891);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10724, '2018-05-27', 'HR', 'clicks', 30948);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10731, '2018-05-27', 'RS', 'views', 86141);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10732, '2018-05-27', 'RS', 'plays', 35152);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10733, '2018-05-27', 'RS', 'clicks', 25658);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10743, '2018-05-28', 'AT', 'views', 86247);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10744, '2018-05-28', 'AT', 'plays', 67353);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10745, '2018-05-28', 'AT', 'clicks', 15925);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10740, '2018-05-28', 'AU', 'views', 47036);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10741, '2018-05-28', 'AU', 'plays', 38675);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10742, '2018-05-28', 'AU', 'clicks', 65242);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10752, '2018-05-28', 'BA', 'views', 29721);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10753, '2018-05-28', 'BA', 'plays', 82755);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10754, '2018-05-28', 'BA', 'clicks', 75221);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10749, '2018-05-28', 'BE', 'views', 65605);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10750, '2018-05-28', 'BE', 'plays', 57176);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10751, '2018-05-28', 'BE', 'clicks', 64533);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10755, '2018-05-28', 'BR', 'views', 46522);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10756, '2018-05-28', 'BR', 'plays', 44431);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10757, '2018-05-28', 'BR', 'clicks', 58614);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10746, '2018-05-28', 'CA', 'views', 14681);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10747, '2018-05-28', 'CA', 'plays', 41354);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10748, '2018-05-28', 'CA', 'clicks', 50680);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10761, '2018-05-28', 'CY', 'views', 20341);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10762, '2018-05-28', 'CY', 'plays', 66982);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10763, '2018-05-28', 'CY', 'clicks', 85627);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10773, '2018-05-28', 'DE', 'views', 63284);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10774, '2018-05-28', 'DE', 'plays', 76923);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10775, '2018-05-28', 'DE', 'clicks', 44471);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10764, '2018-05-28', 'EG', 'views', 20565);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10765, '2018-05-28', 'EG', 'plays', 56290);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10766, '2018-05-28', 'EG', 'clicks', 24022);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10770, '2018-05-28', 'FR', 'views', 81719);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10771, '2018-05-28', 'FR', 'plays', 65607);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10772, '2018-05-28', 'FR', 'clicks', 24479);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10758, '2018-05-28', 'HR', 'views', 34714);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10759, '2018-05-28', 'HR', 'plays', 22444);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10760, '2018-05-28', 'HR', 'clicks', 47168);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10767, '2018-05-28', 'RS', 'views', 5877);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10768, '2018-05-28', 'RS', 'plays', 38458);
INSERT INTO `events` (`id`, `date_of_event`, `country_code`, `type_of_event`, `counts`) VALUES
	(10769, '2018-05-28', 'RS', 'clicks', 35250);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
