<?php

  require_once(dirname(__FILE__) . "/configuration.php");
  require_once(dirname(__FILE__) . "/classes/EventsTable.class.php");

  // only web request "GET" is allowed (no "POST")
  if (isset($_SERVER["REQUEST_METHOD"])) {
    // Web call must be POST with JSON as web call body
    if (strcasecmp($_SERVER["REQUEST_METHOD"], "GET") != 0){
      JSON_Response(false, "Request method for Register Event must be GET!");
    };
  };

  // if call is from command line
  if (!isset($_SERVER["REQUEST_METHOD"])) {
  
    // simulate "GET" request parameters
    $username = "Quantox";
    $outputFormat = "json";
    
  } else {

    // do we have 'username' parameter
    if (!isset($_GET["username"])) {
      header("HTTP/1.1 400 BAD REQUEST");
      JSON_Response(false, "Missing Url GET parameter called 'username'");
    }
    $username = $_GET["username"];

    // do we have 'output' parameter
    if (!isset($_GET["output"])) {
      header("HTTP/1.1 400 BAD REQUEST");
      JSON_Response(false, "Missing Url GET parameter called 'output'");
    }
    $outputFormat = $_GET["output"];
    
  }

  // authorization test by username, see file configuration.php [$validUsernames = array("Quantox");]
  if (in_array($username, $validUsernames) == false) {
    JSON_Response(false, "Authorization error, username '" . $username . "' does not exists!");
  };

  // new event need to have type_of_event [array("views", "plays", "clicks")]
  if (in_array($outputFormat, $outputFormats) == false) {
    JSON_Response(false, "Output parameter '" . $outputFormat . " does not exists!");
  };

  // if last JSON file was created within 10 seconds than just flush that cached file to the browser
  if ($outputFormat == "json") {
  
    $filePath = dirname(__FILE__) . CACHED_DIRECTORY_PATH . CACHED_JSON_FILE_NAME;
    if (file_exists($filePath) &&
        (time() - filemtime($filePath)) < CACHED_EXPIRE_TIME) {
      header("Content-Type: application/json");
      echo file_get_contents($filePath);
      exit();
    }

  // if last CSV file was created within 10 seconds than just flush that cached file to the browser
  } else  if ($outputFormat == "csv") {

    $filePath = dirname(__FILE__) . CACHED_DIRECTORY_PATH . CACHED_CSV_FILE_NAME;
    if (file_exists($filePath) &&
        (time() - filemtime($filePath)) < CACHED_EXPIRE_TIME) {
      header("Content-type: application/excel");
      header("Content-disposition: attachment; filename=\"" . CACHED_CSV_FILE_NAME ."\""); 
      echo file_get_contents($filePath);
      exit();
    }

  // if last XML file was created within 10 seconds than just flush that cached file to the browser
  } else if  ($outputFormat == "xml") {
  
    $filePath = dirname(__FILE__) . CACHED_DIRECTORY_PATH . CACHED_XML_FILE_NAME;
    if (file_exists($filePath) &&
        (time() - filemtime($filePath)) < CACHED_EXPIRE_TIME) {
      header("Content-type: application/xml");
      echo file_get_contents($filePath);
      exit();
    }

  }
  
  // create database layer, do not connect yet to database to save access time
  $clsDatabase = new MySqlDatabase(DB_HOST, // host 
                                   "", // port 
                                   DB_USER, // username
                                   DB_PASSWORD, // password
                                   DB_NAME // database
                                  );
                                  
  try {
  
    // connect to database
    $clsDatabase->connect();
  
    // create EventsTable mapped to MySql table 'events'
    $tblEvents = new EventsTable($clsDatabase);
    
    // get top 5 couuntries by suming all sql table records
    $topFiveCountriesOfAllTimes = $tblEvents->getTopFiveCountriesOfAllTimes();
    
    // if there is no any country, we have empty table
    if (count($topFiveCountriesOfAllTimes) == 0) {
      header("HTTP/1.1 400 BAD REQUEST");
      // return to web caller failed Event registrations
      JSON_Response(false, "It seems that SQL table 'events' has no records!") ;
      exit();
    }

    // extarct only 'country_code(s)' from arrya of values
    $topFiveCountries = array_column($topFiveCountriesOfAllTimes, "country_code");
    // find for that 5 countris all records within last 7 fays
    $lastSevenDaysByCountires = $tblEvents->getLastSevenDaysByCountires($topFiveCountries);

    // create array of values: country_code,all_counts,counts_7_last_days,plays,views,clicks
    for($i=0; $i<count($topFiveCountriesOfAllTimes); $i++) {
    
      foreach($lastSevenDaysByCountires as $lastSevenDaysByCountry) {
      
        if ($topFiveCountriesOfAllTimes[$i]["country_code"] == $lastSevenDaysByCountry["country_code"]) {

          // add also human read-able country name
          if (!isset($topFiveCountriesOfAllTimes[$i]["country_name"])) {
            $topFiveCountriesOfAllTimes[$i]["country_name"] = "";
            $topFiveCountriesOfAllTimes[$i]["country_name"] = $validCountries[$lastSevenDaysByCountry["country_code"]];
          }

          // get counts for last 7 days per country
          if (!isset($topFiveCountriesOfAllTimes[$i]["counts_7_last_days"])) {
            $topFiveCountriesOfAllTimes[$i]["counts_7_last_days"] = 0;
          }
          $topFiveCountriesOfAllTimes[$i]["counts_7_last_days"] += $lastSevenDaysByCountry["counts"];
          
          // get single counts per event type
          if (!isset($topFiveCountriesOfAllTimes[$i][$lastSevenDaysByCountry["type_of_event"]])) {
            $topFiveCountriesOfAllTimes[$i][$lastSevenDaysByCountry["type_of_event"]] = 0;
          }
          $topFiveCountriesOfAllTimes[$i][$lastSevenDaysByCountry["type_of_event"]] += $lastSevenDaysByCountry["counts"];
        }
      }
      
    }

    // disconnect from database
    $clsDatabase->disconnect();

    // if browser requested output format to be JSON file
    if ($outputFormat == "json") {
      Flush_JSON_Response($topFiveCountriesOfAllTimes, CACHED_JSON_FILE_NAME);
    // if browser requested output format to be CSV file
    } else  if ($outputFormat == "csv") {
      Flush_CSV_Response($topFiveCountriesOfAllTimes, CACHED_CSV_FILE_NAME);
    // if browser requested output format to be XML file
    } else if  ($outputFormat == "xml") {
      Flush_XML_Response($topFiveCountriesOfAllTimes, CACHED_XML_FILE_NAME);
    } else {
      header("HTTP/1.1 400 BAD REQUEST");
      // return to web caller failed Event registrations
      JSON_Response(false, "Unsupported output format!");
    }
    
  } catch (Exception $e) {

      header("HTTP/1.1 400 BAD REQUEST");
      // return to web caller failed Event registrations
      JSON_Response(false, $e->getMessage()) ;
  }
  
?>
