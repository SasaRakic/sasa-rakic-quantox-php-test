<?php

  require_once(dirname(__FILE__) . "/configuration.php");

  require_once(dirname(__FILE__) . "/classes/EventsTable.class.php");

  if (isset($_SERVER["REQUEST_METHOD"])) {
    // Web call must be POST with JSON as web call body
    if (strcasecmp($_SERVER["REQUEST_METHOD"], "POST") != 0){
      JSON_Response(false, "Request method for Register Event must be POST!");
    };
  };

  if (!isset($_SERVER["REQUEST_METHOD"])) {
    // command line test - console output - no call from web browser
    $jsonBodyContent = '{ "username" : "Quantox",
                          "country_code" : "US",
                          "type_of_event" : "views"
                        }';
  } else {
    // get POST JSON body/text
    $jsonBodyContent = file_get_contents("php://input");
  }

  // convert POST JSON body to PHP array
  $jsonArray = json_decode($jsonBodyContent);
  if (!(json_last_error() == JSON_ERROR_NONE)) {
    JSON_Response(false, "Request POST body is not valid JSON format!");
  };

  // authorization only by username simple authorization (no password) 
  if (!isset($jsonArray->username)) {
    JSON_Response(false, "Missing JSON field called 'username'");
  };

  // authorization test by username, see file configuration.php [$validUsernames = array("Quantox");]
  if (in_array($jsonArray->username, $validUsernames) == false) {
    JSON_Response(false, "Authorization error, username '" . $jsonArray->username . "' does not exists!");
  };

  // new event need to have country_code (two letters upper case)
  if (!isset($jsonArray->country_code)) {
    JSON_Response(false, "Missing JSON field called 'country_code'");
  };

  // validate country two letters
  if (!isset($validCountries[$jsonArray->country_code])) {
    JSON_Response(false, "Input JSON field country_code '" . $jsonArray->country_code . "' not found, seems to be invalid!");
  };

  // new event need to have type_of_event
  if (!isset($jsonArray->type_of_event)) {
    JSON_Response(false, "Missing JSON field called 'type_of_event'");
  };

  // new event need to have type_of_event [array("views", "plays", "clicks")]
  if (in_array($jsonArray->type_of_event, $typeOfEvents) == false) {
    JSON_Response(false, "Event type '" . $jsonArray->type_of_event . " does not exists!");
  };

  // create database layer, do not connect yet to database to save access time
  $clsDatabase = new MySqlDatabase(DB_HOST, // host 
                                   "", // port 
                                   DB_USER, // username
                                   DB_PASSWORD, // password
                                   DB_NAME // database
                                  );
                                  
  try {
  
    // connect to database
    $clsDatabase->connect();
  
    // create EventsTable mapped to MySql table 'events'
    $tblEvents = new EventsTable($clsDatabase);
    // register this event form web, use today date (not using MySql today date)
    $eventRegistered = $tblEvents->registerEvent(date("Y-m-d"), // $dateOfEvent
                                                 $jsonArray->country_code, // $countryCode
                                                 $jsonArray->type_of_event, // $typeOfEvent
                                                 1 // $increaseCountsBy
                                                );

    // disconnect from database
    $clsDatabase->disconnect();

    // return to web caller, Event has been registered with no errors
    JSON_Response(true, "Event '" . $jsonArray->type_of_event . "' for country '" . $jsonArray->country_code . "' registered");
    
  } catch (Exception $e) {

      // return to web caller failed Event registrations
      JSON_Response(false, $e->getMessage()) ;
  }
  
?>
