<?php

  function Log_Error($message) {
    $fileHandle = fopen(dirname(__FILE__) . "/_errors.log.txt","a");
    if ($fileHandle)
    {
      fwrite($fileHandle,date("Y-m-d H:i:s") . " " . $message . "\r\n");
      fclose($fileHandle);
    }
  };
  
  function Custom_Error_Handler($errroNumber,
                                $errorString,
                                $errorFile,
                                $errorLine,
                                $errorContext) {
    $fileHandle = fopen(dirname(__FILE__) . "/_errors.log.txt","a");
    if ($fileHandle)
    {
      fwrite($fileHandle,date("Y-m-d H:i:s") . " [" . $errroNumber . "] " . $errorString . "\r\n");
      fwrite($fileHandle,"                    Line [" . $errorLine . "]: '" . $errorFile . "'\r\n");
      fclose($fileHandle);
    }
  };
  
  set_error_handler("Custom_Error_Handler", E_ALL | E_STRICT);

?>
